//
//  Extension+ViewController.swift
//  MuseumApp
//
//  Created by Admin on 14/03/19.
//  Copyright © 2019 CCT Macmini. All rights reserved.
//

import Foundation
extension UIViewController {

    //MARK:- Prepare Storyboard instances
    var MAIN_STORYBOARD_R1 : UIStoryboard{
        return UIStoryboard.init(name: (IS_IPAD ? "MainiPad" : "Main"), bundle: nil)
    }


    //MARK:- Prepare XIB class


    //MARK:- Make class object from Storyboard
    func MAKE_STORY_OBJ_R1(Identifier:String) -> UIViewController {
        return MAIN_STORYBOARD_R1.instantiateViewController(withIdentifier: Identifier)
    }

    //MARK:- Push Storyboard class
    func PUSH_STORY_R1(Identifier:String){
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
        self.navigationController?.pushViewController(MAKE_STORY_OBJ_R1(Identifier: Identifier), animated: true)
    }
    func PUSH_STORY_R2(Identifier:String){
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
        self.navigationController?.pushViewController(MAKE_STORY_OBJ_R1(Identifier: Identifier), animated: false)
    }

    //MARK:- Push created class object


    //MARK:- Perform Segue
    func PERFORM_SEGUE(Identifier:String){
        self.performSegue(withIdentifier: Identifier, sender: nil)
    }

    //MARK:- POP class object
    func POP_VC(){
        self.navigationController?.popViewController(animated: true)
    }
}

extension NSObject{

    var IS_IPAD : Bool{
        return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad)
}

}
