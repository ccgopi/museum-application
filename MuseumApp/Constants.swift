//
//  Constants.swift
//  PeopleActive
//
//  Created by CCT on 4/14/17.
//  Copyright © 2017 CCT. All rights reserved.
//

import Foundation
struct Constant
{
    static let deviceID = UIDevice.current.identifierForVendor?.uuidString
    static let alertTitle = Bundle.main.infoDictionary![kCFBundleNameKey as String] as! String
    static let AppStoreID = "1238205532"
    static let noInternetMessage = "No network connectivity, try again later."
    static let nodata = "No data found."
    static let kChannelID = "kChannelID"
    static let pendingLeaveCount = "pendingLeaveCount"
    static let isNewEvent = "isNewEvent"
    static let kIsUserLoogedIn = "IsUserLoogedIn"
    static let kLoginInfo = "LogiInfo"
    static let kEmployeeInfo = "EmployeeInfo"
    static let kProfileURL = "ProfileURL"
    static let kPOLICYREAD = "isPolicyRead1"
    static let kNotificationStatus = "notificationStatus"
    static let kISLOGIN = "IsLogin"
    static let kLastUpdatedDate = "LastUpdatedDate"
    static let kAppStoreVersion = "AppStoreVersion"
    

    //Api Key
    //static let baseURL = "http://203.109.113.162:2803/api/"

    //Testing URl
     //static let baseURL = "http://192.168.1.229:2306/api/"
   // static let baseURL = "http://192.168.1.229:2803/api/"
  
    //amazon Url
    static let baseURL = "http://ec2-13-232-3-75.ap-south-1.compute.amazonaws.com/museum_appv2/public/api/"
    
  // static let baseURL = "http://203.109.113.162:5555/museum_app/public/api/"
 
    
    static let CONFIGAPI = "get-app-config"
    static let VERIFY_TICKET = "verify-ticket"
    static let GET_ITEM_DETAILS = "get-item-details"
    static let LOGOUT = "logout"
    static let errorCode = NSNumber.init(value: 202)
    static let successCode = NSNumber.init(value: 200)
    static let UnPaidLeaveCode = NSNumber.init(value: 111)
    static let ShortBreakLeaveSuccessCode = NSNumber.init(value: 1007)
    static let ShortBreakHalfLeaveCode = NSNumber.init(value: 101)
    static let MileStone = NSNumber.init(value: 1)
    static let Achievements = NSNumber.init(value: 2)
    static let Activities = NSNumber.init(value: 3)
    static let Concerns = NSNumber.init(value: 4)
}
struct Device
{
    // iDevice detection code
    static let IS_IPAD             = UIDevice.current.userInterfaceIdiom == .pad
    static let IS_IPHONE           = UIDevice.current.userInterfaceIdiom == .phone
    static let IS_RETINA           = UIScreen.main.scale >= 2.0
    
    static let SCREEN_WIDTH        = Int(UIScreen.main.bounds.size.width)
    static let SCREEN_HEIGHT       = Int(UIScreen.main.bounds.size.height)
    static let SCREEN_MAX_LENGTH   = Int( max(SCREEN_WIDTH, SCREEN_HEIGHT))
    static let SCREEN_MIN_LENGTH   = Int( min(SCREEN_WIDTH, SCREEN_HEIGHT))
    
    static let IS_IPHONE_4_OR_LESS = IS_IPHONE && SCREEN_MAX_LENGTH  < 568
    static let IS_IPHONE_5         = IS_IPHONE && SCREEN_MAX_LENGTH == 568
    static let IS_IPHONE_6         = IS_IPHONE && SCREEN_MAX_LENGTH == 667
    static let IS_IPHONE_6P        = IS_IPHONE && SCREEN_MAX_LENGTH == 736
    static let IS_IPHONE_X         = IS_IPHONE && SCREEN_MAX_LENGTH == 812
    static let IS_IPAD_PRO         = IS_IPAD && SCREEN_MAX_LENGTH == 1366
    static let IS_IPAD_AIR         = IS_IPAD && SCREEN_MAX_LENGTH == 1024
    
    }
