//
//  GallryImageVC.swift
//  MuseumApp
//
//  Created by CCT Macmini on 26/07/19.
//  Copyright © 2019 CCT Macmini. All rights reserved.
//

import UIKit
import SDWebImage
import ISVImageScrollView

class GallryImageVC: UIViewController,UIScrollViewDelegate

{

    @IBOutlet weak var btnCross: UIButton!
    @IBOutlet weak var imgclose: UIImageView!
   // @IBOutlet weak var imgGallery: UIImageView!
    @IBOutlet weak var scrView: ISVImageScrollView!
    var strImg : String!
    var gestureRecognizer: UITapGestureRecognizer!
    var imgGallery:UIImageView = UIImageView()
    @IBOutlet weak var imgTopCons: NSLayoutConstraint!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        AppUtility.lockOrientation(.all)
        //scrView.minimumZoomScale = 1.0
        //scrView.maximumZoomScale = 10.0
        imgGallery.sd_setShowActivityIndicatorView(true)
        imgGallery.sd_setIndicatorStyle(UIActivityIndicatorView.Style.white)
       
        if strImg != nil
        {
            let urlString = strImg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            imgGallery.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in

            })
        }

        
        self.scrView.imageView = self.imgGallery
        self.scrView.maximumZoomScale = 3.0
        self.scrView.delegate = self
        
//        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTap))
//        doubleTap.numberOfTapsRequired = 2
//        scrView.addGestureRecognizer(doubleTap)
//        
        
          if UIDevice.current.userInterfaceIdiom == .pad
          {
            if UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft
            {
                print("Landscape Left")
                imgTopCons.constant = 0
                imgclose.isHidden = true
                btnCross.isHidden = true
                
            }
            else if UIDevice.current.orientation == UIDeviceOrientation.landscapeRight
            {
                print("Landscape Right")
                imgTopCons.constant = 0
                imgclose.isHidden = true
                btnCross.isHidden = true
            }
            else if UIDevice.current.orientation == UIDeviceOrientation.portraitUpsideDown
            {
                print("Portrait Upside Down")
                imgTopCons.constant = 120
                imgclose.isHidden = false
                btnCross.isHidden = false
            }
            else if UIDevice.current.orientation == UIDeviceOrientation.portrait
            {
                print("Portrait")
                imgTopCons.constant = 120
                imgclose.isHidden = false
                btnCross.isHidden = false
            }
        }
       
          else
          {
            if UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft
            {
                print("Landscape Left")
                imgTopCons.constant = 0
                imgclose.isHidden = true
                btnCross.isHidden = true
                
            }
            else if UIDevice.current.orientation == UIDeviceOrientation.landscapeRight
            {
                print("Landscape Right")
                imgTopCons.constant = 0
                imgclose.isHidden = true
                btnCross.isHidden = true
            }
            else if UIDevice.current.orientation == UIDeviceOrientation.portraitUpsideDown
            {
                print("Portrait Upside Down")
                imgTopCons.constant = 60
                imgclose.isHidden = false
                btnCross.isHidden = false
            }
            else if UIDevice.current.orientation == UIDeviceOrientation.portrait
            {
                print("Portrait")
                imgTopCons.constant = 60
                imgclose.isHidden = false
                btnCross.isHidden = false
            }
        }
        
     
      
    }
    
    
//    override var prefersStatusBarHidden: Bool {
//      return true
//    }

    
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator)
    {
        coordinator.animate(alongsideTransition: { context in
          
             if UIDevice.current.userInterfaceIdiom == .pad
             {
                if UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft
                {
                    print("Landscape Left")
                    self.imgTopCons.constant = 0
                    self.imgclose.isHidden = true
                    self.btnCross.isHidden = true
                 
                }
                else if UIDevice.current.orientation == UIDeviceOrientation.landscapeRight
                {
                    print("Landscape Right")
                    self.imgTopCons.constant = 0
                    self.imgclose.isHidden = true
                    self.btnCross.isHidden = true
                }
                else if UIDevice.current.orientation == UIDeviceOrientation.portraitUpsideDown
                {
                    print("Portrait Upside Down")
                    self.imgTopCons.constant = 120
                    self.imgclose.isHidden = false
                    self.btnCross.isHidden = false
                }
                else if UIDevice.current.orientation == UIDeviceOrientation.portrait
                {
                    print("Portrait")
                    self.imgTopCons.constant = 120
                    self.imgclose.isHidden = false
                    self.btnCross.isHidden = false
                }
            }
            
             else
             {
                if UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft
                {
                    print("Landscape Left")
                    self.imgTopCons.constant = 0
                    self.imgclose.isHidden = true
                    self.btnCross.isHidden = true
                    
                    
                    
                }
                else if UIDevice.current.orientation == UIDeviceOrientation.landscapeRight
                {
                    print("Landscape Right")
                    self.imgTopCons.constant = 0
                    self.imgclose.isHidden = true
                    self.btnCross.isHidden = true
                }
                else if UIDevice.current.orientation == UIDeviceOrientation.portraitUpsideDown
                {
                    print("Portrait Upside Down")
                    self.imgTopCons.constant = 60
                    self.imgclose.isHidden = false
                    self.btnCross.isHidden = false
                }
                else if UIDevice.current.orientation == UIDeviceOrientation.portrait
                {
                    print("Portrait")
                    self.imgTopCons.constant = 60
                    self.imgclose.isHidden = false
                    self.btnCross.isHidden = false
                }
            }
           
           
        })
    }
   

    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        // Don't forget to reset when view is being removed
        AppUtility.lockOrientation(.portrait)
    }
   
//    @objc func handleDoubleTap(_ gestureRecognizer: UIGestureRecognizer?)
//    {
//
//        if scrView.zoomScale > scrView.minimumZoomScale
//        {
//            scrView.setZoomScale(scrView.minimumZoomScale, animated: true)
//        }
//        else
//        {
//            scrView.setZoomScale(scrView.maximumZoomScale, animated: true)
//        }
//
//    }
  
    func viewForZooming(in scrollView: UIScrollView) -> UIView?
    {
        
        return imgGallery
    }
   
    @IBAction func btnClose(_ sender: Any)
    {
        
      self.navigationController?.popViewController(animated: false)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
