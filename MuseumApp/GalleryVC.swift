//
//  GalleryVC.swift
//  MuseumApp
//
//  Created by CCT Mini 2 on 10/04/19.
//  Copyright © 2019 CCT Macmini. All rights reserved.
//

import UIKit
import SDWebImage
class GalleryVC: UIViewController ,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate {

 
    var factSheetDict: Dictionary<String,Any>!
    
    var check: Bool!
    var videoDict: Dictionary<String,Any>!
    var kidsDict: Dictionary<String,Any>!
    var aboutUsDict: Dictionary<String,Any>!
    @IBOutlet weak var ArrowbtnNextPage: UIButton!
    @IBOutlet weak var ArrowbtnPrevPage: UIButton!
   // var scrview : UIScrollView!
  //  var cell: CollectionViewCellImages!
    
    
    
    
    @IBAction func actionBtnPriovious(_ sender: Any)
    {
        
        aboutUsDict = appDel.itemDetails["about"] as? Dictionary<String, Any>
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            if aboutUsDict["status"] as! String != "N"
            {
                
                   self.PUSH_STORY_R2(Identifier: "AboutVC")


            }
            else
            {
                   self.PUSH_STORY_R2(Identifier: "MenuVC")
            }
          
          
        }
        else
        {
            
            if aboutUsDict["status"] as! String != "N"
            {
                 self.PUSH_STORY_R2(Identifier: "AboutVC")

            }
            else
            {
                 self.PUSH_STORY_R2(Identifier: "MenuVC")
                
            }
         
            
        }
        
    }
    
    
    @IBAction func btnHomePress(_ sender: Any) {
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
           
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: MenuVC.self)
                {
                    _ =  self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
        else
        {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: MenuVC.self) {
                    _ =  self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        
    }
    }
    @IBAction func btnNextPage(_ sender: Any)
    {
     
        factSheetDict = appDel.itemDetails["fact_sheet"] as? Dictionary<String, Any>
        videoDict = appDel.itemDetails["video"] as? Dictionary<String, Any>
        kidsDict = appDel.itemDetails["kids"] as? Dictionary<String, Any>
        
        
        if factSheetDict["status"] as! String != "N"
        {
            if UIDevice.current.userInterfaceIdiom == .pad
            {
                PUSH_STORY_R1(Identifier: "FactSheetVC")
                return
            } else
            {
                PUSH_STORY_R1(Identifier: "FactSheetVC")
                return
            }
            
        }
       
        if videoDict["status"] as! String != "N"{
            if UIDevice.current.userInterfaceIdiom == .pad
            {
                PUSH_STORY_R1(Identifier: "VideosVC")
                return
            } else
            {
                PUSH_STORY_R1(Identifier: "VideosVC")
                return
            }}
        
        if kidsDict["status"] as! String != "N"
        {
            if UIDevice.current.userInterfaceIdiom == .pad
            {
                PUSH_STORY_R1(Identifier: "KidsVC")
                return
            } else
            {
                PUSH_STORY_R1(Identifier: "KidsVC")
                return
            }}
        
        
    }
    
    @IBOutlet weak var btnPriviousAction: UIButton!
    
    
    @IBOutlet weak var btnImagenext: UIButton!
    
    @IBOutlet weak var btnImagePrivious: UIButton!
    
    
    @IBAction func btnActionNext(_ sender: Any)
    {
      //  self.scrolview.minimumZoomScale = 1.0
      //  self.scrolview.maximumZoomScale = 4.0
       
        let visibleItems: NSArray = self.colView.indexPathsForVisibleItems as NSArray
        let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
        let nextItem: IndexPath = IndexPath(item: currentItem.item + 1, section: 0)
       
        if nextItem.row < imagesArray.count
        {
            self.colView.scrollToItem(at: nextItem, at: .left, animated: true)
            
            if nextItem.row == imagesArray.count - 1
            {
              btnImagenext.isHidden = true
            
            }
            else
            {
                btnImagePrivious.isHidden = false
                
            }
    }
    }
    
    @IBAction func btnActionPrivious(_ sender: Any)
    {
        let visibleItems: NSArray = self.colView.indexPathsForVisibleItems as NSArray
        let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
        let nextItem: IndexPath = IndexPath(item: currentItem.item - 1, section: 0)
        if nextItem.row < imagesArray.count && nextItem.row >= 0{
            self.colView.scrollToItem(at: nextItem, at: .right, animated: true)
            btnImagenext.isHidden = false
            if nextItem.row == 0
            {
                 btnImagePrivious.isHidden = true
                
            }
            
        }
    }
    
    @IBOutlet weak var dvNext: UIImageView!
    
    @IBOutlet weak var dvPrivious: UIImageView!
    @IBOutlet weak var NochView: UIView!
    @IBOutlet weak var bottomView: UIView!
    
    
    
    @IBOutlet weak var lblTitle: UILabel!
    var Gallery: Dictionary<String,Any>!
    
    @IBOutlet weak var imgViewBackground: UIImageView!
    
    
    @IBOutlet weak var lblDescription: UILabel!
    
     var appDel: AppDelegate!
    @IBOutlet weak var imageGallery: UIImageView!
    
    @IBOutlet weak var colView: UICollectionView!
    
    @IBOutlet weak var btnPrevPage: UIButton!
    
    var imagesArray = Array<Any>()
    @IBOutlet weak var btnNextPage: UIButton!
    
    @IBOutlet weak var btnHomePage: UIButton!
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        check = true
        if(appDel == nil)
        {
            appDel = UIApplication.shared.delegate as? AppDelegate
        }
      
        if let layout = colView.collectionViewLayout as? UICollectionViewFlowLayout {
        layout.scrollDirection = .horizontal  // .horizontal
        }
    
      
   //     colView.register(CollectionViewCellImages.self, forCellWithReuseIdentifier: "cell")
       
        
        factSheetDict = appDel.itemDetails["fact_sheet"] as? Dictionary<String, Any>
        videoDict = appDel.itemDetails["video"] as? Dictionary<String, Any>
        kidsDict = appDel.itemDetails["kids"] as? Dictionary<String, Any>
      
        if factSheetDict["status"] as! String != "N"
        {
            check = checkForHiddenButton(_check: check)
        }
        
        if videoDict["status"] as! String != "N"
        {
            check = checkForHiddenButton(_check: check)
        }
        
        
        if kidsDict["status"] as! String != "N"
        {
            check = checkForHiddenButton(_check: check)
        }
       
        if check == true
        {
            btnNextPage.isHidden = true
            
        }
        btnImagePrivious.isHidden = true
        Gallery = appDel.itemDetails["gallery"] as? Dictionary<String, Any>
        imagesArray = Gallery["gallery_files"]  as? Array<Any> ?? [""]
       
        if imagesArray.count == 1
        {
            btnImagenext.isHidden = true
        }
     
        if UIDevice.current.userInterfaceIdiom == .pad
        {

            let Gallery1 = appDel.itemDetails["ipad_background_image"] as! String
            if Gallery1 != nil
            {
                let urlString = Gallery1.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                imgViewBackground.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in

                })
            }


        }else{
            let Gallery1 = appDel.itemDetails["iphone_background_image"] as! String
            if Gallery1 != nil
            {
                let urlString = Gallery1.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                imgViewBackground.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in

                })
            }
        }
        self.ConfigInformation()
        self.nextButtonText()
        self.PrevButtonText()
    }
  
    func nextButtonText()
    {
        let colorValue_button: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["tab_color"] as! String)
        self.btnNextPage.setTitleColor(colorValue_button, for: .normal)
      
        if factSheetDict["status"] as! String != "N"
        {
            let str = factSheetDict["title"] as? String
            self.btnNextPage.setTitle(str!, for: UIControl.State.normal)
             ArrowbtnNextPage.isHidden = false;
            //self.btnNextPage.setTitle("Facts ", for: UIControl.State.normal)
            return
        }
        else
        {
             ArrowbtnNextPage.isHidden = true;
        }
        if videoDict["status"] as! String != "N"
        {
            let str = videoDict["title"] as? String
            self.btnNextPage.setTitle(str!, for: UIControl.State.normal)
             ArrowbtnNextPage.isHidden = false;
            //self.btnNextPage.setTitle("Videos ", for: UIControl.State.normal)
            return
        }
        else
        {
             ArrowbtnNextPage.isHidden = true;
        }
        if kidsDict["status"] as! String != "N"
        {
            let str = kidsDict["title"] as? String
            self.btnNextPage.setTitle(str!, for: UIControl.State.normal)
             ArrowbtnNextPage.isHidden = false;
          //  self.btnNextPage.setTitle("Kids ", for: UIControl.State.normal)
            return
        }
        else
        {
             ArrowbtnNextPage.isHidden = true;
        }
    }
    
    func PrevButtonText()
    {
        let colorValue_button: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["tab_color"] as! String)
        self.btnPrevPage.setTitleColor(colorValue_button, for: .normal)
      
        aboutUsDict = appDel.itemDetails["about"] as? Dictionary<String, Any>
       
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            if aboutUsDict["status"] as! String != "N"
            {
                let str = aboutUsDict["title"] as? String
                self.btnPrevPage.setTitle(str!, for: UIControl.State.normal)
               // self.btnPrevPage.setTitle("About ", for: UIControl.State.normal)
                return
               // self.PUSH_STORY_R2(Identifier: "AboutVC")
            }
            else
            {
                
                self.btnPrevPage.setTitle("", for: UIControl.State.normal)
                return
              //  self.PUSH_STORY_R2(Identifier: "MenuVC")
            }
        }
        
        else
        {
            
            if aboutUsDict["status"] as! String != "N"
            {
                let str = aboutUsDict["title"] as? String
                self.btnPrevPage.setTitle(str!, for: UIControl.State.normal)
                //self.btnPrevPage.setTitle("About ", for: UIControl.State.normal)
                return
                //self.PUSH_STORY_R2(Identifier: "AboutVC")
                
            }
            else
            {
                self.btnPrevPage.setTitle("", for: UIControl.State.normal)
                return
              //  self.PUSH_STORY_R2(Identifier: "MenuVC")
                
            }
            
            
        }
    }
    
        func ConfigInformation()
        {
            
            //Title Heading Text set
            
            lblTitle.text = appDel.itemDetails["title"] as? String
            lblTitle.textColor = AppDelegate.sharedInstance().hexStringToUIColor(hex : appDel.itemDetails["title_colour"] as! String)
            
            //Title Video Text set
            self.lblDescription.text = Gallery["title"] as? String
            self.lblDescription.textColor = AppDelegate.sharedInstance().hexStringToUIColor(hex : Gallery["color"] as! String)

            var videoLogo : String!
            
            if UIDevice.current.userInterfaceIdiom == .pad
            {
                videoLogo = Gallery["ipad_icon_image"] as? String
            }
            else
            {
                videoLogo = Gallery["iphone_icon_image"] as? String
            }
            
            if videoLogo != nil
            {
                let urlString = videoLogo.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                imageGallery.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    
                })
            }
            
            //home button
            
            let colorValue_button: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["tab_color"] as! String)
            self.btnHomePage.setTitleColor(colorValue_button, for: .normal)
            self.btnHomePage.setTitle("Home", for: UIControl.State.normal)
            
            //bottomView Theme Color set
            
            let colorValue_BottomView : UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["theme_color"] as! String)
            self.bottomView.backgroundColor = colorValue_BottomView
            
            //NochView Theme Color set
            let colorValue_NochView : UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["theme_color"] as! String)
            self.NochView.backgroundColor = colorValue_NochView
            
            var tabBarImg1 : String!
            var tabBarImg2 : String!
            var tabBarImg3 : String!
            
            if UIDevice.current.userInterfaceIdiom == .pad
            {
                tabBarImg1 = self.appDel.configDict["ipad_bottom_icon_1"] as? String
                tabBarImg2 = self.appDel.configDict["ipad_bottom_icon_2"] as? String
                tabBarImg3 = self.appDel.configDict["ipad_bottom_icon_3"] as? String
            }
            else
            {
                tabBarImg1 = self.appDel.configDict["ipad_bottom_icon_1"] as? String
                tabBarImg2 = self.appDel.configDict["ipad_bottom_icon_2"] as? String
                tabBarImg3 = self.appDel.configDict["ipad_bottom_icon_3"] as? String
            }
            
            //btn Prev page
            if tabBarImg1 != nil
            {
//                let urlString = tabBarImg1.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//                self.btnPrevPage.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
//                    , completed: { (image, error, cacheType, imageURL) in
//                        self.btnPrevPage.backgroundImage(for: UIControl.State.normal)
//                })
            }
            
            //btn Next page
            if tabBarImg3 != nil
            {
//                let urlString = tabBarImg3.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//                self.btnNextPage.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
//                    , completed: { (image, error, cacheType, imageURL) in
//                        self.btnNextPage.backgroundImage(for: UIControl.State.normal)
//                })
            }
            
            //btn Home page
            if tabBarImg2 != nil
            {
//                let urlString = tabBarImg2.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//                self.btnHomePage.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
//                    , completed: { (image, error, cacheType, imageURL) in
//                        self.btnHomePage.backgroundImage(for: UIControl.State.normal)
//                })
            }
            
            //Devider Next/Prev image set
            
            let colorValue_devColor: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["tab_color"] as! String)
            dvNext.backgroundColor = colorValue_devColor
            dvPrivious.backgroundColor = colorValue_devColor
            //BackGround ImgSet
         
        }
        
        
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.imagesArray.count
    }
    
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = colView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! CollectionViewCellImages
        var dict: Dictionary = imagesArray[indexPath.row] as! Dictionary<String,Any>
       
//        cell.scrolview.delegate = self
//        cell.scrolview.minimumZoomScale = 1.0;
//        cell.scrolview.maximumZoomScale = 3.0
//        cell.scrolview.zoomScale = 1.0
       
        let Gallery1 = dict["file_name"] as! String
        cell.images.sd_setShowActivityIndicatorView(true)
        cell.images.sd_setIndicatorStyle(UIActivityIndicatorView.Style.white)
        if Gallery1 != nil
        {
            let urlString = Gallery1.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
           cell.images.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                
            })
        }

            cell.tag = indexPath.row
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.didSelectItemTapped(tap: )))
            tap.view?.tag = indexPath.row;
            cell.addGestureRecognizer(tap)
            tap.cancelsTouchesInView = false
    
        return cell
    }
    
    
//    @IBAction func scaleImage(_ sender: UIPinchGestureRecognizer)
//    {
//        cell.images.transform = CGAffineTransform(scaleX: sender.scale, y: sender.scale)
//    }
//
//    func viewForZooming(in scrollView: UIScrollView) -> UIView?
//    {
//        return cell.images
//    }


   @objc  func didSelectItemTapped(tap:UITapGestureRecognizer){


       var dict: Dictionary = imagesArray[tap.view!.tag] as! Dictionary<String,Any>
       print("Gopi Gopi....")

       if UIDevice.current.userInterfaceIdiom == .pad
       {
           let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
           let imgvc = storyboard.instantiateViewController(withIdentifier: "GallryImageVC") as! GallryImageVC
           imgvc.strImg = dict["file_name"] as? String
           self.navigationController?.pushViewController(imgvc, animated: false)
       }
       else
       {

           let storyboard = UIStoryboard(name: "Main", bundle: nil)
           let imgvc = storyboard.instantiateViewController(withIdentifier: "GallryImageVC") as! GallryImageVC
           imgvc.strImg = dict["file_name"] as? String
           self.navigationController?.pushViewController(imgvc, animated: false)
       }


    }
    // MARK: - UICollectionViewDelegate protocol

  /*  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        // handle tap events
     //   print("You selected cell #\(indexPath.item)!")
        
        var dict: Dictionary = imagesArray[indexPath.row] as! Dictionary<String,Any>
        print("Gopi Gopi....")
   
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
            let imgvc = storyboard.instantiateViewController(withIdentifier: "GallryImageVC") as! GallryImageVC
            imgvc.strImg = dict["file_name"] as? String
            self.navigationController?.pushViewController(imgvc, animated: false)
        }
        else
        {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let imgvc = storyboard.instantiateViewController(withIdentifier: "GallryImageVC") as! GallryImageVC
            imgvc.strImg = dict["file_name"] as? String
            self.navigationController?.pushViewController(imgvc, animated: false)
        }
    
    }*/

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let padding: CGFloat =  5
        let collectionViewSize = collectionView.frame.size.width - padding
        let collectionViewSizeHeight = collectionView.frame.size.height - padding
        return CGSize(width: collectionViewSize/1, height: collectionViewSizeHeight/1)
        
    }
    
    func scrollViewDidEndDecelerating(_ scrview: UIScrollView)
    {
        let contentOffsetX = scrview.contentOffset.x
        if contentOffsetX == 0.0
        {
            btnImagePrivious.isHidden = true
        }
        else
        {
            btnImagePrivious.isHidden = false
        }
        
        if colView.contentSize.width <= contentOffsetX + colView.frame.width
        {
            btnImagenext.isHidden = true
        }
        else
        {
            btnImagenext.isHidden = false
        }
    }
    func checkForHiddenButton(_check : Bool) -> Bool {
        check = false
        return check
    }
    
}
