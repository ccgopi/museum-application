//
//  GalleryDetailVC.swift
//  MuseumApp
//
//  Created by CCT Macmini on 25/07/19.
//  Copyright © 2019 CCT Macmini. All rights reserved.
//

import UIKit

class GalleryDetailVC: UIViewController , UIScrollViewDelegate
{

    @IBOutlet weak var btncross: UIButton!
    @IBOutlet weak var imgGallery: UIImageView!
    @IBOutlet weak var scrView: UIScrollView!
  
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        scrView.minimumZoomScale = 1.0
        scrView.maximumZoomScale = 10.0
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView?
    {
        
        return imgGallery
    }
    

   
    @IBAction func btnClose(_ sender: Any)
    {
        
    }
    
}
