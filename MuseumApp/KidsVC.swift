//
//  KidsVC.swift
//  MuseumApp
//
//  Created by CCT Macmini on 03/01/19.
//  Copyright © 2019 CCT Macmini. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire

class KidsVC: UIViewController
{
  
    
    @IBOutlet weak var lblTitleWithIamge: UILabel!
    
    @IBOutlet weak var blurViewTopCons: NSLayoutConstraint!
    @IBOutlet weak var btnNextPage: UIButton!
    @IBOutlet weak var btnPrevPage: UIButton!
    @IBOutlet weak var btnHomePage: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var DevPrevImg: UIImageView!
    @IBOutlet weak var DevNextImg: UIImageView!
    @IBOutlet weak var backImg: UIImageView!
    @IBOutlet weak var NochView: UIView!
    @IBOutlet var tblVideo: UITableView!
    @IBOutlet weak var lblTitleName: UILabel!
    @IBOutlet weak var lblTitleKides: UILabel!
    @IBOutlet weak var kidsImg: UIImageView!
    @IBOutlet weak var btnQuesPrev: UIButton!
    @IBOutlet weak var btnQuesNext: UIButton!
    @IBOutlet weak var lblAnswer: UILabel!
    @IBOutlet weak var lblTotalQues: UILabel!
    @IBOutlet weak var ArrowbtnNextPage: UIButton!
    @IBOutlet weak var ArrowbtnPrevPage: UIButton!

    @IBOutlet weak var hight4: NSLayoutConstraint!
    @IBOutlet weak var widthButton1: NSLayoutConstraint!
    @IBOutlet weak var widthButton2: NSLayoutConstraint!
    @IBOutlet weak var iPadWidthbtn3: NSLayoutConstraint!
    @IBOutlet weak var IpadHightlblQuestion: NSLayoutConstraint!
    @IBOutlet weak var iPadhightBtn4: NSLayoutConstraint!
    @IBOutlet weak var iPadHightbtn3: NSLayoutConstraint!
    @IBOutlet weak var iPadHightbtn4: NSLayoutConstraint!
    @IBOutlet weak var iPadWidthbtn4: NSLayoutConstraint!
    @IBOutlet weak var iPadQuestionImageHight: NSLayoutConstraint!
    @IBOutlet weak var iPadWidthbtn1: NSLayoutConstraint!
    @IBOutlet weak var iPadWidthbtn2: NSLayoutConstraint!
    @IBOutlet weak var iPadHightbtn1: NSLayoutConstraint!
    @IBOutlet weak var hightbtnIpad2: NSLayoutConstraint!
    @IBOutlet weak var widthButton3: NSLayoutConstraint!
    @IBOutlet weak var widthButton4: NSLayoutConstraint!
    
    var appDel: AppDelegate!
    var checkAns: Bool!
    var kidsDict: Dictionary<String,Any>!
    var arrayQuestion: Array<Any>!
    var questionNumber : Int!
    
    @IBOutlet weak var hightImageQuestion: NSLayoutConstraint!
    
    @IBOutlet weak var lblHight: NSLayoutConstraint!
    
    @IBOutlet weak var btnAnswer1: UIButton!
    @IBOutlet weak var btnAnswer2: UIButton!
    @IBOutlet weak var btnAnswer3: UIButton!
    @IBOutlet weak var btnAnswer4: UIButton!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var btnDisplayCorrectAnswer: UILabel!
    
    
    @IBOutlet weak var btnAns1Hight: NSLayoutConstraint!
    @IBOutlet weak var btnAns2Hight: NSLayoutConstraint!
    @IBOutlet weak var btnAns3Hight: NSLayoutConstraint!
    @IBOutlet weak var btnAns4Hight: UIButton!
    var factSheetDict: Dictionary<String,Any>!
    var aboutUsDict: Dictionary<String,Any>!
    var Gallery: Dictionary<String,Any>!
    var videoDict: Dictionary<String,Any>!
    var yourArray = Array<Any>()
    var CountTrue: Int!
    @IBOutlet weak var quesImageView: UIImageView!
    
    
    
    
    
    
   
    @IBAction func btnNextPage(_ sender: Any)
    {
//        if UIDevice.current.userInterfaceIdiom == .pad
//        {
//            let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
//            let Trail = storyboard.instantiateViewController(withIdentifier: "TrailVC") as! TrailVC
//            self.navigationController?.pushViewController(Trail, animated: false)
//        }
//        else
//        {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let Trail = storyboard.instantiateViewController(withIdentifier: "TrailVC") as! TrailVC
//            self.navigationController?.pushViewController(Trail, animated: false)
//        }
    }
    
    @IBAction func btnPrevPage(_ sender: Any)
    {
        factSheetDict = appDel.itemDetails["fact_sheet"] as? Dictionary<String, Any>
        aboutUsDict = appDel.itemDetails["about"] as? Dictionary<String, Any>
        Gallery = appDel.itemDetails["gallery"] as? Dictionary<String, Any>
        videoDict = appDel.itemDetails["video"] as? Dictionary<String, Any>
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            if videoDict["status"] as! String != "N"
            {
               self.PUSH_STORY_R2(Identifier: "VideosVC")
            }
            else if factSheetDict["status"] as! String != "N"
            {
                self.PUSH_STORY_R2(Identifier: "FactSheetVC")
            }
            else if Gallery["status"] as! String != "N"
            {
                 self.PUSH_STORY_R2(Identifier: "GalleryVC1")
            }
            else if aboutUsDict["status"] as! String != "N"
            {
                 self.PUSH_STORY_R2(Identifier: "AboutVC")
            }
           else
            {
              self.PUSH_STORY_R2(Identifier: "MenuVC")
            }
            
            
    
        }
        else
        {
            
            if videoDict["status"] as! String != "N"
            {
                self.PUSH_STORY_R2(Identifier: "VideosVC")
            }
            else if factSheetDict["status"] as! String != "N"
            {
                self.PUSH_STORY_R2(Identifier: "FactSheetVC")
            }
            else if Gallery["status"] as! String != "N"
            {
                self.PUSH_STORY_R2(Identifier: "idGallery")
            }
            else if aboutUsDict["status"] as! String != "N"
            {
                self.PUSH_STORY_R2(Identifier: "AboutVC")
            }
            else
            {
            self.PUSH_STORY_R2(Identifier: "MenuVC")
            }
       
    }
    }
    
    
     func PrevButtonText()
     {
        let colorValue_button: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["tab_color"] as! String)
        self.btnPrevPage.setTitleColor(colorValue_button, for: .normal)
  
        factSheetDict = appDel.itemDetails["fact_sheet"] as? Dictionary<String, Any>
        aboutUsDict = appDel.itemDetails["about"] as? Dictionary<String, Any>
        Gallery = appDel.itemDetails["gallery"] as? Dictionary<String, Any>
        videoDict = appDel.itemDetails["video"] as? Dictionary<String, Any>
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            if videoDict["status"] as! String != "N"
            {
                let str = videoDict["title"] as? String
                self.btnPrevPage.setTitle(str!, for: UIControl.State.normal)
                //self.btnPrevPage.setTitle("Videos ", for: UIControl.State.normal)
                return
               
            }
            else if factSheetDict["status"] as! String != "N"
            {
                let str = factSheetDict["title"] as? String
                self.btnPrevPage.setTitle(str!, for: UIControl.State.normal)
               // self.btnPrevPage.setTitle("Fact sheet ", for: UIControl.State.normal)
                return
            }
            else if Gallery["status"] as! String != "N"
            {
                let str = Gallery["title"] as? String
                self.btnPrevPage.setTitle(str!, for: UIControl.State.normal)
              //  self.btnPrevPage.setTitle("Gallery ", for: UIControl.State.normal)
                 return
               
            }
            else if aboutUsDict["status"] as! String != "N"
            {
                let str = aboutUsDict["title"] as? String
                self.btnPrevPage.setTitle(str!, for: UIControl.State.normal)
               // self.btnPrevPage.setTitle("About ", for: UIControl.State.normal)
                return
               
            }
            else
            {
                self.btnPrevPage.setTitle("Menu ", for: UIControl.State.normal)
                return
               
            }
            
            
        }
        else
        {
            
            if videoDict["status"] as! String != "N"
            {
                let str = videoDict["title"] as? String
                self.btnPrevPage.setTitle(str!, for: UIControl.State.normal)
               // self.btnPrevPage.setTitle("Videos ", for: UIControl.State.normal)
                return
            }
            else if factSheetDict["status"] as! String != "N"
            {
                let str = factSheetDict["title"] as? String
                self.btnPrevPage.setTitle(str!, for: UIControl.State.normal)
               // self.btnPrevPage.setTitle("Fact sheet ", for: UIControl.State.normal)
                return
            }
            else if Gallery["status"] as! String != "N"
            {
                let str = Gallery["title"] as? String
                self.btnPrevPage.setTitle(str!, for: UIControl.State.normal)
              //  self.btnPrevPage.setTitle("Gallery ", for: UIControl.State.normal)
                return
            }
            else if aboutUsDict["status"] as! String != "N"
            {
                let str = aboutUsDict["title"] as? String
                self.btnPrevPage.setTitle(str!, for: UIControl.State.normal)
               // self.btnPrevPage.setTitle("About ", for: UIControl.State.normal)
                return
            }
            else
            {
                self.btnPrevPage.setTitle("", for: UIControl.State.normal)
                return
            }
            
        }
    
    }
    
    
    @IBAction func btnHomePage(_ sender: Any)
    {
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: MenuVC.self) {
                    _ =  self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
//            let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
//            let FactSheet = storyboard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
//            self.navigationController?.pushViewController(FactSheet, animated: false)
        }
        else
        {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: MenuVC.self) {
                    _ =  self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let FactSheet = storyboard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
//            self.navigationController?.pushViewController(FactSheet, animated: false)
        }
    
    }
    @IBAction func btnQuesPrev(_ sender: Any)
    {
         questionNumber = questionNumber - 1
        
        let appendString5 = "["
        let appendString1 = "\(questionNumber + 1)"
        let appendString2 = "out of"
        let appendString3 =  "\(arrayQuestion.count)"
        let appendString6 = "]"
        let appendString4 = "\(appendString5) \(appendString1) \(appendString2) \(appendString3) \(appendString6)"
        lblTotalQues.text = appendString4
        
        
        if questionNumber == 0
        {
            btnQuesPrev.isHidden = true;
            btnQuesNext.isHidden = false;
        }
        else
        {
            btnQuesPrev.isHidden = false;
            btnQuesNext.isHidden = false;
        }
      
        if questionNumber < 0
        {
            print("Please Scan Barcode");
            questionNumber = 0;
            btnQuesPrev.isHidden = true
            let alert = UIAlertController(title: Constant.alertTitle, message: "Please press next button for next questions", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
           self.present(alert, animated: true)
           return;
        }
        else
        {
            
             self.setQuestionAnswer(hex: questionNumber)
            
        }
    }
    
    @IBAction func btnQuesNext(_ sender: Any)
    {
        questionNumber = questionNumber + 1
        btnQuesPrev.isHidden = false;
        let appendString5 = "["
        let appendString1 = "\(questionNumber + 1)"
        let appendString2 = "out of"
        let appendString3 =  "\(arrayQuestion.count)"
        let appendString6 = "]"
        let appendString4 = "\(appendString5) \(appendString1) \(appendString2) \(appendString3) \(appendString6)"
        lblTotalQues.text = appendString4
        
        
         if questionNumber == arrayQuestion.count - 1
        {
            btnQuesNext.isHidden = true;
        }
        else
        {
            btnQuesNext.isHidden = false;
            btnQuesPrev.isHidden = false;
        }
        
        if questionNumber >= arrayQuestion.count
         {
            questionNumber = arrayQuestion.count
            
           print("Please Scan Barcode");
           let alert = UIAlertController(title: Constant.alertTitle, message: "ohhhh!!you have done the question list", preferredStyle: .alert)
          alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
           self.present(alert, animated: true)
            return;
            
          }
        
         else
         {
            
            self.setQuestionAnswer(hex: questionNumber)
         }
        
//        let strmsgAppend = " Correct answers given"
//        let strAppend2 = "( "
//        let msg = String(CountTrue)
//        let strAppend3 = " correct out of "
//        let appendStringcount =  "\(arrayQuestion.count)"
//        let strAppend4 = " )"
//        // let appendString6 = "]"
//        let strMsg = strmsgAppend + strAppend2 + msg + strAppend3 + appendStringcount + strAppend4
//        var qusNo = questionNumber + 1
//        print(qusNo)
//        if qusNo >= arrayQuestion.count{
//            if checkAns == true{
//                Alert.showAlert(title: Constant.alertTitle , message: strMsg)
//            }
//        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        btnQuesPrev.isHidden = true;
        CountTrue = 0
        checkAns = false
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated) // No need for semicolon
        
        if(appDel == nil)
        {
            appDel = UIApplication.shared.delegate as? AppDelegate
        }
        kidsDict = appDel.itemDetails["kids"] as? Dictionary<String, Any>
        arrayQuestion = kidsDict["kids_questions"] as? Array<Any>
        self.BottomBarButtonHideShowMethod()
        self.ConfigInformation()
        self.PrevButtonText()
        questionNumber = 0;
        
        if arrayQuestion.count > 0
        {
            lblTotalQues.isHidden = false
            //btnQuesPrev.isHidden = false;
            btnQuesNext.isHidden = false;
            self.setButtonConfig()
            self.setQuestionAnswer(hex: 0)
        }
        else
        {
            lblTotalQues.isHidden = true
            btnQuesPrev.isHidden = true;
            btnQuesNext.isHidden = true;
        }
        
//        if questionNumber == 0
//        {
//            btnQuesPrev.isHidden = true;
//            btnQuesNext.isHidden = true;
//        }
//        else
//        {
//            btnQuesPrev.isHidden = false;
//            btnQuesNext.isHidden = false;
//        }
        
        if arrayQuestion.count == 1
        {
            btnQuesPrev.isHidden = true;
            btnQuesNext.isHidden = true;
        }
        
        let appendString5 = "["
        let appendString1 = "\(questionNumber + 1)"
        let appendString2 = "out of"
        let appendString3 =  "\(arrayQuestion.count)"
        let appendString6 = "]"
        let appendString4 = "\(appendString5) \(appendString1) \(appendString2) \(appendString3) \(appendString6)"
        lblTotalQues.text = appendString4
    }
    
    func BottomBarButtonHideShowMethod()
    {
        btnNextPage.isHidden = true
        btnNextPage.isEnabled = true
        btnPrevPage.isHidden = false
        btnPrevPage.isEnabled = true
        btnHomePage.isHidden = false
        btnHomePage.isEnabled = true
        DevPrevImg.isHidden = false
        DevNextImg.isHidden = false
    }
    
    func ConfigInformation()
    {
        
        //bottomView Theme Color set
        
        lblTitleName.text = appDel.itemDetails["title"] as? String
        lblTitleName.textColor = AppDelegate.sharedInstance().hexStringToUIColor(hex : appDel.itemDetails["title_colour"] as! String)
        
        lblTitleKides.text = kidsDict["title"] as? String
        lblTitleKides.textColor = AppDelegate.sharedInstance().hexStringToUIColor(hex : kidsDict["color"] as! String)
        
        //NochView Theme Color set
        let colorValue_NochView : UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["theme_color"] as! String)
        self.NochView.backgroundColor = colorValue_NochView
        
        //Home
        
        let colorValue_button: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["tab_color"] as! String)
        self.btnHomePage.setTitleColor(colorValue_button, for: .normal)
        self.btnHomePage.setTitle("Home", for: UIControl.State.normal)
        
        //Devider Next/Prev image set
        
        let colorValue_devColor: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["tab_color"] as! String)
        DevNextImg.backgroundColor = colorValue_devColor
        DevPrevImg.backgroundColor = colorValue_devColor
        var kidsLogo : String!
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            kidsLogo = kidsDict["ipad_icon_image"] as? String
        }
        else
        {
            kidsLogo = kidsDict["iphone_icon_image"] as? String
        }
        
        if kidsLogo != nil
        {
            let urlString = kidsLogo.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            kidsImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                
            })
        }
        
        let colorValue_BottomView : UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["theme_color"] as! String)
        self.bottomView.backgroundColor = colorValue_BottomView
        
        
        var tabBarImg1 : String!
        var tabBarImg2 : String!
        var tabBarImg3 : String!
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            tabBarImg1 = self.appDel.configDict["ipad_bottom_icon_1"] as? String
            tabBarImg2 = self.appDel.configDict["ipad_bottom_icon_2"] as? String
            tabBarImg3 = self.appDel.configDict["ipad_bottom_icon_3"] as? String
            
        }
        else
        {
            tabBarImg1 = self.appDel.configDict["ipad_bottom_icon_1"] as? String
            tabBarImg2 = self.appDel.configDict["ipad_bottom_icon_2"] as? String
            tabBarImg3 = self.appDel.configDict["ipad_bottom_icon_3"] as? String
            
        }
        
        //btn Prev page
        if tabBarImg1 != nil
        {
//            let urlString = tabBarImg1.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//            self.btnPrevPage.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
//                , completed: { (image, error, cacheType, imageURL) in
//                    self.btnPrevPage.backgroundImage(for: UIControl.State.normal)
//            })
        }
        
        //btn Next page
        if tabBarImg3 != nil
        {
//            let urlString = tabBarImg3.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//            self.btnNextPage.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
//                , completed: { (image, error, cacheType, imageURL) in
//                    self.btnNextPage.backgroundImage(for: UIControl.State.normal)
//            })
        }
        
        //btn Home page
        if tabBarImg2 != nil
        {
//            let urlString = tabBarImg2.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//            self.btnHomePage.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
//                , completed: { (image, error, cacheType, imageURL) in
//                    self.btnHomePage.backgroundImage(for: UIControl.State.normal)
//            })
//
        }
        
        //BackGround ImgSet
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            let bannerImgStr = appDel.itemDetails["ipad_background_image"] as! String
            
            if bannerImgStr != nil
            {
                let urlString = bannerImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                self.backImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    
                })
            }
        }
        else
        {
            let bannerImgStr = appDel.itemDetails["iphone_background_image"] as! String
            if bannerImgStr != nil
            {
                let urlString = bannerImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                self.backImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    
                })
            }
        }
    }
    
    func setQuestionAnswer(hex:Int)
    {
        questionNumber = hex
        var dict: Dictionary = arrayQuestion[hex] as! Dictionary<String,Any>
        var arrayAnswer = dict["kids_questions_answers"] as! Array<Any>
        
        var indexRightAnsNum : Int!
        
        
        for i in 0..<arrayAnswer.count
        {
            var dictAns: Dictionary = arrayAnswer[i] as! Dictionary<String,Any>
            
            if dictAns["is_correct"] as! Bool
            {
                indexRightAnsNum = i;
            }
            
            if i == 0
            {
                if dict["question_answer_type"] as? Int == 1   {
                    if UIDevice.current.userInterfaceIdiom == .pad{
                        iPadHightbtn1.constant = 90
                        iPadWidthbtn1.constant = 500
                    }else{
                        btnAns1Hight.constant = 65
                        widthButton1.constant = 200
                    }
                    btnAnswer1.sd_setBackgroundImage(with:URL.init(string: "" ) , for: .normal) { (image, url, cache, urlimage) in
                        self.btnAnswer1.sd_backgroundImageURL(for: .normal)
                    }
                    btnAnswer1.sd_backgroundImageURL(for: .normal)
                    btnAnswer1.setTitle(dictAns["answer"] as? String,for: .normal)
                  
                    
                    let hight = self.SetButtonHight(btn: btnAnswer1 , str: dictAns["answer"] as! String )
                    
                    if UIDevice.current.userInterfaceIdiom == .pad{
                        if hight >= 75.0{
                            iPadHightbtn1.constant = hight + 75
                        }
                    }else{
                        if hight >= 50.0{
                            btnAns1Hight.constant = hight + 60
                        }
                    }
                    
                    
                }else if dict["question_answer_type"] as? Int ==  3{
                    if UIDevice.current.userInterfaceIdiom == .pad{
                        iPadHightbtn1.constant = 85
                        iPadWidthbtn1.constant = 500
                    }else{
                        btnAns1Hight.constant = 65
                        widthButton1.constant = 200
                    }
                    
                    //                    iPadHightbtn1.constant = 75
                    //                    iPadWidthbtn1.constant = 215
                    btnAnswer1.sd_setBackgroundImage(with:URL.init(string: "" ) , for: .normal) { (image, url, cache, urlimage) in
                        self.btnAnswer1.sd_backgroundImageURL(for: .normal)
                    }
                    btnAnswer1.sd_backgroundImageURL(for: .normal)
                    btnAnswer1.setTitle(dictAns["answer"] as? String,for: .normal)
                    let hight = self.SetButtonHight(btn: btnAnswer1 , str: dictAns["answer"] as! String )
                    lblTitleWithIamge.isHidden = true
                    
                    if UIDevice.current.userInterfaceIdiom == .pad{
                        if hight >= 75.0{
                            iPadHightbtn1.constant = hight + 75
                        }
                    }else{
                        if hight >= 50.0{
                            btnAns1Hight.constant = hight + 60
                        }
                    }
                    
                }
                else{
                    if UIDevice.current.userInterfaceIdiom == .pad{
                        iPadHightbtn1.constant = 180
                        iPadWidthbtn1.constant = 360
                    }else{
                        btnAns1Hight.constant = 100
                        widthButton1.constant = 200
                    }
                    //                    iPadHightbtn1.constant = 180
                    //                    iPadWidthbtn1.constant = 360
                    btnAnswer1.sd_setBackgroundImage(with:URL.init(string: dictAns["answer"] as? String ?? "" ) , for: .normal) { (image, url, cache, urlimage) in
                        self.btnAnswer1.sd_backgroundImageURL(for: .normal)
                    }
                    lblTitleWithIamge.isHidden = false
                     lblTitleWithIamge.text = ""
                    btnAnswer1.sd_backgroundImageURL(for: .normal)
                    //self.SetWhiteBorder(button_: btnAnswer1)
                    btnAnswer1.setTitle("" ,for: .normal)
                }
                
            }
            else if i == 1
            {
                if dict["question_answer_type"] as? Int == 1  {
                    if UIDevice.current.userInterfaceIdiom == .pad{
                        hightbtnIpad2 .constant = 85
                        iPadWidthbtn2.constant = 500
                    }else{
                        btnAns2Hight.constant = 65
                        widthButton2.constant = 200
                    }
                    //
                    btnAnswer2.sd_setBackgroundImage(with:URL.init(string: "" ) , for: .normal) { (image, url, cache, urlimage) in
                        self.btnAnswer2.sd_backgroundImageURL(for: .normal)
                    }
                    btnAnswer2.sd_backgroundImageURL(for: .normal)
                    btnAnswer2.setTitle(dictAns["answer"] as? String,for: .normal)
                    let hight = self.SetButtonHight(btn: btnAnswer2 , str: dictAns["answer"] as! String )
                    
                    if UIDevice.current.userInterfaceIdiom == .pad{
                        if hight >= 75.0{
                            hightbtnIpad2.constant = hight + 75
                        }
                    }else{
                        if hight >= 50.0{
                            btnAns2Hight.constant = hight + 60
                        }
                    }
                    
                } else if dict["question_answer_type"] as? Int ==  3{
                    if UIDevice.current.userInterfaceIdiom == .pad{
                        hightbtnIpad2 .constant = 85
                        iPadWidthbtn2.constant = 500
                    }else{
                        btnAns2Hight.constant = 65
                        widthButton2.constant = 200
                    }
                    btnAnswer2.sd_setBackgroundImage(with:URL.init(string: "" ) , for: .normal) { (image, url, cache, urlimage) in
                        self.btnAnswer1.sd_backgroundImageURL(for: .normal)
                    }
                    btnAnswer2.sd_backgroundImageURL(for: .normal)
                    btnAnswer2.setTitle(dictAns["answer"] as? String,for: .normal)
                    let hight = self.SetButtonHight(btn: btnAnswer2 , str: dictAns["answer"] as! String )
                    
                    if UIDevice.current.userInterfaceIdiom == .pad{
                        if hight >= 75.0{
                            hightbtnIpad2.constant = hight + 75
                        }
                    }else{
                        if hight >= 50.0{
                            btnAns2Hight.constant = hight + 60
                        }
                    }
                }
                else{
                    if UIDevice.current.userInterfaceIdiom == .pad{
                        hightbtnIpad2 .constant = 180
                        iPadWidthbtn2.constant = 360
                    }else{
                        btnAns2Hight.constant = 100
                        widthButton2.constant = 200
                    }
                    
                    btnAnswer2.sd_setBackgroundImage(with:URL.init(string: dictAns["answer"] as? String ?? "" ) , for: .normal) { (image, url, cache, urlimage) in
                        self.btnAnswer1.sd_backgroundImageURL(for: .normal)
                        
                        // print(image?.size.height)
                        //self.btnAns2Hight.constant = image?.size.height ?? 0.0
                    }
                    btnAnswer2.sd_backgroundImageURL(for: .normal)
                    btnAnswer2.setTitle("" ,for: .normal)
                }
                
                
            }
            else if i == 2
            {
                
                if dict["question_answer_type"] as? Int == 1  {
                    
                    if UIDevice.current.userInterfaceIdiom == .pad{
                        iPadHightbtn3.constant = 85
                        iPadWidthbtn3.constant = 500
                    }else{
                        btnAns3Hight.constant = 65
                        widthButton3.constant = 200
                    }
                    
                    //
                    btnAnswer3.sd_setBackgroundImage(with:URL.init(string: "" ) , for: .normal) { (image, url, cache, urlimage) in
                        self.btnAnswer3.sd_backgroundImageURL(for: .normal)
                        
                    }
                    btnAnswer3.sd_backgroundImageURL(for: .normal)
                    btnAnswer3.setTitle(dictAns["answer"] as? String,for: .normal)
                    let hight = self.SetButtonHight(btn: btnAnswer3 , str: dictAns["answer"] as! String )
                    
                    if UIDevice.current.userInterfaceIdiom == .pad{
                        if hight >= 75.0{
                            iPadHightbtn3.constant = hight + 75
                        }
                    }else{
                        if hight >= 50.0{
                            btnAns3Hight.constant = hight + 60
                        }
                    }
                }else if dict["question_answer_type"] as? Int ==  3{
                    if UIDevice.current.userInterfaceIdiom == .pad{
                        iPadHightbtn3.constant = 85
                        iPadWidthbtn3.constant = 500
                    }else{
                        btnAns3Hight.constant = 65
                        widthButton3.constant = 200
                    }
                    btnAnswer3.sd_setBackgroundImage(with:URL.init(string: "" ) , for: .normal) { (image, url, cache, urlimage) in
                        self.btnAnswer1.sd_backgroundImageURL(for: .normal)
                    }
                    btnAnswer3.sd_backgroundImageURL(for: .normal)
                    btnAnswer3.setTitle(dictAns["answer"] as? String,for: .normal)
                    let hight = self.SetButtonHight(btn: btnAnswer3 , str: dictAns["answer"] as! String )
                    
                    if UIDevice.current.userInterfaceIdiom == .pad{
                        if hight >= 75.0{
                            iPadHightbtn3.constant = hight + 75
                        }
                    }else{
                        if hight >= 50.0{
                            btnAns3Hight.constant = hight + 60
                        }
                    }
                }
                else{
                    if UIDevice.current.userInterfaceIdiom == .pad{
                        iPadHightbtn3.constant = 180
                        iPadWidthbtn3.constant = 360
                    }else{
                        btnAns3Hight.constant = 100
                        widthButton3.constant = 200
                    }
                    
                    btnAnswer3.sd_setBackgroundImage(with:URL.init(string: dictAns["answer"] as? String ?? "" ) , for: .normal) { (image, url, cache, urlimage) in
                        self.btnAnswer3.sd_backgroundImageURL(for: .normal)
                    }
                    btnAnswer3.sd_backgroundImageURL(for: .normal)
                    btnAnswer3.setTitle("" ,for: .normal)
                }
                
            }
            else if i == 3
            {
                if dict["question_answer_type"] as? Int == 1  {
                    if UIDevice.current.userInterfaceIdiom == .pad{
                        iPadHightbtn4.constant =  85
                        iPadWidthbtn4.constant = 500
                    }else{
                        hight4.constant = 65
                        widthButton4.constant = 200
                    }
                    
                    //
                    btnAnswer4.sd_setBackgroundImage(with:URL.init(string: "" ) , for: .normal) { (image, url, cache, urlimage) in
                        self.btnAnswer1.sd_backgroundImageURL(for: .normal)
                    }
                    btnAnswer4.sd_backgroundImageURL(for: .normal)
                    btnAnswer4.setTitle(dictAns["answer"] as? String,for: .normal)
                    let hight = self.SetButtonHight(btn: btnAnswer4 , str: dictAns["answer"] as! String )
                    
                    if UIDevice.current.userInterfaceIdiom == .pad{
                        if hight >= 75.0{
                            iPadHightbtn4.constant = hight + 75
                        }
                    }else{
                        if hight >= 50.0{
                            hight4.constant = hight + 60
                        }
                    }
                }else if dict["question_answer_type"] as? Int ==  3{
                    if UIDevice.current.userInterfaceIdiom == .pad{
                        iPadHightbtn4.constant =  85
                        iPadWidthbtn4.constant = 500
                    }else{
                        hight4.constant = 65
                        widthButton4.constant = 200
                    }
                    btnAnswer4.sd_setBackgroundImage(with:URL.init(string: "" ) , for: .normal) { (image, url, cache, urlimage) in
                        self.btnAnswer1.sd_backgroundImageURL(for: .normal)
                    }
                    btnAnswer4.sd_backgroundImageURL(for: .normal)
                    btnAnswer4.setTitle(dictAns["answer"] as? String,for: .normal)
                    btnAnswer4.setTitle(dictAns["answer"] as? String,for: .normal)
                    let hight = self.SetButtonHight(btn: btnAnswer4 , str: dictAns["answer"] as! String )
                    
                    if UIDevice.current.userInterfaceIdiom == .pad{
                        if hight >= 75.0{
                            iPadHightbtn4.constant = hight + 75
                        }
                    }else{
                        if hight >= 50.0{
                            hight4.constant = hight + 60
                        }
                    }
                }
                else{
                    if UIDevice.current.userInterfaceIdiom == .pad{
                        iPadHightbtn4.constant =  180
                        iPadWidthbtn4.constant = 360
                    }else{
                        hight4.constant = 100
                        widthButton4.constant = 200
                    }
                    
                    
                    btnAnswer4.sd_setBackgroundImage(with:URL.init(string: dictAns["answer"] as? String ?? "" ) , for: .normal) { (image, url, cache, urlimage) in
                        self.btnAnswer4.sd_backgroundImageURL(for: .normal)
                        self.btnAnswer4.setTitle("" ,for: .normal)
                        
                    }
                    
                }
                
                
            }
        }
        //button.setTitle("Button Title",for: .normal)
        self.setButtonConfig()
        
        
        if dict["isAnsGiven"] != nil
        {
            if dict["isAnsGiven"] as! Bool {
                self.setButtonDisable()
                var indexNum : Int!
                indexNum = dict["givenAnsIndex"] as? Int
                
                btnDisplayCorrectAnswer.text = dict["savedAns"] as? String
                btnDisplayCorrectAnswer.isHidden = false
                if dict["isAnsCorrent"] as! Bool
                {
                    if indexNum  == 1
                    {
                        btnAnswer1.backgroundColor = .clear
                        btnAnswer1.layer.cornerRadius = 1
                        btnAnswer1.layer.borderWidth = 5
                        btnAnswer1.layer.borderColor = UIColor.green.cgColor
                        
                    }
                    else if indexNum  == 2
                    {
                        btnAnswer2.backgroundColor = .clear
                        btnAnswer2.layer.cornerRadius = 1
                        btnAnswer2.layer.borderWidth = 5
                        btnAnswer2.layer.borderColor = UIColor.green.cgColor
                    }
                    else if indexNum  == 3
                    {
                        btnAnswer3.backgroundColor = .clear
                        btnAnswer3.layer.cornerRadius = 1
                        btnAnswer3.layer.borderWidth = 5
                        btnAnswer3.layer.borderColor = UIColor.green.cgColor
                    }
                    else if indexNum  == 4
                    {
                        btnAnswer4.backgroundColor = .clear
                        btnAnswer4.layer.cornerRadius = 1
                        btnAnswer4.layer.borderWidth = 5
                        btnAnswer4.layer.borderColor = UIColor.green.cgColor
                    }
                }
                else
                {
                    if indexNum  == 1
                    {
                        btnAnswer1.backgroundColor = .clear
                        btnAnswer1.layer.cornerRadius = 1
                        btnAnswer1.layer.borderWidth = 5
                        btnAnswer1.layer.borderColor = UIColor.red.cgColor
                    }
                    else if indexNum  == 2
                    {
                        btnAnswer2.backgroundColor = .clear
                        btnAnswer2.layer.cornerRadius = 1
                        btnAnswer2.layer.borderWidth = 5
                        btnAnswer2.layer.borderColor = UIColor.red.cgColor
                        //
                    }
                    else if indexNum  == 3
                    {
                        btnAnswer3.backgroundColor = .clear
                        btnAnswer3.layer.cornerRadius = 1
                        btnAnswer3.layer.borderWidth = 5
                        btnAnswer3.layer.borderColor = UIColor.red.cgColor
                    }
                    else if indexNum  == 4
                    {
                        btnAnswer4.backgroundColor = .clear
                        btnAnswer4.layer.cornerRadius = 1
                        btnAnswer4.layer.borderWidth = 5
                        btnAnswer4.layer.borderColor = UIColor.red.cgColor
                    }
                    
                    if indexRightAnsNum  == 0
                    {
                        
                        btnAnswer1.backgroundColor = .clear
                        btnAnswer1.layer.cornerRadius = 1
                        btnAnswer1.layer.borderWidth = 5
                        btnAnswer1.layer.borderColor = UIColor.green.cgColor
                        
                    }
                    else if indexRightAnsNum  == 1
                    {
                        btnAnswer2.backgroundColor = .clear
                        btnAnswer2.layer.cornerRadius = 1
                        btnAnswer2.layer.borderWidth = 5
                        btnAnswer2.layer.borderColor = UIColor.green.cgColor
                    }
                    else if indexRightAnsNum  == 2
                    {
                        btnAnswer3.backgroundColor = .clear
                        btnAnswer3.layer.cornerRadius = 1
                        btnAnswer3.layer.borderWidth = 5
                        btnAnswer3.layer.borderColor = UIColor.green.cgColor
                    }
                    else if indexRightAnsNum  == 3
                    {
                        btnAnswer4.backgroundColor = .clear
                        btnAnswer4.layer.cornerRadius = 1
                        btnAnswer4.layer.borderWidth = 5
                        btnAnswer4.layer.borderColor = UIColor.green.cgColor
                    }
                }
            }
        }
        else
        {
            self.setButtonEnabled()
        }
        
        
        
       
        
        
        
        if dict["question_answer_type"] as? Int == 1   {
            lblQuestion.text = dict["question"] as? String ?? ""
            
            //  lblHight.constant = 55
            quesImageView.isHidden = true
            lblTitleWithIamge.isHidden = true
            if UIDevice.current.userInterfaceIdiom == .pad{
                let font1 = UIFont(name: "HelveticaNeueLTStd-Cn", size: 18)
                
                let hight = heightForView(text:  dict["question"] as? String ?? "" , font: font1 ?? UIFont.systemFont(ofSize: 20), width: 600)
                
                //IpadHightlblQuestion.constant = hight
                iPadQuestionImageHight.constant = 0
               
            }else{
                lblHight.constant = 55
            }
            //
        }else if dict["question_answer_type"] as? Int == 2{
            lblQuestion.text = dict["question"] as? String ?? ""
            
            // lblHight.constant = 55
            if UIDevice.current.userInterfaceIdiom == .pad{
                let font1 = UIFont(name: "HelveticaNeueLTStd-Cn", size: 18)
                
                let hight = heightForView(text:  dict["question"] as? String ?? "" , font: font1 ?? UIFont.systemFont(ofSize: 20), width: 600)
               // IpadHightlblQuestion.constant = hight
                iPadQuestionImageHight.constant = 0
                 // quesImageView.isHidden = false
            }else{
                lblHight.constant = 55
            }
            //            IpadHightlblQuestion.constant = 70
            //            iPadQuestionImageHight.constant = 70
            quesImageView.isHidden = true
        }
        else{
            lblQuestion.text = ""
            
            if UIDevice.current.userInterfaceIdiom == .pad{
               // IpadHightlblQuestion.constant = 300
                iPadQuestionImageHight.constant = 300
            }else{
                hightImageQuestion.constant = 200
                lblHight.constant = 200
            }
            //            IpadHightlblQuestion.constant = 200
            //            iPadQuestionImageHight.constant = 200
            quesImageView.isHidden = false
            lblTitleWithIamge.isHidden = false
            lblTitleWithIamge.text = dict["question"] as? String
            // let bannerImgStr = dict["question_image"] as? String
            // if bannerImgStr != nil
            // {
            //  let urlString = bannerImgStr?.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            self.quesImageView.sd_setImage(with: URL.init(string: dict["question_image"] as? String ?? ""), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                //self.quesImageView.image = image
            })
        }
    }
    //self.setButtonConfig()
    
    

    func setButtonConfig()
    {
        self.setButtonEnabled()
        
        let dict: Dictionary? = arrayQuestion[questionNumber] as? Dictionary<String,Any>
        
        print("Dict Data",dict)
        
        btnAnswer1.backgroundColor = .clear
        btnAnswer1.layer.cornerRadius = 1
        btnAnswer1.layer.borderWidth = 5
        btnAnswer1.layer.borderColor = UIColor.white.cgColor
        
        btnAnswer2.backgroundColor = .clear
        btnAnswer2.layer.cornerRadius = 1
        btnAnswer2.layer.borderWidth = 5
        btnAnswer2.layer.borderColor = UIColor.white.cgColor
        
        btnAnswer3.backgroundColor = .clear
        btnAnswer3.layer.cornerRadius = 1
        btnAnswer3.layer.borderWidth = 5
        btnAnswer3.layer.borderColor = UIColor.white.cgColor
        
        btnAnswer4.backgroundColor = .clear
        btnAnswer4.layer.cornerRadius = 1
        btnAnswer4.layer.borderWidth = 5
        btnAnswer4.layer.borderColor = UIColor.white.cgColor
        btnDisplayCorrectAnswer.isHidden = true
       
        
    }
    
    func setButtonDisable()
    {
        btnAnswer1.isUserInteractionEnabled = false;
        btnAnswer2.isUserInteractionEnabled = false;
        btnAnswer3.isUserInteractionEnabled = false;
        btnAnswer4.isUserInteractionEnabled = false;
    }
    
    func setButtonEnabled()
    {
        btnAnswer1.isUserInteractionEnabled = true;
        btnAnswer2.isUserInteractionEnabled = true;
        btnAnswer3.isUserInteractionEnabled = true;
        btnAnswer4.isUserInteractionEnabled = true;
    }
    
    
    @IBAction func clickAnswer1(_ sender: Any)
    {
        
        var dict: Dictionary = arrayQuestion[questionNumber] as! Dictionary<String,Any>
        var arrayAnswer1 = dict["kids_questions_answers"] as? Array<Any>
        var dictAns: Dictionary = arrayAnswer1?[0] as! Dictionary<String,Any>
        dict["isAnsGiven"] = true
       
        if dictAns["is_correct"] as! Bool
        {
            dict["isAnsCorrent"] = true
            btnAnswer1.backgroundColor = .clear
            btnAnswer1.layer.cornerRadius = 1
            btnAnswer1.layer.borderWidth = 5
            btnAnswer1.layer.borderColor = UIColor.green.cgColor
            //btnDisplayCorrectAnswer.isHidden = true
            btnDisplayCorrectAnswer.isHidden = false
            btnDisplayCorrectAnswer.text =  dictAns["feedback_msg"] as? String
           // btnDisplayCorrectAnswer.text = ""
            CountTrue = CountTrue + 1
            yourArray.append("1")
        }
        else
        {
            dict["isAnsCorrent"] = false
            btnAnswer1.backgroundColor = .clear
            btnAnswer1.layer.cornerRadius = 1
            btnAnswer1.layer.borderWidth = 5
            btnAnswer1.layer.borderColor = UIColor.red.cgColor
            btnDisplayCorrectAnswer.isHidden = false
            btnDisplayCorrectAnswer.text =  dictAns["feedback_msg"] as? String
            yourArray.append("1")
            var dictAns1: Dictionary = arrayAnswer1?[1] as! Dictionary<String,Any>
            var dictAns2: Dictionary = arrayAnswer1?[2] as! Dictionary<String,Any>
            var dictAns3: Dictionary = arrayAnswer1?[3] as! Dictionary<String,Any>
            
            
            if dictAns1["is_correct"] as! Bool
            {
                //dict["isAnsCorrent"] = true
                btnAnswer2.backgroundColor = .clear
                btnAnswer2.layer.cornerRadius = 1
                btnAnswer2.layer.borderWidth = 5
                btnAnswer2.layer.borderColor = UIColor.green.cgColor
               
            }
            else if dictAns2["is_correct"] as! Bool
            {
                //dict["isAnsCorrent"] = true
                btnAnswer3.backgroundColor = .clear
                btnAnswer3.layer.cornerRadius = 1
                btnAnswer3.layer.borderWidth = 5
                btnAnswer3.layer.borderColor = UIColor.green.cgColor
                
            }
            else if dictAns3["is_correct"] as! Bool
            {
                //dict["isAnsCorrent"] = true
                btnAnswer4.backgroundColor = .clear
                btnAnswer4.layer.cornerRadius = 1
                btnAnswer4.layer.borderWidth = 5
                btnAnswer4.layer.borderColor = UIColor.green.cgColor
                
            }
        }
        
        
        
        dict["givenAnsIndex"] = 1;
        dict["savedAns"] = btnDisplayCorrectAnswer.text
        arrayQuestion[questionNumber] = dict
        let strmsgAppend = "Correct answers given "
        let strAppend2 = "("
        let msg = String(CountTrue)
        let strAppend3 = " correct out of "
        let appendString3 =  "\(arrayQuestion.count)"
        let strAppend4 = ")"
       // let appendString6 = "]"
        let strMsg = strmsgAppend + strAppend2 + msg + strAppend3 + appendString3 + strAppend4
        var qusNo = questionNumber + 1
        print(qusNo)
        if qusNo >= arrayQuestion.count{
            Alert.showAlertWithHandler(title: Constant.alertTitle, message: strMsg) { (action) in
                self.checkAns = true
                self.PoptoMenu()
                //self.POP_VC()
                
            }
        }


        self.setButtonDisable()

    }
    
    @IBAction func clickAnswer2(_ sender: Any)
    {
        
        var dict: Dictionary = arrayQuestion[questionNumber] as! Dictionary<String,Any>
        var arrayAnswer1 = dict["kids_questions_answers"] as? Array<Any>
        var dictAns: Dictionary = arrayAnswer1?[1] as! Dictionary<String,Any>
        
        dict["isAnsGiven"] = true
        
        if dictAns["is_correct"] as! Bool
        {   dict["isAnsCorrent"] = true
           
            btnAnswer2.backgroundColor = .clear
            btnAnswer2.layer.cornerRadius = 1
            btnAnswer2.layer.borderWidth = 5
            btnAnswer2.layer.borderColor = UIColor.green.cgColor
           // btnDisplayCorrectAnswer.isHidden = true
            btnDisplayCorrectAnswer.isHidden = false
            btnDisplayCorrectAnswer.text =  dictAns["feedback_msg"] as? String
           // btnDisplayCorrectAnswer.text = ""
            CountTrue = CountTrue + 1
            yourArray.append("1")
           
        }
        else
        {
            dict["isAnsCorrent"] = false
            btnAnswer2.backgroundColor = .clear
            btnAnswer2.layer.cornerRadius = 1
            btnAnswer2.layer.borderWidth = 5
            btnAnswer2.layer.borderColor = UIColor.red.cgColor
            btnDisplayCorrectAnswer.isHidden = false
            btnDisplayCorrectAnswer.text =  dictAns["feedback_msg"] as? String
            yourArray.append("1")
            var dictAns1: Dictionary = arrayAnswer1?[0] as! Dictionary<String,Any>
            var dictAns2: Dictionary = arrayAnswer1?[2] as! Dictionary<String,Any>
            var dictAns3: Dictionary = arrayAnswer1?[3] as! Dictionary<String,Any>
            
            
            if dictAns1["is_correct"] as! Bool
            {
                //dict["isAnsCorrent"] = true
                btnAnswer1.backgroundColor = .clear
                btnAnswer1.layer.cornerRadius = 1
                btnAnswer1.layer.borderWidth = 5
                btnAnswer1.layer.borderColor = UIColor.green.cgColor
            }
            else if dictAns2["is_correct"] as! Bool
            {
               // dict["isAnsCorrent"] = true
                btnAnswer3.backgroundColor = .clear
                btnAnswer3.layer.cornerRadius = 1
                btnAnswer3.layer.borderWidth = 5
                btnAnswer3.layer.borderColor = UIColor.green.cgColor
            }
            else if dictAns3["is_correct"] as! Bool
            {
                //dict["isAnsCorrent"] = true
                btnAnswer4.backgroundColor = .clear
                btnAnswer4.layer.cornerRadius = 1
                btnAnswer4.layer.borderWidth = 5
                btnAnswer4.layer.borderColor = UIColor.green.cgColor
            }
         
           
        }
        dict["givenAnsIndex"] = 2;
        dict["savedAns"] = btnDisplayCorrectAnswer.text
        arrayQuestion[questionNumber] = dict
      
        // let appendString6 = "]"
       
        let strmsgAppend = "Correct answers given "
        let strAppend2 = "("
        let msg = String(CountTrue)
        let strAppend3 = " correct out of "
        let appendString3 =  "\(arrayQuestion.count)"
        let strAppend4 = ")"
        // let appendString6 = "]"
        let strMsg = strmsgAppend + strAppend2 + msg + strAppend3 + appendString3 + strAppend4
          var qusNo = questionNumber + 1
       // questionNumber = questionNumber + 1
        if qusNo >= arrayQuestion.count{
            Alert.showAlertWithHandler(title: Constant.alertTitle, message: strMsg) { (action) in
                self.checkAns = true
                self.PoptoMenu()
                //self.POP_VC()
                
            }
        }
        self.setButtonDisable()
    }
    
  
    @IBAction func clickAnswer3(_ sender: Any)
    {
        var dict: Dictionary = arrayQuestion[questionNumber] as! Dictionary<String,Any>
        var arrayAnswer1 = dict["kids_questions_answers"] as? Array<Any>
        var dictAns: Dictionary = arrayAnswer1?[2] as! Dictionary<String,Any>
        dict["isAnsGiven"] = true
       
        if dictAns["is_correct"] as! Bool
        {
            dict["isAnsCorrent"] = true
            btnAnswer3.backgroundColor = .clear
            btnAnswer3.layer.cornerRadius = 1
            btnAnswer3.layer.borderWidth = 5
            btnAnswer3.layer.borderColor = UIColor.green.cgColor
          //  btnDisplayCorrectAnswer.isHidden = true
            btnDisplayCorrectAnswer.isHidden = false
            btnDisplayCorrectAnswer.text =  dictAns["feedback_msg"] as? String
           // btnDisplayCorrectAnswer.text = ""
            CountTrue = CountTrue + 1
            yourArray.append("1")
        }
        else
        {
            dict["isAnsCorrent"] = false
            btnAnswer3.backgroundColor = .clear
            btnAnswer3.layer.cornerRadius = 1
            btnAnswer3.layer.borderWidth = 5
            btnAnswer3.layer.borderColor = UIColor.red.cgColor
            btnDisplayCorrectAnswer.isHidden = false
            btnDisplayCorrectAnswer.text =  dictAns["feedback_msg"] as? String
            
            var dictAns1: Dictionary = arrayAnswer1?[0] as! Dictionary<String,Any>
            var dictAns2: Dictionary = arrayAnswer1?[1] as! Dictionary<String,Any>
            var dictAns3: Dictionary = arrayAnswer1?[3] as! Dictionary<String,Any>
            yourArray.append("1")
            
            if dictAns1["is_correct"] as! Bool
            {
                //dict["isAnsCorrent"] = true
                btnAnswer1.backgroundColor = .clear
                btnAnswer1.layer.cornerRadius = 1
                btnAnswer1.layer.borderWidth = 5
                btnAnswer1.layer.borderColor = UIColor.green.cgColor
            }
            else if dictAns2["is_correct"] as! Bool
            {
                //dict["isAnsCorrent"] = true
                btnAnswer2.backgroundColor = .clear
                btnAnswer2.layer.cornerRadius = 1
                btnAnswer2.layer.borderWidth = 5
                btnAnswer2.layer.borderColor = UIColor.green.cgColor
            }
            else if dictAns3["is_correct"] as! Bool
            {
                //dict["isAnsCorrent"] = true
                btnAnswer4.backgroundColor = .clear
                btnAnswer4.layer.cornerRadius = 1
                btnAnswer4.layer.borderWidth = 5
                btnAnswer4.layer.borderColor = UIColor.green.cgColor
            }
        }
        dict["givenAnsIndex"] = 3;
        dict["savedAns"] = btnDisplayCorrectAnswer.text
        arrayQuestion[questionNumber] = dict
        let strmsgAppend = " Correct answers given "
        let strAppend2 = "("
        let msg = String(CountTrue)
        let strAppend3 = " correct out of "
        let appendString3 =  "\(arrayQuestion.count)"
        let strAppend4 = ")"
        // let appendString6 = "]"
        let strMsg = strmsgAppend + strAppend2 + msg + strAppend3 + appendString3 + strAppend4
       
      
        var qusNo = questionNumber + 1
        if qusNo >= arrayQuestion.count{
            Alert.showAlertWithHandler(title: Constant.alertTitle, message: strMsg) { (action) in
                self.checkAns = true
                self.PoptoMenu()
                //self.POP_VC()
                
            }
            //Alert.showAlert(title: Constant.alertTitle , message: strMsg)
            
        }
        self.setButtonDisable()
    
    }
    
    @IBAction func clickAnswer4(_ sender: Any)
    {
        var dict: Dictionary = arrayQuestion[questionNumber] as! Dictionary<String,Any>
        var arrayAnswer1 = dict["kids_questions_answers"] as? Array<Any>
        var dictAns: Dictionary = arrayAnswer1?[3] as! Dictionary<String,Any>
        
        dict["isAnsGiven"] = true
        
        if dictAns["is_correct"] as! Bool
        {
            dict["isAnsCorrent"] = true
            btnAnswer4.backgroundColor = .clear
            btnAnswer4.layer.cornerRadius = 1
            btnAnswer4.layer.borderWidth = 5
            btnAnswer4.layer.borderColor = UIColor.green.cgColor
           // btnDisplayCorrectAnswer.isHidden = true
            btnDisplayCorrectAnswer.isHidden = false
            btnDisplayCorrectAnswer.text =  dictAns["feedback_msg"] as? String
            //btnDisplayCorrectAnswer.text = ""
            CountTrue = CountTrue + 1
            yourArray.append("1")
        }
        else
        {
            dict["isAnsCorrent"] = false
            btnAnswer4.backgroundColor = .clear
            btnAnswer4.layer.cornerRadius = 1
            btnAnswer4.layer.borderWidth = 5
            btnAnswer4.layer.borderColor = UIColor.red.cgColor
            btnDisplayCorrectAnswer.isHidden = false
            btnDisplayCorrectAnswer.text =  dictAns["feedback_msg"] as? String
            yourArray.append("1")
            var dictAns1: Dictionary = arrayAnswer1?[0] as! Dictionary<String,Any>
            var dictAns2: Dictionary = arrayAnswer1?[2] as! Dictionary<String,Any>
            var dictAns3: Dictionary = arrayAnswer1?[1] as! Dictionary<String,Any>
            
            
            if dictAns1["is_correct"] as! Bool
            {
                //dict["isAnsCorrent"] = true
                btnAnswer1.backgroundColor = .clear
                btnAnswer1.layer.cornerRadius = 1
                btnAnswer1.layer.borderWidth = 5
                btnAnswer1.layer.borderColor = UIColor.green.cgColor
            }
            else if dictAns2["is_correct"] as! Bool
            {
                //dict["isAnsCorrent"] = true
                btnAnswer3.backgroundColor = .clear
                btnAnswer3.layer.cornerRadius = 1
                btnAnswer3.layer.borderWidth = 5
                btnAnswer3.layer.borderColor = UIColor.green.cgColor
            }
            else if dictAns3["is_correct"] as! Bool
            {
                //dict["isAnsCorrent"] = true
                btnAnswer2.backgroundColor = .clear
                btnAnswer2.layer.cornerRadius = 1
                btnAnswer2.layer.borderWidth = 5
                btnAnswer2.layer.borderColor = UIColor.green.cgColor
            }
        }
        dict["givenAnsIndex"] = 4;
        dict["savedAns"] = btnDisplayCorrectAnswer.text
        arrayQuestion[questionNumber] = dict
        let strmsgAppend = "Correct answers given "
        let strAppend2 = "("
        let msg = String(CountTrue)
        let strAppend3 = " correct out of "
        let appendString3 =  "\(arrayQuestion.count)"
        let strAppend4 = ")"
        // let appendString6 = "]"
        let strMsg = strmsgAppend + strAppend2 + msg + strAppend3 + appendString3 + strAppend4
        
       
      
         var qusNo = questionNumber + 1
        if qusNo >= arrayQuestion.count{
            //if yourArray.count != arrayQuestion.count{
               //  Alert.showAlert(title: Constant.alertTitle , message: "Skip")
           // }else{
            Alert.showAlertWithHandler(title: Constant.alertTitle, message: strMsg) { (action) in
                self.checkAns = true
                self.PoptoMenu()
                //self.POP_VC()
                
            }
            }
          
        
        self.setButtonDisable()
    }
  
   
    func SetWhiteBorder(button_ : UIButton)  {
//        button_.backgroundColor = UIColor.white
//        button_.layer.cornerRadius = 1
//        button_.layer.borderWidth = 5
//        button_.layer.borderColor = UIColor.white.cgColor
    }
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        //let label:UILabel = UILabel(frame: CGRectMake(0, 0, width, CGFloat.greatestFiniteMagnitude))
        let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: width, height: CGFloat.greatestFiniteMagnitude))
        let label:UILabel = UILabel(frame: rect)
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.isHidden = true
        label.sizeToFit()
        print(label.frame.height)
        return label.frame.height
    }
    
    func SetButtonHight(btn : UIButton , str : String) -> CGFloat  {
        
       
        if UIDevice.current.userInterfaceIdiom == .pad{
             let font = UIFont(name: "HelveticaNeueLTStd-Cn", size: 24)
         let height = heightForView(text: str, font: font ?? UIFont.systemFont(ofSize: 25), width: 500)
            btn.contentEdgeInsets = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
            return height
        }else{
               let font = UIFont(name: "HelveticaNeueLTStd-Cn", size: 18)
            let height = heightForView(text: str, font: font ?? UIFont.systemFont(ofSize: 20), width: 200)
            btn.contentEdgeInsets = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
            return height
        }
        
        
    }
    
    func PoptoMenu()  {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: MenuVC.self) {
                _ =  self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
}
