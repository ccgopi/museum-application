//
//  AboutVC.swift
//  MuseumApp
//
//  Created by CCT Macmini on 03/01/19.
//  Copyright © 2019 CCT Macmini. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire


class AboutVC: UIViewController
{
    
    @IBOutlet weak var blurViewTopCons: NSLayoutConstraint!
    @IBOutlet weak var btnNextPage: UIButton!
    @IBOutlet weak var btnPrevPage: UIButton!
    @IBOutlet weak var btnHomePage: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var DevPrevImg: UIImageView!
    @IBOutlet weak var DevNextImg: UIImageView!
    @IBOutlet weak var backImg: UIImageView!
    @IBOutlet weak var webview: UIWebView!
    @IBOutlet weak var NochView: UIView!
    @IBOutlet weak var lblTitleName: UILabel!
    @IBOutlet weak var lblTitleAbout: UILabel!
    @IBOutlet weak var AboutImg: UIImageView!
    @IBOutlet weak var ArrowbtnNextPage: UIButton!
    @IBOutlet weak var ArrowbtnPrevPage: UIButton!
    var aboutUsDict: Dictionary<String,Any>!
    var Gallery: Dictionary<String,Any>!
    var factSheetDict: Dictionary<String,Any>!
    var videoDict: Dictionary<String,Any>!
    var kidsDict: Dictionary<String,Any>!
    var factSheet: Int!
    var Videos: Int!
    var Kids: Int!
    var check: Bool!
   
    //var aboutUsDict: NSMutableDictionary!
    var appDel: AppDelegate!
    @IBAction func btnNextPage(_ sender: Any)
    {
       
        if Gallery["status"] as! String != "N"
        {
            
            if UIDevice.current.userInterfaceIdiom == .pad
            {
                PUSH_STORY_R1(Identifier: "GalleryVC1")
                return
            } else
            {
                PUSH_STORY_R1(Identifier: "idGallery")
                return
            }}
        
        
        if factSheetDict["status"] as! String != "N"
        {
            if UIDevice.current.userInterfaceIdiom == .pad
            {
                PUSH_STORY_R1(Identifier: "FactSheetVC")
                return
            } else
            {
                PUSH_STORY_R1(Identifier: "FactSheetVC")
                return
            }}
        
        
        if videoDict["status"] as! String != "N"
        {
            if UIDevice.current.userInterfaceIdiom == .pad
            {
                PUSH_STORY_R1(Identifier: "VideosVC")
                return
            } else
            {
                PUSH_STORY_R1(Identifier: "VideosVC")
                return
            }}
        
        if kidsDict["status"] as! String != "N"
        {
            if UIDevice.current.userInterfaceIdiom == .pad
            {
                PUSH_STORY_R1(Identifier: "KidsVC")
                return
            } else
            {
                PUSH_STORY_R1(Identifier: "KidsVC")
                return
            }}
    
//
//        if UIDevice.current.userInterfaceIdiom == .pad
//        {
//            let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
//            let FactSheet = storyboard.instantiateViewController(withIdentifier: "FactSheetVC") as! FactSheetVC
//            self.navigationController?.pushViewController(FactSheet, animated: false)
//        }
//        else
//        {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let FactSheet = storyboard.instantiateViewController(withIdentifier: "FactSheetVC") as! FactSheetVC
//            self.navigationController?.pushViewController(FactSheet, animated: false)
//        }
    }
    
    @IBAction func btnPrevPage(_ sender: Any)
    {
        if UIDevice.current.userInterfaceIdiom == .pad
        {
           self.PUSH_STORY_R2(Identifier: "MenuVC")
           //  self.navigationController?.popViewController(animated: true)
        }
        else
        {
            self.PUSH_STORY_R2(Identifier: "MenuVC")
         // self.navigationController?.popViewController(animated: true)

        }
     
    }
    
     func PrevButtonText()
     {
        let colorValue_button: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["tab_color"] as! String)
        self.btnPrevPage.setTitleColor(colorValue_button, for: .normal)
       
        if UIDevice.current.userInterfaceIdiom == .pad
        {
           
            self.btnPrevPage.setTitle("", for: UIControl.State.normal)
            self.ArrowbtnPrevPage.isHidden = false
            return
         
        }
        else
        {
            self.btnPrevPage.setTitle("", for: UIControl.State.normal)
            self.ArrowbtnPrevPage.isHidden = false
            return
        }
     }
   
    @IBAction func btnHomePage(_ sender: Any)
    {
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
             //POP_VC()
            for controller in self.navigationController!.viewControllers as Array
            {
                if controller.isKind(of: MenuVC.self)
                {
                    _ =  self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }

        }
        else
        {
            for controller in self.navigationController!.viewControllers as Array
            {
                if controller.isKind(of: MenuVC.self)
                {
                    _ =  self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        check = true
    
        if(appDel == nil)
        {
            self.appDel = UIApplication.shared.delegate as! AppDelegate
        }
        
        
        aboutUsDict = appDel.itemDetails["about"] as? Dictionary<String, Any>
        Gallery = appDel.itemDetails["gallery"] as? Dictionary<String, Any>
        factSheetDict = appDel.itemDetails["fact_sheet"] as? Dictionary<String, Any>
        videoDict = appDel.itemDetails["video"] as? Dictionary<String, Any>
        kidsDict = appDel.itemDetails["kids"] as? Dictionary<String, Any>
        
        if Gallery["status"] as! String != "N"
        {
         check = checkForHiddenButton(_check: check)
            
        }
        if factSheetDict["status"] as! String != "N"
        {
            check = checkForHiddenButton(_check: check)
        }
        
        if videoDict["status"] as! String != "N"
        {
            check = checkForHiddenButton(_check: check)
        }
        
        if kidsDict["status"] as! String != "N"
        {
           check = checkForHiddenButton(_check: check)
        }
       
        if check == true
        {
            btnNextPage.isHidden = true
          
          
           
        }
        
        print(aboutUsDict)
        
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated) // No need for semicolon
        
       
      //  arrayAbout = aboutUsDict["video_files"] as? Array<Any>
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.BottomBarButtonHideShowMethod()
       
        if aboutUsDict != nil
        {
             self.ConfigInformation()
        }
        self.SetMenuTopConstraint()
        self.nextButtonText()
        self.PrevButtonText()
      
    }
    
    func nextButtonText()
    {
        let colorValue_button: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["tab_color"] as! String)
        self.btnNextPage.setTitleColor(colorValue_button, for: .normal)
       
        if Gallery["status"] as! String != "N"
        {
            let str = Gallery["title"] as? String
            self.btnNextPage.setTitle(str!, for: UIControl.State.normal)
            ArrowbtnNextPage.isHidden = false;
         //   self.btnNextPage.setTitle("Gallery ", for: UIControl.State.normal)
            return
        }
        else
        {
            ArrowbtnNextPage.isHidden = true;
        }
       
        if factSheetDict["status"] as! String != "N"
        {
            let str = factSheetDict["title"] as? String
            self.btnNextPage.setTitle(str!, for: UIControl.State.normal)
            ArrowbtnNextPage.isHidden = false;
           // self.btnNextPage.setTitle("Facts ", for: UIControl.State.normal)
            return
        }
        else
        {
            ArrowbtnNextPage.isHidden = true;
        }
       
        if videoDict["status"] as! String != "N"
        {
            let str = videoDict["title"] as? String
            self.btnNextPage.setTitle(str!, for: UIControl.State.normal)
            ArrowbtnNextPage.isHidden = false
            
            //self.btnNextPage.setTitle("Videos ", for: UIControl.State.normal)
            return
        }
        else
        {
             ArrowbtnNextPage.isHidden = true;
        }
            
       
        
        if kidsDict["status"] as! String != "N"
        {
            let str = kidsDict["title"] as? String
            self.btnNextPage.setTitle(str!, for: UIControl.State.normal)
             ArrowbtnNextPage.isHidden = false
            ArrowbtnNextPage.isHidden = false;
          //  self.btnNextPage.setTitle("Kids ", for: UIControl.State.normal)
            return
        }
        else
        {
             ArrowbtnNextPage.isHidden = true;
        }
       
    }
    
    func BottomBarButtonHideShowMethod()
    {
        //btnNextPage.isHidden = false
        btnNextPage.isEnabled = true
        btnPrevPage.isHidden = false
        btnPrevPage.isEnabled = true
        btnHomePage.isHidden = false
        btnHomePage.isEnabled = true
        DevPrevImg.isHidden = false
        DevNextImg.isHidden = false
    }
    
    func SetMenuTopConstraint()
    {
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            if Device.IS_IPAD_PRO
            {
                blurViewTopCons.constant = 30
            }
            else
            {
               
                blurViewTopCons.constant = 10
            }
        }
            
        else
        {
            if Device.IS_IPHONE_6
            {
                
            }
                
            else if Device.IS_IPHONE_6P
            {
                
            }
                
            else
            {
                
            }
        }
        
    }

    func ConfigInformation()
    {
     
        
        //webview Load
        
       
        var strText = aboutUsDict["description"] as? String ?? ""
        strText.replacingOccurrences(of: "\n", with: "<br/>")
        var htmlFont : String!
       
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            htmlFont = "<font face='HELVETICANEUELTSTD-CN' size='40' align= 'center' color= 'white' >\(strText)"
        }
        else
        {
             htmlFont = "<font face='HELVETICANEUELTSTD-CN' size='5' align= 'center' color= 'white' >\(strText)"
        }
       
         //let url = UserDefaults.standard.object(forKey: "Base_URL") as! String
         webview.loadHTMLString(htmlFont, baseURL: nil)
         webview.isOpaque = false;
         webview.backgroundColor = UIColor.clear
        
        //[webview loadHTMLString:htmlFont baseURL:nil];
        
        
     //   let url: URL! = URL(string: strPdf)
      //  webview.loadRequest(URLRequest(url: url))
        
        
        //home button
        
        let colorValue_button: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["tab_color"] as! String)
        self.btnHomePage.setTitleColor(colorValue_button, for: .normal)
        self.btnHomePage.setTitle("Home", for: UIControl.State.normal)
        
        //Title Heading Text set
        
        lblTitleName.text = appDel.itemDetails["title"] as? String
        lblTitleName.textColor = AppDelegate.sharedInstance().hexStringToUIColor(hex : appDel.itemDetails["title_colour"] as! String)
        
        
        //Title Video Text set
            
        self.lblTitleAbout.text = aboutUsDict["title"] as? String
        self.lblTitleAbout.textColor = AppDelegate.sharedInstance().hexStringToUIColor(hex : aboutUsDict["color"] as! String)
        
        //Set Heading Image
        
        var aboutLogo : String!
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            aboutLogo = aboutUsDict["ipad_icon_image"] as? String
        }
        else
        {
            aboutLogo = aboutUsDict["iphone_icon_image"] as? String
        }
        
        if aboutLogo != nil
        {
            let urlString = aboutLogo.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            AboutImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                
            })
        }
        
        
        //bottomView Theme Color set
        
        let colorValue_BottomView : UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["theme_color"] as! String)
        self.bottomView.backgroundColor = colorValue_BottomView
        
        //NochView
        
        let colorValue_NochView : UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["theme_color"] as! String)
        self.NochView.backgroundColor = colorValue_NochView
        
        
        //BackGround ImgSet
       
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            
            let bannerImgStr = appDel.itemDetails["ipad_background_image"] as! String
            if bannerImgStr != nil
            {
                let urlString = bannerImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                self.backImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    
                })
            }
        }
        else
        {
            let bannerImgStr = appDel.itemDetails["iphone_background_image"] as! String
            if bannerImgStr != nil
            {
                let urlString = bannerImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                self.backImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    
                })
            }
        }
        
        
        var tabBarImg1 : String!
        var tabBarImg2 : String!
        var tabBarImg3 : String!
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            tabBarImg1 = self.appDel.configDict["ipad_bottom_icon_1"] as? String
            tabBarImg2 = self.appDel.configDict["ipad_bottom_icon_2"] as? String
            tabBarImg3 = self.appDel.configDict["ipad_bottom_icon_3"] as? String
            
        }
        else
        {
            tabBarImg1 = self.appDel.configDict["ipad_bottom_icon_1"] as? String
            tabBarImg2 = self.appDel.configDict["ipad_bottom_icon_2"] as? String
            tabBarImg3 = self.appDel.configDict["ipad_bottom_icon_3"] as? String
            
        }
        
        //btn Prev page
        if tabBarImg1 != nil
        {
//            let urlString = tabBarImg1.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//            self.btnPrevPage.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
//                , completed: { (image, error, cacheType, imageURL) in
//                    self.btnPrevPage.backgroundImage(for: UIControl.State.normal)
//            })
        }
        
        //btn Next page
        if tabBarImg3 != nil
        {
//            let urlString = tabBarImg3.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//            self.btnNextPage.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
//                , completed: { (image, error, cacheType, imageURL) in
//                    self.btnNextPage.backgroundImage(for: UIControl.State.normal)
//            })
        }
        
        //btn Home page
        if tabBarImg2 != nil
        {
//            let urlString = tabBarImg2.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//            self.btnHomePage.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
//                , completed: { (image, error, cacheType, imageURL) in
//                    self.btnHomePage.backgroundImage(for: UIControl.State.normal)
//            })
//
        }
        
        //Devider Next/Prev image set
        
        let colorValue_devColor: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["tab_color"] as! String)
        DevNextImg.backgroundColor = colorValue_devColor
        DevPrevImg.backgroundColor = colorValue_devColor
        
    }
    
   
    func checkForHiddenButton(_check : Bool) -> Bool {
        check = false
        return check
    }
    
}
