//
//  TrailDetailsViewController.swift
//  MuseumApp
//
//  Created by CCT Mini 2 on 09/04/19.
//  Copyright © 2019 CCT Macmini. All rights reserved.
//

import UIKit
import SDWebImage
class TrailDetailsViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lbldescription: UILabel!
    
    @IBOutlet weak var imgView: UIImageView!
    var DetailsData : NSDictionary!
    var LatLOngArray : Array<Any>!
     var finalStr : String! = String()
    
    @IBOutlet weak var btnActionTrail: UIButton!
    
    @IBAction func btnPressTrail(_ sender: Any)
    {
    }
    
    @IBOutlet weak var BackImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(DetailsData)
        let bannerImgStr = DetailsData["image"] as! String
        lblTitle.text = DetailsData["title"] as! String
        lblTitle.textColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: DetailsData["color"] as! String)
        lbldescription.textColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: "#FFFFFF" )
        
        
                    if bannerImgStr != nil
                    {
                        let urlString = bannerImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                        self.imgView.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
        
                        })
                    }
        lbldescription.text = DetailsData["description"] as! String
        if UIDevice.current.userInterfaceIdiom == .pad
        {

            let bannerImgStr = DetailsData["ipad_background_image"] as! String
            if bannerImgStr != nil
            {
                let urlString = bannerImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                self.BackImage.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in

                })
            }
        }
        else
        {
            let bannerImgStr = DetailsData["iphone_background_image"] as! String
            if bannerImgStr != nil
            {
                let urlString = bannerImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                self.BackImage.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in

                })
            }
        }
        
        LatLOngArray = DetailsData["trail_coordinates"] as! Array<Any>
        for var i in (0..<LatLOngArray.count){
           
            var dict1: Dictionary = self.LatLOngArray[i] as! Dictionary<String,Any>
            let strLat = dict1["latitude"] as! String
            let strLong = dict1["longitude"] as! String
            let combing = "/" + strLat + "," + strLong
           
            
            //finalStr.append(combing)
            finalStr.append(contentsOf: combing)
        }
        print(finalStr)
    }
    
   
    
    @IBAction func btnBackAction(_ sender: Any)
    {
         POP_VC()
        
        
    }
    
    
    @IBAction func btnActionShowTrail(_ sender: Any)
    {
//
        let strLink =  "https://www.google.com/maps/dir" + finalStr
        
        let googleMapUrlString = strLink
        if let url = URL(string: googleMapUrlString)
        {
            UIApplication.shared.openURL(url)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
