//
//  HomeVC.swift
//  MuseumApp
//
//  Created by CCT Macmini on 31/12/18.
//  Copyright © 2018 CCT Macmini. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
import BarcodeScanner
import SystemConfiguration


class HomeVC: UIViewController
{
  
    @IBOutlet weak var btnNextPage: UIButton!
    @IBOutlet weak var btnNextPageText: UIButton!
    
    @IBOutlet weak var lblHeadingText: UILabel!
    @IBOutlet weak var btnPrevPage: UIButton!
    @IBOutlet weak var btnHomePage: UIButton!
    @IBOutlet weak var btnCameraIcon: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var NochView: UIView!
    @IBOutlet weak var DevPrevImg: UIImageView!
    @IBOutlet weak var DevNextImg: UIImageView!
    @IBOutlet weak var backImg: UIImageView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var ArrowbtnNextPage: UIButton!
    @IBOutlet weak var ArrowbtnPrevPage: UIButton!
    
    @IBOutlet weak var CameraViewTopCons: NSLayoutConstraint!
    var appDel: AppDelegate!
    var strScanBarcode:String!
   
    @IBAction func btnNextPage(_ sender: Any)
    {
        
        let alert = UIAlertController(title: Constant.alertTitle, message: "Are you sure you want to log out?", preferredStyle: .alert)
      
       
        let action1 = UIAlertAction(title: "Yes", style: .default)
        { (action:UIAlertAction) in
            
            let domain = Bundle.main.bundleIdentifier!
            UserDefaults.standard.removePersistentDomain(forName: domain)
            UserDefaults.standard.synchronize()
                                          
            if UIDevice.current.userInterfaceIdiom == .pad
            {
                let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
                let viewVC = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                self.navigationController?.pushViewController(viewVC, animated: false)
            }
           else
           {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewVC = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            self.navigationController?.pushViewController(viewVC, animated: false)
        }
          
         // self.getLogoutMethod()
            
        }
        alert.addAction(action1)
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    @IBAction func btnPrevPage(_ sender: Any)
    {
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
            let MuseumIntroduction = storyboard.instantiateViewController(withIdentifier: "MuseumIntroductionVC") as! MuseumIntroductionVC
            self.navigationController?.pushViewController(MuseumIntroduction, animated: false)
        }
        else
        {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let MuseumIntroduction = storyboard.instantiateViewController(withIdentifier: "MuseumIntroductionVC") as! MuseumIntroductionVC
            self.navigationController?.pushViewController(MuseumIntroduction, animated: false)
        }
        
        // self.navigationController?.popViewController(animated: true)
    }
   
    public class Reachability
    {

        class func isConnectedToNetwork() -> Bool
        {

            var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
            zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
            zeroAddress.sin_family = sa_family_t(AF_INET)

            let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
                $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                    SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
                }
            }

            var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
            if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
                return false
            }

            /* Only Working for WIFI
            let isReachable = flags == .reachable
            let needsConnection = flags == .connectionRequired

            return isReachable && !needsConnection
            */

            // Working for Cellular and WIFI
            let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
            let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
            let ret = (isReachable && !needsConnection)

            return ret

        }
    }
    
    private func makeBarcodeScannerViewController() -> BarcodeScannerViewController
    {
        let viewController = BarcodeScannerViewController()
        viewController.codeDelegate = self
        viewController.errorDelegate = self
        viewController.dismissalDelegate = self
        return viewController
    }
    
    @IBAction func btnCameraIcon(_ sender: Any)
      {
        
        if Reachability.isConnectedToNetwork()
        {
            let viewController = makeBarcodeScannerViewController()
            viewController.title = "Barcode Scanner"
            present(viewController, animated: true, completion: nil)
        }
        
        else
        {
            let alert = UIAlertController(title: Constant.alertTitle, message: Constant.noInternetMessage, preferredStyle: .alert)
                       alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                      self.present(alert, animated: true)
        }
        
        
       
        
      }
    
  
    override func viewDidLoad()
    {
        super.viewDidLoad()
     
        if(appDel == nil)
        {
            self.appDel = UIApplication.shared.delegate as? AppDelegate
        }
        
//        strScanBarcode = "item15560158612"
//        self.getItemDetails()
       // self.PUSH_STORY_R1(Identifier:"TrailviewController")
      
        self.CameraConsSet()
        lblHeadingText.text = appDel.configDict["how_to_use_text"] as? String
         //  self.HeadingTextSet()
         // Do any additional setup after loading the view.
    }
    
    
     func HeadingTextSet()
     {
        
        var strText = appDel.configDict["how_to_use_text"] as! String
        // webview.loadHTMLString(str, baseURL: nil)
        strText = strText.replacingOccurrences(of: "\r\n\r\n", with: "<br/>")
        let htmlData = NSString(string: strText).data(using: String.Encoding.unicode.rawValue)
        let paragraphStyle = NSMutableParagraphStyle.init()
        paragraphStyle.alignment = .center
        let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]
        let attributedString = try! NSMutableAttributedString(data: htmlData!, options: options, documentAttributes: nil)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: NSRange.init(location: 0, length: attributedString.string.count))
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSRange.init(location: 0, length: attributedString.string.count))
        textView.isUserInteractionEnabled = false
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            
            if Device.IS_IPAD_PRO
            {
                attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name:"DINNeuzeitGrotesk-BoldCond",size:48)!, range: NSRange.init(location: 0, length: attributedString.string.count))
                //DINNeuzeitGrotesk-Light 20.0
                textView.attributedText = attributedString
                textView.textContainerInset =  UIEdgeInsets(top:160, left: 30, bottom:5, right: 30)
            }
                
            else
            {
                attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name:"DINNeuzeitGrotesk-BoldCond",size:42)!, range: NSRange.init(location: 0, length: attributedString.string.count))
                //DINNeuzeitGrotesk-Light 20.0
                textView.attributedText = attributedString
                textView.textContainerInset =  UIEdgeInsets(top:90, left: 30, bottom:5, right: 30)
            }
        }
            
        else
        {
            if Device.IS_IPHONE_6
            {
                attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name:"DINNeuzeitGrotesk-BoldCond",size:29)!, range: NSRange.init(location: 0, length: attributedString.string.count))
                //DINNeuzeitGrotesk-Light 20.0
                textView.attributedText = attributedString
                textView.textContainerInset =  UIEdgeInsets(top:30, left: 30, bottom:5, right: 30)
                
            }
                
            else if Device.IS_IPHONE_5
            {
                attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name:"DINNeuzeitGrotesk-BoldCond",size:22)!, range: NSRange.init(location: 0, length: attributedString.string.count))
                //DINNeuzeitGrotesk-Light 20.0
                textView.attributedText = attributedString
                textView.textContainerInset =  UIEdgeInsets(top:30, left: 30, bottom:5, right: 30)
                
            }
            else if Device.IS_IPHONE_6P
            {
                attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name:"DINNeuzeitGrotesk-BoldCond",size:29)!, range: NSRange.init(location: 0, length: attributedString.string.count))
                
                textView.attributedText = attributedString
                textView.textContainerInset =  UIEdgeInsets(top:40, left: 30, bottom:5, right: 30)
            }
            else
            {
                attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name:"DINNeuzeitGrotesk-BoldCond",size:30)!, range: NSRange.init(location: 0, length: attributedString.string.count))
                //DINNeuzeitGrotesk-Light 20.0
                textView.attributedText = attributedString
                textView.textContainerInset =  UIEdgeInsets(top:60, left: 30, bottom:5, right: 30)
            }
            
        }

        
     }
    
     func CameraConsSet()
     {
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            
         }
        
        else
        {
            if Device.IS_IPHONE_5
            {
                CameraViewTopCons.constant = 20
            }
            else
            {
                 CameraViewTopCons.constant = 70
            }
            
        }
   
     }
    
    func getItemDetails()
    {
        
        if !self.appDel.CheckForInternetConnection()
        {
            let alert = UIAlertController(title: Constant.alertTitle, message: Constant.noInternetMessage, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }

//        let parameters: Parameters = [
//            "barcode" : "quod-aut-aliquam",
//            "token" :self.appDel.strToken
//        ]

        let parameters: Parameters = [
            "barcode" : strScanBarcode,
            "token" :self.appDel.strToken
        ]
        
        self.appDel.ShowHUD()
         let url = UserDefaults.standard.object(forKey: "Base_URL") as! String
        let requestURL: String =  url + Constant.GET_ITEM_DETAILS
        print(requestURL)
        print(parameters)
        Alamofire.request(requestURL, method: .post, parameters: parameters
            , encoding: JSONEncoding.default, headers: nil)
            .responseJSON { response in
                self.appDel.HideHUD()
                switch response.result
                {
                case .success(_):
                    
                    if let data = response.result.value
                    {
                        let tempArray = data as! Dictionary<String,Any>
                        print(tempArray)
                        if tempArray["code"] != nil
                        {
                            if tempArray["code"] as! Int == 200
                            {
                                    
                                    self.appDel.itemDetails =  tempArray["detail"] as? Dictionary<String, Any>
                                   
                                    if UIDevice.current.userInterfaceIdiom == .pad
                                    {
                                        let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
                                        let Menu = storyboard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
                                        self.navigationController?.pushViewController(Menu, animated: false)
                                    }
                                    else
                                    {
                                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        let Menu = storyboard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
                                        self.navigationController?.pushViewController(Menu, animated: false)
                                    }
                            }
                              
                            else if tempArray["code"] as! Int == 300
                            {
                                
                                let domain = Bundle.main.bundleIdentifier!
                                UserDefaults.standard.removePersistentDomain(forName: domain)
                                UserDefaults.standard.synchronize()
                                
                                let ErrorMsg1 = tempArray["message"] as! String
                                let alert = UIAlertController(title: Constant.alertTitle, message: ErrorMsg1, preferredStyle: .alert)
                              //  alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                              //  self.present(alert, animated: true)
                             
                                
                                let action12 = UIAlertAction(title: "Ok", style: .default)
                                {
                                    (action:UIAlertAction) in
                                    
                                    if UIDevice.current.userInterfaceIdiom == .pad
                                    {
                                        let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
                                        let viewVC = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                        self.navigationController?.pushViewController(viewVC, animated: false)
                                    }
                                    else
                                    {
                                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        let viewVC = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                        self.navigationController?.pushViewController(viewVC, animated: false)
                                    }
                                    
                                    
                                }
                                alert.addAction(action12)
                                self.present(alert, animated: true)
                            }
                            
                                
                            else
                            {
                                let ErrorMsg = tempArray["message"] as! String
                                let alert = UIAlertController(title: Constant.alertTitle, message: ErrorMsg, preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                                self.present(alert, animated: true)
                            }
                            
                        }
                        else
                        {
                            let ErrorMsg = tempArray["message"] as! String
                            let alert = UIAlertController(title: Constant.alertTitle, message: ErrorMsg, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                            self.present(alert, animated: true)
                        }
                        
                        //self.appDel.configDict =  tempArray["config"] as? Dictionary<String, Any>
                    }
                    break
                    
                case .failure(_):
                    print(response.result.error!)
                    self.appDel.HideHUD()
                    DispatchQueue.main.async
                        {
                            let alert = UIAlertController(title: Constant.alertTitle, message: "Something went wrong, please try later.", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                            self.present(alert, animated: true)
                    }
                    break
                }
        }
    }
    
    
    
    func getLogoutMethod()
    {
        
        if !self.appDel.CheckForInternetConnection()
        {
            let alert = UIAlertController(title: Constant.alertTitle, message: Constant.noInternetMessage, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }
        
        //        let parameters: Parameters = [
        //            "barcode" : "quod-aut-aliquam",
        //            "token" :self.appDel.strToken
        //        ]
        
        let parameters: Parameters = [
            "token" :self.appDel.strToken
        ]
        
        self.appDel.ShowHUD()
         let url = UserDefaults.standard.object(forKey: "Base_URL") as! String
        let requestURL: String = url + Constant.LOGOUT
        Alamofire.request(requestURL, method: .post, parameters: parameters
            , encoding: JSONEncoding.default, headers: nil)
            .responseJSON { response in
                self.appDel.HideHUD()
                switch response.result
                {
                case .success(_):
                    
                    if let data = response.result.value
                    {
                        let tempArray = data as! Dictionary<String,Any>
                        
                        if tempArray["code"] != nil
                        {
                            if tempArray["code"] as! Int == 200
                            {
                                
                                let domain = Bundle.main.bundleIdentifier!
                                UserDefaults.standard.removePersistentDomain(forName: domain)
                                UserDefaults.standard.synchronize()
                               
                                if UIDevice.current.userInterfaceIdiom == .pad
                                {
                                    let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
                                    let viewVC = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                    self.navigationController?.pushViewController(viewVC, animated: false)
                                }
                                else
                                {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let viewVC = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                    self.navigationController?.pushViewController(viewVC, animated: false)
                                }
                            }
                            else
                            {
                                let ErrorMsg = tempArray["message"] as! String
                                let alert = UIAlertController(title: Constant.alertTitle, message: ErrorMsg, preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                                self.present(alert, animated: true)
                            }
                            
                        }
                        else
                        {
                            let ErrorMsg = tempArray["message"] as! String
                            let alert = UIAlertController(title: Constant.alertTitle, message: ErrorMsg, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                            self.present(alert, animated: true)
                        }
                        
                        //self.appDel.configDict =  tempArray["config"] as? Dictionary<String, Any>
                    }
                    break
                    
                case .failure(_):
                    print(response.result.error!)
                    self.appDel.HideHUD()
                    DispatchQueue.main.async
                        {
                            let alert = UIAlertController(title: Constant.alertTitle, message: "Something went wrong, please try later.", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                            self.present(alert, animated: true)
                    }
                    break
                }
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated) // No need for semicolon
        
        if(appDel == nil)
        {
            appDel = UIApplication.shared.delegate as? AppDelegate
        }
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.BottomBarButtonHideShowMethod()
        self.ConfigInformation()
    }
    
    func BottomBarButtonHideShowMethod()
    {
      
        btnNextPage.isHidden = false
        btnNextPage.isEnabled = true
      
       // btnNextPage.isHidden = true
       // btnNextPage.isEnabled = false
        btnPrevPage.isHidden = false
        btnPrevPage.isEnabled = true
        btnHomePage.isHidden = true
        btnHomePage.isEnabled = false
        DevPrevImg.isHidden = false
        DevNextImg.isHidden = false
    }
    
    func ConfigInformation()
    {
        
        //bottomView Theme Color set
        
        let colorValue_BottomView : UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["theme_color"] as! String)
        self.bottomView.backgroundColor = colorValue_BottomView
        
        
        //NochView Theme Color set
        
        let colorValue_NochView : UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["theme_color"] as! String)
        self.NochView.backgroundColor = colorValue_NochView
        
        
       // btnNextPageText
        
        let colorValue_button: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["tab_color"] as! String)
        self.btnNextPageText.setTitleColor(colorValue_button, for: .normal)
        self.btnNextPageText.setTitle("Log Out", for: UIControl.State.normal)
        self.btnPrevPage.setTitleColor(colorValue_button, for: .normal)
        self.btnPrevPage.setTitle("Previous", for: UIControl.State.normal)
        
    
       // camera icon image set
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            let tabBarCamImg = appDel.configDict["ipad_screen2_camera_image"] as! String
            if tabBarCamImg != nil
            {
                
                let urlString = tabBarCamImg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                self.btnCameraIcon.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
                    , completed: { (image, error, cacheType, imageURL) in
                        self.btnCameraIcon.backgroundImage(for: UIControl.State.normal)
                })
                
            }
        }
        else
        {
            let tabBarCamImg = appDel.configDict["iphone_screen2_camera_image"] as! String
            if tabBarCamImg != nil
            {
                
                let urlString = tabBarCamImg.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                self.btnCameraIcon.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
                    , completed: { (image, error, cacheType, imageURL) in
                        self.btnCameraIcon.backgroundImage(for: UIControl.State.normal)
                })
                
            }
        }
        
      
        
        //btn Next page
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            let tabBarImg3 = appDel.configDict["ipad_logout_image"] as! String
            if tabBarImg3 != nil
            {
                
                let urlString = tabBarImg3.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                self.btnNextPage.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
                    , completed: { (image, error, cacheType, imageURL) in
                        self.btnNextPage.backgroundImage(for: UIControl.State.normal)
                })
                
            }
        }
        else
        {
            let tabBarImg3 = appDel.configDict["iphone_logout_image"] as! String
            if tabBarImg3 != nil
            {
                
                let urlString = tabBarImg3.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                self.btnNextPage.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
                    , completed: { (image, error, cacheType, imageURL) in
                        self.btnNextPage.backgroundImage(for: UIControl.State.normal)
                })
                
            }
        }
        
      
        
        //btn Prev page
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            let tabBarImg1 = appDel.configDict["ipad_bottom_icon_1"] as! String
            if tabBarImg1 != nil
            {
                
//                let urlString = tabBarImg1.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//
//                self.btnPrevPage.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
//                    , completed: { (image, error, cacheType, imageURL) in
//                        self.btnPrevPage.backgroundImage(for: UIControl.State.normal)
//                })
                
            }
        }
        else
        {
            let tabBarImg1 = appDel.configDict["iphone_bottom_icon_1"] as! String
            if tabBarImg1 != nil
            {
                
//                let urlString = tabBarImg1.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//                
//                self.btnPrevPage.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
//                    , completed: { (image, error, cacheType, imageURL) in
//                        self.btnPrevPage.backgroundImage(for: UIControl.State.normal)
//                })
                
            }
        }
        
    
        //BackGround ImgSet
        
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            let bannerImgStr = appDel.configDict["ipad_screen3_background_image"] as! String
            
            if bannerImgStr != nil
            {
                let urlString = bannerImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                self.backImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    
                })
            }
            
        }
        else
        {
            let bannerImgStr = appDel.configDict["iphone_screen3_background_image"] as! String
            
            if bannerImgStr != nil
            {
                let urlString = bannerImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                self.backImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    
                })
            }
            
        }
        
      
        //Devider Next/Prev image set
        
        let colorValue_devColor: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["tab_color"] as! String)
        DevNextImg.backgroundColor = colorValue_devColor
        DevPrevImg.backgroundColor = colorValue_devColor
        
    }
   
}
// MARK: - BarcodeScannerCodeDelegate
extension HomeVC: BarcodeScannerCodeDelegate
{
    func scanner(_ controller: BarcodeScannerViewController, didCaptureCode code: String, type: String)
    {
        print("Barcode Data: \(code)")
        print("Symbology Type: \(type)")
       // let string = "The rain in Spain"
        let stringResult = code.contains("trail_")
        if stringResult
        {
           
           
            DispatchQueue.main.async {
                controller.dismiss(animated: true)
                {
                   
                    let str = code
                    var newStr = (str as? NSString)?.substring(from: 6)
                    print(newStr)
                    UserDefaults.standard.set(newStr, forKey: "trail") //setObject
                    UserDefaults.standard.set("true", forKey: "CallAPI")
                    
                    if UIDevice.current.userInterfaceIdiom == .pad
                    {
                        self.PUSH_STORY_R1(Identifier:"trailPadVc")
                    }
                    else
                    {
                        self.PUSH_STORY_R1(Identifier:"TrailviewController")
                    }
                }
            //}
            }
        }
        else{
            strScanBarcode = code
            //lblActivationCode.text = code
            controller.dismiss(animated: true, completion: nil)
            self.getItemDetails()
        }
       
        
        
        
        //DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
        //controller.resetWithError()
        
        //}
    }
    
}
// MARK: - BarcodeScannerErrorDelegate
extension HomeVC: BarcodeScannerErrorDelegate
{
    func scanner(_ controller: BarcodeScannerViewController, didReceiveError error: Error)
    {
        
        //lblActivationCode.text = "Error In Barcode"
        print(error)
    }
}


// MARK: - BarcodeScannerDismissalDelegate

extension HomeVC: BarcodeScannerDismissalDelegate
{
    func scannerDidDismiss(_ controller: BarcodeScannerViewController)
    {
        controller.dismiss(animated: true, completion: nil)
    }
}
