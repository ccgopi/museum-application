//
//  ViewController.swift
//  MuseumApp
//
//  Created by CCT Macmini on 31/12/18.
//  Copyright © 2018 CCT Macmini. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import BarcodeScanner

class ViewController: UIViewController
{
    @IBOutlet weak var lblActivationCode: UILabel!
    @IBOutlet weak var btnActivationCode: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    var appDel: AppDelegate!
    
   
    @IBOutlet weak var btnTrail: UIButton!
    
   
    override func viewDidLoad()
    {
        super.viewDidLoad()
       
        if(appDel == nil)
        {
            self.appDel = UIApplication.shared.delegate as! AppDelegate
        }
    
        self.buttonBorderMethod()
        
       //  lblActivationCode.text = "ticket15617029472"
      //  VerifyTicket()
      }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        AppUtility.lockOrientation(.portrait)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    
        // Or to rotate and lock
        // AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
        
    }
    
     func buttonBorderMethod()
     {
        btnActivationCode.backgroundColor = .clear
        btnActivationCode.layer.cornerRadius = 5
        btnActivationCode.layer.borderWidth = 2
        btnActivationCode.layer.borderColor = UIColor.darkGray.cgColor
        
        btnSubmit.backgroundColor = .clear
        btnSubmit.layer.cornerRadius = 5
        btnSubmit.layer.borderWidth = 2
        btnSubmit.layer.borderColor = UIColor.darkGray.cgColor
        
        btnTrail.backgroundColor = .clear
        btnTrail.layer.cornerRadius = 5
        btnTrail.layer.borderWidth = 2
        btnTrail.layer.borderColor = UIColor.darkGray.cgColor
        
     }
    
    func ConfigInformation()
    {
        if !self.appDel.CheckForInternetConnection()
        {
            
            let alert = UIAlertController(title: Constant.alertTitle, message: Constant.noInternetMessage, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }
        
        
        self.appDel.ShowHUD()
        let url = UserDefaults.standard.object(forKey: "Base_URL") as! String

        let requestURL: String = url + Constant.CONFIGAPI
       
        Alamofire.request(requestURL, method: .get, parameters: nil
            , encoding: JSONEncoding.default, headers: nil)
            .responseJSON { response in
                
                self.appDel.HideHUD()
                switch response.result
                {
                case .success(_):
                    
                    if let data = response.result.value
                    {
                        let tempArray = data as! Dictionary<String,Any>
                        self.appDel.configDict =  tempArray["config"] as? Dictionary<String, Any>
                        print(self.appDel.configDict)
                        UserDefaults.standard.set("YES", forKey: "isLogin") //setObject
                        UserDefaults.standard.set(self.appDel.configDict, forKey: "configDictValue") //setObject
                        
                        if UIDevice.current.userInterfaceIdiom == .pad
                        {
                            let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
                            let CustomerLanding = storyboard.instantiateViewController(withIdentifier: "CustomerLandingVC") as! CustomerLandingVC
                            self.navigationController?.pushViewController(CustomerLanding, animated: false)
                        }
                        else
                        {
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let CustomerLanding = storyboard.instantiateViewController(withIdentifier: "CustomerLandingVC") as! CustomerLandingVC
                            self.navigationController?.pushViewController(CustomerLanding, animated: false)
                        }
                    }
                    break
                    
                case .failure(_):
                    print(response.result.error!)
                    
                    DispatchQueue.main.async
                        {
                            let alert = UIAlertController(title: Constant.alertTitle, message: "Something went wrong, please try later.", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                            self.present(alert, animated: true)
                    }
                    break
                }
        }
    }
    @IBAction func ScanBarcode(_ sender: Any) {
        
        let viewController = makeBarcodeScannerViewController()
        viewController.title = "Barcode Scanner"
        present(viewController, animated: true, completion: nil)
    }
    private func makeBarcodeScannerViewController() -> BarcodeScannerViewController
    {
        let viewController = BarcodeScannerViewController()
        viewController.codeDelegate = self
        viewController.errorDelegate = self
        viewController.dismissalDelegate = self
        return viewController
    }
    @IBAction func btnActivationCode(_ sender: Any)
    {
        let viewController = makeBarcodeScannerViewController()
        viewController.title = "Barcode Scanner"
        present(viewController, animated: true, completion: nil)

        
        
//
//            if UIDevice.current.userInterfaceIdiom == .pad
//            {
//                let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
//                let viewVC = storyboard.instantiateViewController(withIdentifier: "CustomerLandingVC") as! CustomerLandingVC
//                self.navigationController?.pushViewController(viewVC, animated: false)
//            }
//           else
//           {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let viewVC = storyboard.instantiateViewController(withIdentifier: "CustomerLandingVC") as! CustomerLandingVC
//            self.navigationController?.pushViewController(viewVC, animated: false)
//        }
    }
    
    @IBAction func btnSubmit(_ sender: Any)
    {
        self.VerifyTicket()
        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let CustomerLanding = storyboard.instantiateViewController(withIdentifier: "CustomerLandingVC") as! CustomerLandingVC
//     self.navigationController?.pushViewController(CustomerLanding, animated: true)
    }
   
    func VerifyTicket()
    {
        if lblActivationCode.text == "ACTIVATION CODE"
        {
            print("Please Scan Barcode");
            let alert = UIAlertController(title: Constant.alertTitle, message: "Please scan QR code.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true)
            return;
        }
        else if lblActivationCode.text == "Error In Barcode"
        {
            print("Error In Barcode");
            let alert = UIAlertController(title: Constant.alertTitle, message: "Error In Barcode", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true)
            return;
        }
        if !self.appDel.CheckForInternetConnection()
        {
            let alert = UIAlertController(title: Constant.alertTitle, message: Constant.noInternetMessage, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }
        let parameters: Parameters = [
            "decoded_barcode" : lblActivationCode.text!,
           "device_id" : Constant.deviceID!
       ]

        self.appDel.ShowHUD()
//        let parameters: Parameters = [
//            "decoded_barcode" : "TKT67890",
//            "device_id" :"12345"
//        ]
        let url = UserDefaults.standard.object(forKey: "Base_URL") as! String
        let requestURL: String =  url + Constant.VERIFY_TICKET
        Alamofire.request(requestURL, method: .post, parameters: parameters
            , encoding: JSONEncoding.default, headers: nil)
            .responseJSON { response in
                
                self.appDel.HideHUD()
                switch response.result
                {
                
                case .success(_):
                    
                    
                    if let data = response.result.value
                    {
                        let tempArray = data as! Dictionary<String,Any>
                        print(tempArray)
                        if tempArray["code"] != nil
                        {
                            if tempArray["code"] as! Int == 200
                            {
                               
                                 self.appDel.strToken = tempArray["token"] as? String
                                 UserDefaults.standard.set( self.appDel.strToken, forKey: "Token") //setObject
                                 self.ConfigInformation()
                            }
                            else
                            {
                                let ErrorMsg = tempArray["message"] as! String
                                let alert = UIAlertController(title: Constant.alertTitle, message: ErrorMsg, preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                                self.present(alert, animated: true)
                            }
                            
                        }
                        else
                        {
                            let ErrorMsg = tempArray["message"] as! String
                            let alert = UIAlertController(title: Constant.alertTitle, message: ErrorMsg, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                            self.present(alert, animated: true)
                        }
                        
                        //self.appDel.configDict =  tempArray["config"] as? Dictionary<String, Any>
                    }
                    break
                    
                case .failure(_):
                    print(response.result.error!)
                    
                    DispatchQueue.main.async
                        {
                            let alert = UIAlertController(title: Constant.alertTitle, message: "Something went wrong, please try later.", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                            self.present(alert, animated: true)
                       }
                    break
                }
        }
    }
    
    @IBAction func btnActionTrail(_ sender: Any) {
       
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
          PUSH_STORY_R1(Identifier:"trailPadVc")
        }
        else
        {
            PUSH_STORY_R1(Identifier:"TrailviewController")
        }
       
        
    }
    
}
// MARK: - BarcodeScannerCodeDelegate
extension ViewController: BarcodeScannerCodeDelegate
{
    func scanner(_ controller: BarcodeScannerViewController, didCaptureCode code: String, type: String) {
        print("Barcode Data: \(code)")
        print("Symbology Type: \(type)")
        
        let fullCode : String = code
       
        if fullCode.contains("#")
        {
            let fullCodeArr : [String] = fullCode.components(separatedBy: "#")
            lblActivationCode.text = fullCode
            // And then to access the individual words:
            let Code : String! = fullCodeArr[0]
            let Url : String! = fullCodeArr[1]
            let strCode = String(Code)
            lblActivationCode.text = strCode
            let strURL = String(Url)
            UserDefaults.standard.set(strURL, forKey: "Base_URL")
            controller.dismiss(animated: true, completion: nil)
            VerifyTicket()
        }
        
        else
        {
            controller.dismiss(animated: true, completion: nil)
            let alert = UIAlertController(title: Constant.alertTitle, message: "Please scan valid QR code.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true)
            
        }
     
        // (
//        print("Barcode Data: \(code)")
//        print("Symbology Type: \(type)")
//        lblActivationCode.text = code
//        controller.dismiss(animated: true, completion: nil)
//        VerifyTicket()
        //DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
            //controller.resetWithError()
            
        //}
    }
}
// MARK: - BarcodeScannerErrorDelegate
extension ViewController: BarcodeScannerErrorDelegate
{
    func scanner(_ controller: BarcodeScannerViewController, didReceiveError error: Error) {
        
        lblActivationCode.text = "Error In Barcode"
        controller.dismiss(animated: true, completion: nil)
        print(error)
    }
}

// MARK: - BarcodeScannerDismissalDelegate

extension ViewController: BarcodeScannerDismissalDelegate
{
    func scannerDidDismiss(_ controller: BarcodeScannerViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}

