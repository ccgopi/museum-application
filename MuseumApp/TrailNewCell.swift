//
//  TrailNewCell.swift
//  MuseumApp
//
//  Created by CCT Macmini on 21/06/19.
//  Copyright © 2019 CCT Macmini. All rights reserved.
//

import UIKit

class TrailNewCell: UITableViewCell
{

    @IBOutlet weak var viewTitle: UIView!
    @IBOutlet weak var viewImg: UIView!
    @IBOutlet weak var viewTextDynamic: UIView!
    @IBOutlet weak var viewMapbtn: UIView!
    @IBOutlet weak var viewWebsitebtn: UIView!
    @IBOutlet weak var imgTrail: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDynamicTxt: UILabel!
    @IBOutlet weak var btnMap: UIButton!
    @IBOutlet weak var btnVisitWebsite: UIButton!
   
    @IBOutlet weak var imageViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
