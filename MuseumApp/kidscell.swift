//
//  kidscell.swift
//  MuseumApp
//
//  Created by CCT Macmini on 28/01/19.
//  Copyright © 2019 CCT Macmini. All rights reserved.
//

import UIKit

class kidscell: UICollectionViewCell, UIScrollViewDelegate
{

    @IBOutlet weak var btnAnswer1: customButton!
    @IBOutlet weak var btnAnswer2: customButton!
    @IBOutlet weak var btnAnswer3: customButton!
    @IBOutlet weak var btnAnswer4: customButton!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var btnDisplayCorrectAnswer: UILabel!
    @IBOutlet weak var btnQuesPrev: UIButton!
    @IBOutlet weak var btnQuesNext: UIButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
        
        // 2. add the gesture recognizer to a view
        scrollView.addGestureRecognizer(tapGesture)
    
    }
    @objc func handleTap(sender: UITapGestureRecognizer)
    {
        print("tap")
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        
    }
}
