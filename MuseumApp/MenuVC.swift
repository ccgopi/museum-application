//
//  MenuVC.swift
//  MuseumApp
//
//  Created by CCT Macmini on 02/01/19.
//  Copyright © 2019 CCT Macmini. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire

class MenuVC: UIViewController
{

    
    @IBOutlet weak var hightGallery: NSLayoutConstraint!
    @IBOutlet weak var blurViewTopCons: NSLayoutConstraint!
    @IBOutlet weak var aboutViewTopCons: NSLayoutConstraint!
    @IBOutlet weak var btnNextPage: UIButton!
    @IBOutlet weak var btnPrevPage: UIButton!
    @IBOutlet weak var btnHomePage: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var DevPrevImg: UIImageView!
    @IBOutlet weak var DevNextImg: UIImageView!
    @IBOutlet weak var backImg: UIImageView!
    @IBOutlet weak var btnAbout: UIButton!
    @IBOutlet weak var btnFactSheet: UIButton!
    @IBOutlet weak var btnVideos: UIButton!
    @IBOutlet weak var btnKids: UIButton!
    @IBOutlet weak var btnTrail: UIButton!
    @IBOutlet weak var NochView: UIView!
    @IBOutlet weak var aboutImg: UIImageView!
    @IBOutlet weak var factsheetImg: UIImageView!
    @IBOutlet weak var videosImg: UIImageView!
    @IBOutlet weak var kidsImg: UIImageView!
    @IBOutlet weak var trailImg: UIImageView!
    @IBOutlet weak var lblAbout: UILabel!
    @IBOutlet weak var lblFactsheet: UILabel!
    @IBOutlet weak var lblvideos: UILabel!
    @IBOutlet weak var lblkids: UILabel!
    @IBOutlet weak var lbltrail: UILabel!
    @IBOutlet weak var lblTitleName: UILabel!
    @IBOutlet weak var ArrowbtnNextPage: UIButton!
    @IBOutlet weak var ArrowbtnPrevPage: UIButton!
    
    
    @IBOutlet weak var lblGallery: UIImageView!
    
    var aboutUsDict: Dictionary<String,Any>!
    var Gallery: Dictionary<String,Any>!
    var factSheetDict: Dictionary<String,Any>!
    var videoDict: Dictionary<String,Any>!
    var kidsDict: Dictionary<String,Any>!
    var trailUsDict: Dictionary<String,Any>!
    var AboutCheck: Int!
    var check: Bool!
    var factSheet: Int!
    var Videos: Int!
    var Kids: Int!
    var appDel: AppDelegate!
   
    @IBOutlet weak var hightAbtView: NSLayoutConstraint!
    @IBOutlet weak var hightFactView: NSLayoutConstraint!
    @IBOutlet weak var hightVideoView: NSLayoutConstraint!
    @IBOutlet weak var hightKidsView: NSLayoutConstraint!
    @IBOutlet weak var kidsView: UIView!
    @IBOutlet weak var videosView: UIView!
    @IBOutlet weak var factSheetView: UIView!
    
    @IBOutlet weak var viewGallery: UIView!
    
    
    @IBOutlet weak var btnNexttransPerent: UIButton!
    
    @IBOutlet weak var viewAbout: UIView!
    
    @IBAction func btnNextPage(_ sender: Any)
    {
        
       // if AboutCheck == 1{
    
        
        //self.btnNextPage.setTitleColor(.red, for: .normal)
        
        if aboutUsDict["status"] as! String != "N"
        {
      
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            
           PUSH_STORY_R1(Identifier: "AboutVC")
            return

        }
        else
        {
            PUSH_STORY_R1(Identifier: "AboutVC")
            return
            
            
        }
            
        }
    
        if Gallery["status"] as! String != "N"
         {
            //self.btnNextPage.setTitle("Gallery ", for: UIControl.State.normal)
            if UIDevice.current.userInterfaceIdiom == .pad
            {
                PUSH_STORY_R1(Identifier: "GalleryVC1")
                return
            }
            else
            {
                PUSH_STORY_R1(Identifier: "idGallery")
                return
            }
            
        }
       
         if factSheetDict["status"] as! String != "N"
         {
           // self.btnNextPage.setTitle("Fact sheet ", for: UIControl.State.normal)
            if UIDevice.current.userInterfaceIdiom == .pad
            {
                PUSH_STORY_R1(Identifier: "FactSheetVC")
                return
            } else
            {
                PUSH_STORY_R1(Identifier: "FactSheetVC")
                return
            }
            
        }
        
        
      if videoDict["status"] as! String != "N"
      {
         //self.btnNextPage.setTitle("Videos ", for: UIControl.State.normal)
            if UIDevice.current.userInterfaceIdiom == .pad
            {
                PUSH_STORY_R1(Identifier: "VideosVC")
                return
            } else
            {
                PUSH_STORY_R1(Identifier: "VideosVC")
                return
            }
        
        }
        
        if kidsDict["status"] as! String != "N"
        {
           // self.btnNextPage.setTitle("Kids ", for: UIControl.State.normal)
            if UIDevice.current.userInterfaceIdiom == .pad
            {
                PUSH_STORY_R1(Identifier: "KidsVC")
                return
            } else
            {
                PUSH_STORY_R1(Identifier: "KidsVC")
                return
            }
            
        }
        
    }
    
    @IBAction func btnPrevPage(_ sender: Any)
    {
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            POP_VC()

//            let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
//            let home = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
//            self.navigationController?.pushViewController(home, animated: false)
        }
        else
        {
            POP_VC()

//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let home = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
//            self.navigationController?.pushViewController(home, animated: false)
        }
        
        //self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnHomePage(_ sender: Any)
    {
        
        let ErrorMsg = "Are you sure you want to quit viewing this Item?"
        let alert = UIAlertController(title: Constant.alertTitle, message: ErrorMsg, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Yes", style: .default)
        { (action:UIAlertAction) in
            
            if UIDevice.current.userInterfaceIdiom == .pad
            {
              //  self.navigationController?.popViewController(animated: true)
                self.PUSH_STORY_R2(Identifier: "HomeVC")
            }
            else
            {
               // self.navigationController?.popViewController(animated: true)
                self.PUSH_STORY_R2(Identifier: "HomeVC")
            }
         
            
        }
        alert.addAction(action1)
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true)
 
    }
      
  
    @IBAction func btnAbout(_ sender: Any)
    {
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
            let About = storyboard.instantiateViewController(withIdentifier: "AboutVC") as! AboutVC
            self.navigationController?.pushViewController(About, animated: false)
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let About = storyboard.instantiateViewController(withIdentifier: "AboutVC") as! AboutVC
            self.navigationController?.pushViewController(About, animated: false)
        }
        
        
    }
    
    @IBAction func btnFactSheet(_ sender: Any)
    {
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
            let FactSheet = storyboard.instantiateViewController(withIdentifier: "FactSheetVC") as! FactSheetVC
            self.navigationController?.pushViewController(FactSheet, animated: false)
        }
        else
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let FactSheet = storyboard.instantiateViewController(withIdentifier: "FactSheetVC") as! FactSheetVC
        self.navigationController?.pushViewController(FactSheet, animated: false)
        }
    }
    
    @IBAction func btnVideos(_ sender: Any)
    {
          if UIDevice.current.userInterfaceIdiom == .pad
          {
            let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
            let Videos = storyboard.instantiateViewController(withIdentifier: "VideosVC") as! VideosVC
            self.navigationController?.pushViewController(Videos, animated: false)
            
          }
        
        else
          {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let Videos = storyboard.instantiateViewController(withIdentifier: "VideosVC") as! VideosVC
            self.navigationController?.pushViewController(Videos, animated: false)

       
          }
    }
    
    @IBAction func btnKids(_ sender: Any)
    {
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
            let Kids = storyboard.instantiateViewController(withIdentifier: "KidsVC") as! KidsVC
            self.navigationController?.pushViewController(Kids, animated: false)
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let Kids = storyboard.instantiateViewController(withIdentifier: "KidsVC") as! KidsVC
            self.navigationController?.pushViewController(Kids, animated: false)
        }
    }
    
    @IBAction func btnTrail(_ sender: Any)
    {
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
         
            let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
            let Kids = storyboard.instantiateViewController(withIdentifier: "GalleryVC1") as! GalleryVC
            self.navigationController?.pushViewController(Kids, animated: false)
        }
        
        else
        {
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let Trail = storyboard.instantiateViewController(withIdentifier: "idGallery") as! GalleryVC
           self.navigationController?.pushViewController(Trail, animated: false)
        }
        
       }
    
   
    override func viewDidLoad()
    {
        check = true
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated) // No need for semicolon
        
        if(appDel == nil)
        {
            appDel = UIApplication.shared.delegate as? AppDelegate
        }
         self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
        self.BottomBarButtonHideShowMethod()
        self.ConfigInformation()
        self.SetMenuTopConstraint()
        print(self.appDel.itemDetails)
        
        aboutUsDict = appDel.itemDetails["about"] as? Dictionary<String, Any>
        
        // for hidden view if data is not come from web
        if aboutUsDict["status"] as! String == "N"
       {
            viewAbout.isHidden = true
            hightAbtView.constant = 0
           // check = self.checkForHiddenButton(_check: check)
        }
//
        Gallery = appDel.itemDetails["gallery"] as? Dictionary<String, Any>
        
        if Gallery["status"] as! String == "N"
        {
            viewGallery.isHidden = true
            hightGallery.constant = 0
           // check = self.checkForHiddenButton(_check: check)
        }
        factSheetDict = appDel.itemDetails["fact_sheet"] as? Dictionary<String, Any>
        if factSheetDict["status"] as! String == "N"
        {
            factSheetView.isHidden = true
            hightFactView.constant = 0
           // check = self.checkForHiddenButton(_check: check)
        }

       videoDict = appDel.itemDetails["video"] as? Dictionary<String, Any>
        if videoDict["status"] as! String == "N"
        {
            videosView.isHidden = true
            hightVideoView.constant = 0
          //  check = self.checkForHiddenButton(_check: check)
        }

        kidsDict = appDel.itemDetails["kids"] as? Dictionary<String, Any>
       
        if kidsDict["status"] as! String == "N"
        {
        kidsView.isHidden = true
        hightKidsView.constant = 0
       // check = self.checkForHiddenButton(_check: check)
        }
        
        if aboutUsDict["status"] as! String != "N"
        {
            check = checkForHiddenButton(_check: check)
        }
        if Gallery["status"] as! String != "N"
        {
            check = checkForHiddenButton(_check: check)
            
        }
        
        
        if factSheetDict["status"] as! String != "N"
        {
            check = checkForHiddenButton(_check: check)
        }
        
        
        if videoDict["status"] as! String != "N"
        {
            check = checkForHiddenButton(_check: check)
        }
        
        
        if kidsDict["status"] as! String != "N"
        {
            check = checkForHiddenButton(_check: check)
        }
        if check == true
        {
            btnNextPage.isHidden = true
            if UIDevice.current.userInterfaceIdiom == .pad{
            btnNexttransPerent.isHidden = true
            }
        }
        
        self.btnNextPage.setTitleColor(.red, for: .normal)
        
       // trailUsDict = appDel.itemDetails["trail"] as? Dictionary<String, Any>
        
       
        //About Title
        
        let colorValue_lblTitle1: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: aboutUsDict["color"] as! String)
        lblAbout.text = aboutUsDict["title"] as? String
        lblAbout.textColor = colorValue_lblTitle1
        
        
        //Factsheet Title
        
        let colorValue_lblTitle2: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: factSheetDict["color"] as! String)
        lblFactsheet.text = factSheetDict["title"] as? String
        lblFactsheet.textColor = colorValue_lblTitle2
        
        
        //Video Title
        
        let colorValue_lblTitle3: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: videoDict["color"] as! String)
        lblvideos.text = videoDict["title"] as? String
        lblvideos.textColor = colorValue_lblTitle3
        
        
        //Kids Title
        
        let colorValue_lblTitle4: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: kidsDict["color"] as! String)
        lblkids.text = kidsDict["title"] as? String
        lblkids.textColor = colorValue_lblTitle4
        
        
        
        //Trail Title
        
        let colorValue_lblTitle5: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: Gallery["color"] as! String)
        lbltrail.text = Gallery["title"] as? String
        lbltrail.textColor = colorValue_lblTitle5
        
        
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            
            let aboutUSUrl = aboutUsDict["ipad_icon_image"] as! String
            if aboutUSUrl != nil
            {
                let urlString = aboutUSUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                aboutImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    
                })
            }
            
            let factUrl = factSheetDict["ipad_icon_image"] as! String
            if factUrl != nil
            {
                let urlString = factUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                factsheetImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    
                })
            }
            
            let videoUrl = videoDict["ipad_icon_image"] as! String
            if videoUrl != nil
            {
                let urlString = videoUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                videosImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    
                })
            }
            
            let kidsUrl = kidsDict["ipad_icon_image"] as! String
            if kidsUrl != nil
            {
                let urlString = kidsUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                kidsImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    
                })
            }
            
            let trailUrl = Gallery["ipad_icon_image"] as! String
            if trailUrl != nil
            {
                let urlString = trailUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                trailImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    
                })
            }
        }
        else
        
        {
            
            let aboutUSUrl = aboutUsDict["iphone_icon_image"] as! String
            if aboutUSUrl != nil
            {
                let urlString = aboutUSUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                aboutImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    
                })
            }
            
            let factUrl = factSheetDict["iphone_icon_image"] as! String
            if factUrl != nil
            {
                let urlString = factUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                factsheetImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    
                })
            }
            
            let videoUrl = videoDict["iphone_icon_image"] as! String
            if videoUrl != nil
            {
                let urlString = videoUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                videosImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    
                })
            }
            
            let kidsUrl = kidsDict["iphone_icon_image"] as! String
            if kidsUrl != nil
            {
                let urlString = kidsUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                kidsImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    
                })
            }
            
            let trailUrl = Gallery["iphone_icon_image"] as! String
            if trailUrl != nil
            {
                let urlString = trailUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                trailImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in

                })
            }
        }
        lblTitleName.text = appDel.itemDetails["title"] as? String
        lblTitleName.textColor = AppDelegate.sharedInstance().hexStringToUIColor(hex : appDel.itemDetails["title_colour"] as! String)
        self.nextButtonText()
    }
    
    func nextButtonText()
    {
        let colorValue_button: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["tab_color"] as! String)
       // self.btnPrevPage.setTitleColor(colorValue_devColor, for: .normal)
        self.btnNextPage.setTitleColor(colorValue_button, for: .normal)
      
        if aboutUsDict["status"] as! String != "N"
        {
            let str = aboutUsDict["title"] as? String
            self.btnNextPage.setTitle(str!, for: UIControl.State.normal)
            return
            
          //  self.btnNextPage.setTitle("About ", for: UIControl.State.normal)
           
        }
        if Gallery["status"] as! String != "N"
        {
            let str = Gallery["title"] as? String
            self.btnNextPage.setTitle(str!, for: UIControl.State.normal)
           // self.btnNextPage.setTitle("Gallery ", for: UIControl.State.normal)
          
            return
        }
        if factSheetDict["status"] as! String != "N"
        {
            let str = factSheetDict["title"] as? String
            self.btnNextPage.setTitle(str!, for: UIControl.State.normal)
           // self.btnNextPage.setTitle("Fact sheet ", for: UIControl.State.normal)
           return
        }
        if videoDict["status"] as! String != "N"
        {
            let str = videoDict["title"] as? String
            self.btnNextPage.setTitle(str!, for: UIControl.State.normal)
         //   self.btnNextPage.setTitle("Videos ", for: UIControl.State.normal)
            return
        }
        if kidsDict["status"] as! String != "N"
        {
            let str = kidsDict["title"] as? String
            self.btnNextPage.setTitle(str!, for: UIControl.State.normal)
          //  self.btnNextPage.setTitle("Kids ", for: UIControl.State.normal)
           return
        }
    }
    
    func previousButtonText()
    {
        
    }
    
    func SetMenuTopConstraint()
      {
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            if Device.IS_IPAD_PRO
            {
                aboutViewTopCons.constant = 150
                blurViewTopCons.constant = 30
            }
            else
            {
                 aboutViewTopCons.constant = 40
                 blurViewTopCons.constant = 10
            }
        }
        
        else
        {
             if Device.IS_IPHONE_6
             {
                
             }
            
             else if Device.IS_IPHONE_6P
             {
                
             }
            
             else
             {
                
            }
        }
     
    }
    
    func BottomBarButtonHideShowMethod()
    {
        btnNextPage.isHidden = false
        btnNextPage.isEnabled = true
        btnPrevPage.isHidden = true
        btnPrevPage.isEnabled = false
        btnHomePage.isHidden = false
        btnHomePage.isEnabled = true
        DevPrevImg.isHidden = false
        DevNextImg.isHidden = false
    }
    
    func ConfigInformation()
    {
        //BackGround ImgSet
       
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            
            let bannerImgStr = appDel.itemDetails["ipad_background_image"] as! String
        
            if bannerImgStr != nil
            {
                let urlString = bannerImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                self.backImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    
                })
            }
        }
        else
        {
            let bannerImgStr = appDel.itemDetails["iphone_background_image"] as! String
            if bannerImgStr != nil
            {
                let urlString = bannerImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                self.backImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    
                })
            }
        }
        
    //home
        
        let colorValue_button: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["tab_color"] as! String)
        self.btnHomePage.setTitleColor(colorValue_button, for: .normal)
        self.btnHomePage.setTitle("Home", for: UIControl.State.normal)
        
        //bottomView Theme Color set
        let colorValue_BottomView : UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["theme_color"] as! String)
        self.bottomView.backgroundColor = colorValue_BottomView
       
        
        let colorValue_NochView : UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["theme_color"] as! String)
        self.NochView.backgroundColor = colorValue_NochView
     
        
        
        var tabBarImg1 : String!
        var tabBarImg2 : String!
        var tabBarImg3 : String!
        
        //Devider Next/Prev image set
        let colorValue_devColor: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["tab_color"] as! String)
        DevNextImg.backgroundColor = colorValue_devColor
        DevPrevImg.backgroundColor = colorValue_devColor
     
        
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            tabBarImg1 = self.appDel.configDict["ipad_bottom_icon_1"] as? String
            tabBarImg2 = self.appDel.configDict["ipad_bottom_icon_2"] as? String
            tabBarImg3 = self.appDel.configDict["ipad_bottom_icon_3"] as? String
            
        }
        else
        {
            tabBarImg1 = self.appDel.configDict["ipad_bottom_icon_1"] as? String
            tabBarImg2 = self.appDel.configDict["ipad_bottom_icon_2"] as? String
            tabBarImg3 = self.appDel.configDict["ipad_bottom_icon_3"] as? String
            
        }
        
        //btn Prev page
        if tabBarImg1 != nil
        {
//            let urlString = tabBarImg1.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//            self.btnPrevPage.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
//                , completed: { (image, error, cacheType, imageURL) in
//                    self.btnPrevPage.backgroundImage(for: UIControl.State.normal)
//            })
        
            
        }
        
        //btn Next page
        if tabBarImg3 != nil
        {
//            let urlString = tabBarImg3.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//            self.btnNextPage.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
//                , completed: { (image, error, cacheType, imageURL) in
//                    self.btnNextPage.backgroundImage(for: UIControl.State.normal)
//            })
        
           // self.btnNextPage.setTitleColor(.black, for: .normal)
        }
        
        //btn Home page
        if tabBarImg2 != nil
        {
//            let urlString = tabBarImg2.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//            self.btnHomePage.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
//                , completed: { (image, error, cacheType, imageURL) in
//                    self.btnHomePage.backgroundImage(for: UIControl.State.normal)
//            })
            
        }
   
    }
    func checkForHiddenButton(_check : Bool) -> Bool
    {
        check = false
        return check
    }
    
}
