//
//  TrailViewController.swift
//  MuseumApp
//
//  Created by Admin on 22/03/19.
//  Copyright © 2019 CCT Macmini. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import SystemConfiguration

@available(iOS 10.0, *)
class TrailViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var viewTheme: UIView!
    
    var aryImageDetail = [Dictionary<String, Any>]()
    var appDel: AppDelegate!
    var DataDict : NSDictionary = NSDictionary()
    var SaveData : NSMutableArray = NSMutableArray()
    var SaveData2 : Array<Any>!
    var arraytemp = [String]()
    var yourArray = Array<Any>()
    var IdofTrail = [String]()
    let defaults = UserDefaults.standard
     // var arrProductList:NSMutableArray = NSMutableArray()
    var Details : NSDictionary = NSDictionary()
    var DictOffline : NSDictionary = NSDictionary()
    @IBOutlet weak var tblTrail: UITableView!
    @IBOutlet weak var imgBackGround: UIImageView!
    
    var title_Color: String!
    var title_background_Color: String!
    var header_Title_Color: String!
    
     @IBOutlet weak var lblTitleHeader: UILabel!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        self.tblTrail.backgroundColor = UIColor.clear
        
        if(appDel == nil)
        {
            appDel = UIApplication.shared.delegate as? AppDelegate
        }
        print(appDel.configDict["ipad_trail_background_image"])
        tblTrail.tableFooterView = UIView()
        //        if UIDevice.current.userInterfaceIdiom == .pad
        //        {
        //            self.tblTrail.register(UINib.init(nibName: "trailPad", bundle: nil), forCellReuseIdentifier: "TrailIpad")
        //            tblTrail.rowHeight = UITableView.automaticDimension
        //            tblTrail.estimatedRowHeight = 140
        //            tblTrail.reloadData()
        //        }
        //        else
        //        {
        //            self.tblTrail.register(UINib.init(nibName: "TrailXib", bundle: nil), forCellReuseIdentifier: "TrailCell")
        //        }
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            self.tblTrail.tableFooterView = UIView(frame: .zero)
            self.tblTrail.separatorStyle = .none
            self.tblTrail.estimatedRowHeight = 88.0
            self.tblTrail.rowHeight = UITableView.automaticDimension
            self.tblTrail.register(UINib.init(nibName: "TrailNewCell", bundle: nil), forCellReuseIdentifier: "TrailNewCell")
        }
        else
        {
            self.tblTrail.tableFooterView = UIView(frame: .zero)
            self.tblTrail.separatorStyle = .none
            self.tblTrail.estimatedRowHeight = 88.0
            self.tblTrail.rowHeight = UITableView.automaticDimension
            self.tblTrail.register(UINib.init(nibName: "TrailNewCell", bundle: nil), forCellReuseIdentifier: "TrailNewCell")
        }
        
        let loadedCart = UserDefaults.standard.dictionary(forKey: "dict1")
        print(loadedCart)
        

        if UserDefaults.standard.value(forKey: "CallAPI") != nil
        {
            if UserDefaults.standard.string(forKey: "CallAPI") == "true"
            {   UserDefaults.standard.set("false", forKey: "CallAPI")
                if Reachability.isConnectedToNetwork()
                {
                    self.CallTrailApi()
                }
            }
            else
            {
                   if UserDefaults.standard.value(forKey: "trailD") == nil
                                      {
                                                                         
                                          self.yourArray = (loadedCart!["trail_coordinates"] as? Array<Any>)!
                                          self.title_Color = loadedCart!["title_color"] as? String
                                          self.title_background_Color = loadedCart!["title_background_color"] as? String
                                          self.header_Title_Color = loadedCart!["color"] as? String
                                          self.lblTitleHeader.text = loadedCart!["title"] as? String
                                          self.lblTitleHeader.textColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: self.header_Title_Color)
                                        // self.SetBgImage()
                                        
                                        
                                        
                                        let colorValue_NochView : UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["theme_color"] as! String)
                                                                                     self.viewTheme.backgroundColor = colorValue_NochView
                                                                                     
                                                                                     
                                                                                     if UIDevice.current.userInterfaceIdiom == .pad
                                                                                     {
                                                                                         
                                                                                         let bannerImgStr = loadedCart!["tablet_background_image"] as! String
                                                                                         
                                                                                         if bannerImgStr != nil
                                                                                         {
                                                                                             let urlString = bannerImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                                                                                             self.imgBackGround.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                                                                                                 
                                                                                             })
                                                                                         }
                                                                                     }
                                                                                     else
                                                                                     {
                                                                                         let bannerImgStr = loadedCart!["iphone_background_image"] as! String
                                                                                         if bannerImgStr != nil
                                                                                         {
                                                                                             let urlString = bannerImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                                                                                             self.imgBackGround.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                                                                                                 
                                                                                             })
                                                                                         }
                                                                                     }
                                                                              
                                                             
                                          }
                                              else
                                            {
                                              self.yourArray = (loadedCart!["trail_coordinates"] as? Array<Any>)!
                                              self.title_Color = loadedCart!["title_color"] as? String
                                              self.title_background_Color = loadedCart!["title_background_color"] as? String
                                              self.header_Title_Color = loadedCart!["color"] as? String
                                              self.lblTitleHeader.text = loadedCart!["title"] as? String
                                              self.lblTitleHeader.textColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: self.header_Title_Color)
                                                
                                                
                                                
                                           //   self.SetBgImage()
                                                
                                                let colorValue_NochView : UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["theme_color"] as! String)
                                                       self.viewTheme.backgroundColor = colorValue_NochView
                                                       
                                                       
                                                       if UIDevice.current.userInterfaceIdiom == .pad
                                                       {
                                                           
                                                           let bannerImgStr = loadedCart!["tablet_background_image"] as! String
                                                           
                                                           if bannerImgStr != nil
                                                           {
                                                               let urlString = bannerImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                                                               self.imgBackGround.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                                                                   
                                                               })
                                                           }
                                                       }
                                                       else
                                                       {
                                                           let bannerImgStr = loadedCart!["iphone_background_image"] as! String
                                                           if bannerImgStr != nil
                                                           {
                                                               let urlString = bannerImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                                                               self.imgBackGround.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                                                                   
                                                               })
                                                           }
                                                       }
                                                
                                                
                                                                             
                                              let count1 = self.yourArray.count

                                             }
                             
                                self.tblTrail.reloadData()
                }
        }
        
//        if Reachability.isConnectedToNetwork()
//        {
//            self.CallTrailApi()
//        }
//        else
//        {
//           if UserDefaults.standard.value(forKey: "trailD") == nil
//                              {
//
//                                  self.yourArray = (loadedCart!["trail_coordinates"] as? Array<Any>)!
//                                  self.title_Color = loadedCart!["title_color"] as? String
//                                  self.title_background_Color = loadedCart!["title_background_color"] as? String
//                                  self.header_Title_Color = loadedCart!["color"] as? String
//                                  self.lblTitleHeader.text = loadedCart!["title"] as? String
//                                  self.lblTitleHeader.textColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: self.header_Title_Color)
//                                // self.SetBgImage()
//
//
//
//                                let colorValue_NochView : UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["theme_color"] as! String)
//                                                                             self.viewTheme.backgroundColor = colorValue_NochView
//
//
//                                                                             if UIDevice.current.userInterfaceIdiom == .pad
//                                                                             {
//
//                                                                                 let bannerImgStr = loadedCart!["tablet_background_image"] as! String
//
//                                                                                 if bannerImgStr != nil
//                                                                                 {
//                                                                                     let urlString = bannerImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//                                                                                     self.imgBackGround.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
//
//                                                                                     })
//                                                                                 }
//                                                                             }
//                                                                             else
//                                                                             {
//                                                                                 let bannerImgStr = loadedCart!["iphone_background_image"] as! String
//                                                                                 if bannerImgStr != nil
//                                                                                 {
//                                                                                     let urlString = bannerImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//                                                                                     self.imgBackGround.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
//
//                                                                                     })
//                                                                                 }
//                                                                             }
//
//
//                                  }
//                                      else
//                                    {
//                                      self.yourArray = (loadedCart!["trail_coordinates"] as? Array<Any>)!
//                                      self.title_Color = loadedCart!["title_color"] as? String
//                                      self.title_background_Color = loadedCart!["title_background_color"] as? String
//                                      self.header_Title_Color = loadedCart!["color"] as? String
//                                      self.lblTitleHeader.text = loadedCart!["title"] as? String
//                                      self.lblTitleHeader.textColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: self.header_Title_Color)
//
//
//
//                                   //   self.SetBgImage()
//
//                                        let colorValue_NochView : UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["theme_color"] as! String)
//                                               self.viewTheme.backgroundColor = colorValue_NochView
//
//
//                                               if UIDevice.current.userInterfaceIdiom == .pad
//                                               {
//
//                                                   let bannerImgStr = loadedCart!["tablet_background_image"] as! String
//
//                                                   if bannerImgStr != nil
//                                                   {
//                                                       let urlString = bannerImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//                                                       self.imgBackGround.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
//
//                                                       })
//                                                   }
//                                               }
//                                               else
//                                               {
//                                                   let bannerImgStr = loadedCart!["iphone_background_image"] as! String
//                                                   if bannerImgStr != nil
//                                                   {
//                                                       let urlString = bannerImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//                                                       self.imgBackGround.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
//
//                                                       })
//                                                   }
//                                               }
//
//
//
//                                      let count1 = self.yourArray.count
//
//                                     }
//
//                        self.tblTrail.reloadData()
//        }
       
//    if self.appDel.CheckForInternetConnection()
//    {
//
//                     if UserDefaults.standard.value(forKey: "trailD") == nil
//                 {
//
//                     self.yourArray = (loadedCart!["trail_coordinates"] as? Array<Any>)!
//                     self.title_Color = loadedCart!["title_color"] as? String
//                     self.title_background_Color = loadedCart!["title_background_color"] as? String
//                     self.header_Title_Color = loadedCart!["color"] as? String
//                     self.lblTitleHeader.text = loadedCart!["title"] as? String
//                 self.lblTitleHeader.textColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: self.header_Title_Color)
//                   self.SetBgImage()
//
//                     }
//                         else
//                       {
//                         self.yourArray = (loadedCart!["trail_coordinates"] as? Array<Any>)!
//                         self.title_Color = loadedCart!["title_color"] as? String
//                         self.title_background_Color = loadedCart!["title_background_color"] as? String
//                         self.header_Title_Color = loadedCart!["color"] as? String
//                         self.lblTitleHeader.text = loadedCart!["title"] as? String
//                         self.lblTitleHeader.textColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: self.header_Title_Color)
//                         self.SetBgImage()
//
//                         let count1 = self.yourArray.count
//
//                        }
//
//           self.tblTrail.reloadData()
//
//
//    }
//
//        else
//    {
//         self.CallTrailApi()
//
//    }
//
       
        // Do any additional setup after loading the view.
    }
    
    public class Reachability
    {

        class func isConnectedToNetwork() -> Bool
        {

            var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
            zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
            zeroAddress.sin_family = sa_family_t(AF_INET)

            let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
                $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                    SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
                }
            }

            var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
            if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
                return false
            }

            /* Only Working for WIFI
            let isReachable = flags == .reachable
            let needsConnection = flags == .connectionRequired

            return isReachable && !needsConnection
            */

            // Working for Cellular and WIFI
            let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
            let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
            let ret = (isReachable && !needsConnection)

            return ret

        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated) // No need for semicolon
    
    }
    
    @IBAction func btnBackAction(_ sender: Any)
    {
        let ErrorMsg = "Are you sure you want to quit viewing this Trail?"
        let alert = UIAlertController(title: Constant.alertTitle, message: ErrorMsg, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Yes", style: .default)
        { (action:UIAlertAction) in
            
          //self.POP_VC()
          //   strIsFrom == "TrailCheck"
            
            UserDefaults.standard.set("true", forKey: "TrailCheck")
            
            if UIDevice.current.userInterfaceIdiom == .pad
                       {
                           let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
                           let viewVC = storyboard.instantiateViewController(withIdentifier: "CustomerLandingVC") as! CustomerLandingVC
                           viewVC.strIsFrom = "TrailCheck"
                           self.navigationController?.pushViewController(viewVC, animated: false)
                       }
                      else
                      {
                       let storyboard = UIStoryboard(name: "Main", bundle: nil)
                       let viewVC = storyboard.instantiateViewController(withIdentifier: "CustomerLandingVC") as! CustomerLandingVC
                        viewVC.strIsFrom = "TrailCheck"
                       self.navigationController?.pushViewController(viewVC, animated: false)
            
            }
            
//              if UIDevice.current.userInterfaceIdiom == .pad
//                      {
//                          self.PUSH_STORY_R1(Identifier:"CustomerLandingVC")
//                      }
//                      else
//                      {
//                          self.PUSH_STORY_R1(Identifier:"CustomerLandingVC")
//                      }
            
        }
        alert.addAction(action1)
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true)
   
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if yourArray.count == 0
        {
            return 0
        }
        else
        {
             return self.yourArray.count
        }
       // return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        //******************NEw Code ******************//
        
        var cellIdentifier: String = ""
        
        if  UIDevice.current.userInterfaceIdiom == .pad
        {
            cellIdentifier = "TrailNewCell"
            
        }
        else
        {
            cellIdentifier = "TrailNewCell"
        }
        
        let cell: TrailNewCell = self.tblTrail.dequeueReusableCell(withIdentifier: cellIdentifier) as! TrailNewCell
      
        //Label Title color dynamic
        var dict: Dictionary = yourArray[indexPath.row] as! Dictionary<String,Any>
        
//        let defaults = UserDefaults.standard
//        defaults.set(yourArray, forKey: "NewArray")
//        print(defaults.object(forKey: "NewArray"))
    //  defaults.set(dict, forKey: "SavedDict")
     

        //        defaults.setValue(dict, forKey: "DictValue") //Saved the Dictionary in user default
//        let dictValue = defaults.value(forKey: "DictValue") //Retrieving the value from user default
//        print(dictValue)  // Printing the value
        
      
        
         cell.lblTitle.text = dict["coordinates_title"] as? String
       
        //cell.lblTitle.text = "AppDelegateAppDelegateAppDelegateAppDelegate AppDelegateAppDelegate  AppDelegateAppDelegate"
          cell.lblTitle.textColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: self.title_Color)
         cell.viewTitle.backgroundColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: self.title_background_Color)
                //Label long text color dynamic
                
        
        let stringA = dict["coordinate_description"] as? String
        //cell.lblDynamicTxt.attributedText = stringA?.htmlToAttributedString
        cell.lblDynamicTxt.setHTMLFromString(htmlText: stringA!)
       
        // google button
        
        let pictureUrl = dict["coordinates_image"] as! String
        if pictureUrl != nil
        {
            let urlString = pictureUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            cell.imgTrail.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                
            })
        }
        
        if pictureUrl == ""
        {
            cell.imgTrail.isHidden = true
            cell.imageViewHeight.constant = 0
        }
        else
        {
            cell.imgTrail.isHidden = false
            cell.imageViewHeight.constant = 175
        }
       
        //cell.imageHeight.constant = 0
        
        //cell.btnMap.backgroundColor = .clear
        cell.btnMap.layer.cornerRadius = 5
        cell.btnMap.layer.borderWidth = 1
        cell.btnMap.layer.borderColor = UIColor.clear.cgColor
        
        
        // View Website button
        
        cell.btnVisitWebsite.tag = indexPath.row
        cell.btnVisitWebsite.addTarget(self, action: #selector(self.visitWeb(_:)), for: .touchUpInside)
        
        cell.btnMap.tag = indexPath.row
        cell.btnMap.addTarget(self, action: #selector(self.MapView(_:)), for: .touchUpInside)
      

        
        //************Old code **************//
        
      /*  var cellIdentifier: String = ""
        if  UIDevice.current.userInterfaceIdiom == .pad
        {
            cellIdentifier = "TrailIpad"
            
        }
        else
        {
            cellIdentifier = "TrailCell"
        }
        
        let cell: TrailTableViewCell = self.tblTrail.dequeueReusableCell(withIdentifier: cellIdentifier) as! TrailTableViewCell
         var dict: Dictionary = yourArray[indexPath.row] as! Dictionary<String,Any>
      
        cell.lblName.text = dict["title"] as? String
        cell.selectionStyle = .none
        cell.btnSelected.tag = indexPath.row
        cell.btnSelected.addTarget(self, action: #selector(self.coolFunc(_:)), for: .touchUpInside)*/
     
        return cell
    }
    
    @IBAction func visitWeb(_ sender:UIButton!)
    {
        var dict: Dictionary = yourArray[sender .tag] as! Dictionary<String,Any>
       
        guard let url = URL(string: dict["websitelink"] as! String) else { return }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
        }
        else {
            // Fallback on earlier versions
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            // return 98.0;
            return UITableView.automaticDimension
        }
            
        else
        {
            //return 74.0;
            return UITableView.automaticDimension
        }
        
    }
    @IBAction func coolFunc(_ sender:UIButton!)
    {
        let dict: Dictionary = yourArray[sender.tag] as! Dictionary<String,Any> //
    
        if  UIDevice.current.userInterfaceIdiom == .pad{
            let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
            let DetailVC = storyboard.instantiateViewController(withIdentifier: "TrailviewDetails") as! TrailDetailsViewController
            DetailVC.DetailsData = dict as NSDictionary
            self.navigationController?.pushViewController(DetailVC, animated: false)
        }
        else
        {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let DetailVC = storyboard.instantiateViewController(withIdentifier: "TrailviewDetails") as! TrailDetailsViewController
            DetailVC.DetailsData = dict as NSDictionary
            self.navigationController?.pushViewController(DetailVC, animated: false)
            
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        return
         print(self.Details)
        let dict: Dictionary = yourArray[indexPath.row] as! Dictionary<String,Any> //
        if  UIDevice.current.userInterfaceIdiom == .pad{
            let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
            let DetailVC = storyboard.instantiateViewController(withIdentifier: "TrailviewDetails") as! TrailDetailsViewController
            DetailVC.DetailsData = dict as NSDictionary
            self.navigationController?.pushViewController(DetailVC, animated: false)
        }
        else{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let DetailVC = storyboard.instantiateViewController(withIdentifier: "TrailviewDetails") as! TrailDetailsViewController
            DetailVC.DetailsData = dict as NSDictionary
            self.navigationController?.pushViewController(DetailVC, animated: false)
            
        }
    }
    
    @IBAction func MapView(_ sender:UIButton!)
    {
        if  UIDevice.current.userInterfaceIdiom == .pad
        {
             var dict: Dictionary = yourArray[sender .tag] as! Dictionary<String,Any>
            
            let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
            let mapvc = storyboard.instantiateViewController(withIdentifier: "mapPaid") as! MapVC
            mapvc.DestinationLat = dict["latitude"] as? String
            mapvc.DestinationLong = dict["longitude"] as? String
            self.navigationController?.pushViewController(mapvc, animated: false)
            
        }
        else
        {
            
            var dict: Dictionary = yourArray[sender .tag] as! Dictionary<String,Any>
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mapvc = storyboard.instantiateViewController(withIdentifier: "mapVc") as! MapVC
            mapvc.DestinationLat = dict["latitude"] as? String
            mapvc.DestinationLong = dict["longitude"] as? String
            //mapvc.DestinationLat = "23.0214"
            //mapvc.DestinationLong = "72.5523"
            self.navigationController?.pushViewController(mapvc, animated: false)
        }
    }
    
    func CallTrailApi()
    {
       
        self.appDel.ShowHUD()
        let parameters: Parameters = [
            "barcode" : UserDefaults.standard.string(forKey: "trail")!,
            "token" :self.appDel.strToken!
        ]
        API.sharedInstance.apiRequestWithJsonResponse(apiName: APIName.FetchTrail, requestType:.post, paramValues: parameters, headersValues: nil, SuccessBlock: { (responce) in
            
            self.DataDict = responce as? NSDictionary ?? ["default" : "Defalut"]
            
            if self.DataDict["code"] as? Int == 200
            {
            self.appDel.HideHUD()
               
                
//                if let routingNumber = self.DataDict["detail"] as? NSNull {
//                    print("null data")
//                    print(routingNumber)
//                }
               
                if self.DataDict["detail"] as? String == ""
                {
                    let alert = UIAlertController(title: Constant.alertTitle, message: "This is a different Museum Barcode which is not valid, You will be now logged out from the system. Please scan the ticket again and then retry", preferredStyle: .alert)
                         
                          
                           let action1 = UIAlertAction(title: "Ok", style: .default)
                           { (action:UIAlertAction) in
                               
                               let domain = Bundle.main.bundleIdentifier!
                               UserDefaults.standard.removePersistentDomain(forName: domain)
                               UserDefaults.standard.synchronize()
                                                             
                               if UIDevice.current.userInterfaceIdiom == .pad
                               {
                                   let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
                                   let viewVC = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                   self.navigationController?.pushViewController(viewVC, animated: false)
                               }
                              else
                              {
                               let storyboard = UIStoryboard(name: "Main", bundle: nil)
                               let viewVC = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                               self.navigationController?.pushViewController(viewVC, animated: false)
                           }
                             
                            // self.getLogoutMethod()
                               
                           }
                           alert.addAction(action1)
                           //alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
                           self.present(alert, animated: true)
                }
                else
                {
                    self.Details = self.DataDict["detail"] as? NSDictionary ?? ["default" : "Defalut"]
                            
                            print(self.IdofTrail)
                          
                            let intID2 = self.Details ["id"] as! Int
                            let strID = String(intID2)

                            print("Onlie Gopi data",self.Details)
                            self.defaults.set(self.Details, forKey: "dict1")
                            let loadedCart = UserDefaults.standard.dictionary(forKey: "dict1")
                             print("Offline Gopi data",self.Details)
                          // print(loadedCart)
                            
                                if UserDefaults.standard.value(forKey: "trailD") == nil
                                               {
                                                   
                                                   
                                                   
                                                   
                                                   self.yourArray = (self.Details["trail_coordinates"] as? Array<Any>)!
                                                   self.title_Color = self.Details["title_color"] as? String
                                                   self.title_background_Color = self.Details["title_background_color"] as? String
                                                   self.header_Title_Color = self.Details["color"] as? String
                                                   self.lblTitleHeader.text = self.Details["title"] as? String
                                                   self.lblTitleHeader.textColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: self.header_Title_Color)
                                                   self.SetBgImage()
                                
                                               }
                                               else
                                               {
                                                   self.yourArray = (self.Details["trail_coordinates"] as? Array<Any>)!
                                                   self.title_Color = self.Details["title_color"] as? String
                                                   self.title_background_Color = self.Details["title_background_color"] as? String
                                                   self.header_Title_Color = self.Details["color"] as? String
                                                   self.lblTitleHeader.text = self.Details["title"] as? String
                                                   self.lblTitleHeader.textColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: self.header_Title_Color)
                                                   self.SetBgImage()
                                                
                                                   let count1 = self.yourArray.count

                                              }
                    
                          self.tblTrail.reloadData()
                }
                
                
                
        
            }
            else if self.DataDict["code"] as? Int == 300
            {
                self.appDel.HideHUD()
                let domain = Bundle.main.bundleIdentifier!
                UserDefaults.standard.removePersistentDomain(forName: domain)
                UserDefaults.standard.synchronize()
                
                let ErrorMsg1 = self.DataDict["message"] as! String
                let alert = UIAlertController(title: Constant.alertTitle, message: ErrorMsg1, preferredStyle: .alert)
                //  alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                //  self.present(alert, animated: true)
                
                
                let action12 = UIAlertAction(title: "Ok", style: .default)
                {
                    (action:UIAlertAction) in
                    
                    if UIDevice.current.userInterfaceIdiom == .pad
                    {
                        let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
                        let viewVC = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                        self.navigationController?.pushViewController(viewVC, animated: false)
                    }
                    else
                    {
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let viewVC = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                        self.navigationController?.pushViewController(viewVC, animated: false)
                    }
                    
                    
                }
                alert.addAction(action12)
                self.present(alert, animated: true)
                
            }
                
                
          
                   
            else
            {
                 self.appDel.HideHUD()
                 Alert.showAlert(title: Constant.alertTitle, message: responce["message"] as? String )
            }
        }) { (Error) in
             self.appDel.HideHUD()
             Alert.showAlert(title: Constant.alertTitle, message: "Something went wrong, please try later.")
             print(Error)
            
           
        }
    }
    
   
    
    func SetBgImage()
    {
     
        let colorValue_NochView : UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["theme_color"] as! String)
        self.viewTheme.backgroundColor = colorValue_NochView
        
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            
            let bannerImgStr = self.Details["tablet_background_image"] as! String
            
            if bannerImgStr != nil
            {
                let urlString = bannerImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                self.imgBackGround.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    
                })
            }
        }
        else
        {
            let bannerImgStr = self.Details["iphone_background_image"] as! String
            if bannerImgStr != nil
            {
                let urlString = bannerImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                self.imgBackGround.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    
                })
            }
        }
    }
}
extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
extension UILabel
{
    func setHTMLFromString(htmlText: String)
    {
        
//        DispatchQueue.main.async
//            {
                let modifiedFont = String(format:"<span style=\"font-family: 'HelveticaNeueLTStd-Cn'; color: white ; font-size: \(self.font!.pointSize)\">%@</span>", htmlText)
                
                //process collection values
                let attrStr = try! NSAttributedString(
                    data: modifiedFont.data(using: .unicode, allowLossyConversion: true)!,
                    options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue],
                    documentAttributes: nil)
                self.attributedText = attrStr
                
        }
       
    //}
   
}
