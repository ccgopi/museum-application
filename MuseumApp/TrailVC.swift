//
//  TrailVC.swift
//  MuseumApp
//
//  Created by CCT Macmini on 03/01/19.
//  Copyright © 2019 CCT Macmini. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire

class TrailVC: UIViewController,UITableViewDelegate,UITableViewDataSource
{

    @IBOutlet weak var blurViewTopCons: NSLayoutConstraint!
    @IBOutlet weak var btnNextPage: UIButton!
    @IBOutlet weak var btnPrevPage: UIButton!
    @IBOutlet weak var btnHomePage: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var DevPrevImg: UIImageView!
    @IBOutlet weak var DevNextImg: UIImageView!
    @IBOutlet weak var backImg: UIImageView!
    @IBOutlet weak var NochView: UIView!
    @IBOutlet var tblVideo: UITableView!
    @IBOutlet weak var lblTitleName: UILabel!
    @IBOutlet weak var lblTitleTrail: UILabel!
    @IBOutlet weak var TrailImg: UIImageView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var btnCameraIcon: UIButton!
    @IBOutlet weak var btnMapIcon: UIButton!
    @IBOutlet weak var webview: UIWebView!
    var aryImageDetail = [Dictionary<String, Any>]()
    var appDel: AppDelegate!
    var trailUsDict: Dictionary<String,Any>!
    @IBOutlet weak var webviewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var tblTrail: UITableView!
    
    @IBAction func btnNextPage(_ sender: Any)
    {
        
       
    }
    
    @IBAction func btnPrevPage(_ sender: Any)
    {
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
            let kids = storyboard.instantiateViewController(withIdentifier: "KidsVC") as! KidsVC
            self.navigationController?.pushViewController(kids, animated: false)
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let kids = storyboard.instantiateViewController(withIdentifier: "KidsVC") as! KidsVC
            self.navigationController?.pushViewController(kids, animated: false)
            
        }
      
    }
    
    @IBAction func btnCameraIcon(_ sender: Any)
    {
        
        
    }
    @IBAction func btnMapIcon(_ sender: Any)
    {
        
        
    }
    
    @IBAction func btnHomePage(_ sender: Any)
    {
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
            let FactSheet = storyboard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
            self.navigationController?.pushViewController(FactSheet, animated: false)
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let FactSheet = storyboard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
            self.navigationController?.pushViewController(FactSheet, animated: false)
        }
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
      
       /* if UIDevice.current.userInterfaceIdiom == .pad
        {
            tblTrail.rowHeight = UITableView.automaticDimension
            tblTrail.estimatedRowHeight = 140
            tblTrail.reloadData()
            
        }*/
       
        self.aryImageDetail.append(["name": "Museam1",
                                    "Aderss": "Panchvati circle ahemdabad",
                                    "Lat":"22.3039",
                                    "Long":"70.8022",
                                    "img" : ""])
        self.aryImageDetail.append(["name": "Museam2",
                                    "Aderss": "Shivranjani shivranjni shivranjni shivranjni shivranjni",
                                    "Lat":"22.3072",
                                    "Long":"73.1812",
                                    "img" : ""])
        self.aryImageDetail.append(["name": "Museam3",
                                    "Aderss": "Shivranjani shivranjni shivranjni shivranjni shivranjni ahemdabad ahemdabad Shivranjani shivranjni shivranjni shivranjni shivranjni ahemdabad ahemdabad Shivranjani shivranjni shivranjni shivranjni shivranjni ahemdabad ahemdabad",
                                    "Lat":"21.1702",
                                    "Long":"72.8311",
                                    "img" : ""])
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated) // No need for semicolon
        
        if(appDel == nil)
        {
            appDel = UIApplication.shared.delegate as? AppDelegate
        }
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
          //gopi
        trailUsDict = appDel.itemDetails["trail"] as? Dictionary<String, Any>
        self.BottomBarButtonHideShowMethod()
        
       
//        if UIDevice.current.userInterfaceIdiom == .pad
//        {
//             self.tblTrail.register(UINib.init(nibName: "trailPad", bundle: nil), forCellReuseIdentifier: "TrailIpad")
//        }
//        else
//        {
//             self.tblTrail.register(UINib.init(nibName: "TrailCell", bundle: nil), forCellReuseIdentifier: "TrailCell")
//        }
//     
        
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            self.tblTrail.tableFooterView = UIView(frame: .zero)
            self.tblTrail.separatorStyle = .none
            self.tblTrail.estimatedRowHeight = 88.0
            self.tblTrail.rowHeight = UITableView.automaticDimension
            self.tblTrail.register(UINib.init(nibName: "TrailNewCell", bundle: nil), forCellReuseIdentifier: "TrailNewCell")
        }
        else
        {
            self.tblTrail.tableFooterView = UIView(frame: .zero)
            self.tblTrail.separatorStyle = .none
            self.tblTrail.estimatedRowHeight = 88.0
            self.tblTrail.rowHeight = UITableView.automaticDimension
            self.tblTrail.register(UINib.init(nibName: "TrailNewCell", bundle: nil), forCellReuseIdentifier: "TrailNewCell")
        }
     // gopi
        
        self.ConfigInformation()
       
    }
    
    func SetConsTraintofview()
    {
          if UIDevice.current.userInterfaceIdiom == .pad
        {
            
        }
        else
        
        {
             if Device.IS_IPHONE_6
             
            {
                 webviewHeight.constant = 285
            }
             
             else if Device.IS_IPHONE_5
            {
                 webviewHeight.constant = 285
            }
            
             else if Device.IS_IPHONE_6P
          
             {
               webviewHeight.constant = 320
             }
           
        else
        {
             webviewHeight.constant = 330
       }
           
        }
       
    }
    
    func SetWebviewTxt()
    {
        //webview Load
        
        let strText = trailUsDict["description"]  as? String ?? ""
        strText.replacingOccurrences(of: "\n", with: "<br/>")
        
        var htmlFont : String!
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            htmlFont = "<font face='HELVETICANEUELTSTD-CN' size='40' align= 'center' color= 'white' >\(strText)"
        }
        else
        {
            htmlFont = "<font face='HELVETICANEUELTSTD-CN' size='5' color= 'white' >\(strText)"
        }
        webview.loadHTMLString(htmlFont, baseURL: nil)
        webview.isOpaque = false;
        webview.backgroundColor = UIColor.clear
    }
    
    func SetTHtmlText()
    {
        //webview load
        
        var strText = trailUsDict["description"] as! String
        strText = strText.replacingOccurrences(of: "\r\n\r\n", with: "<br/>")
        let htmlData = NSString(string: strText).data(using: String.Encoding.unicode.rawValue)
        let paragraphStyle = NSMutableParagraphStyle.init()
        paragraphStyle.alignment = .center
        let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]
        let attributedString = try! NSMutableAttributedString(data: htmlData!, options: options, documentAttributes: nil)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: NSRange.init(location: 0, length: attributedString.string.count))
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSRange.init(location: 0, length: attributedString.string.count))
        textView.isUserInteractionEnabled = false
        
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            
            if Device.IS_IPAD_PRO
            {
                attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name:"DINNeuzeitGrotesk-BoldCond",size:48)!, range: NSRange.init(location: 0, length: attributedString.string.count))
                //DINNeuzeitGrotesk-Light 20.0
                textView.attributedText = attributedString
                textView.textContainerInset =  UIEdgeInsets(top:160, left: 30, bottom:5, right: 30)
            }
                
            else
            {
                attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name:"DINNeuzeitGrotesk-BoldCond",size:31)!, range: NSRange.init(location: 0, length: attributedString.string.count))
                //DINNeuzeitGrotesk-Light 20.0
                textView.attributedText = attributedString
                textView.textContainerInset =  UIEdgeInsets(top:15, left: 30, bottom:5, right: 30)
            }
        }
            
        else
        {
            if Device.IS_IPHONE_6
            {
                attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name:"DINNeuzeitGrotesk-BoldCond",size:25)!, range: NSRange.init(location: 0, length: attributedString.string.count))
                //DINNeuzeitGrotesk-Light 20.0
                textView.attributedText = attributedString
                textView.textContainerInset =  UIEdgeInsets(top:10, left: 0, bottom:0, right: 0)
            }
                
            else if Device.IS_IPHONE_5
            {
                attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name:"DINNeuzeitGrotesk-BoldCond",size:22)!, range: NSRange.init(location: 0, length: attributedString.string.count))
                //DINNeuzeitGrotesk-Light 20.0
                textView.attributedText = attributedString
                textView.textContainerInset =  UIEdgeInsets(top:30, left: 30, bottom:5, right: 30)
                
            }
            else if Device.IS_IPHONE_6P
            {
                attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name:"DINNeuzeitGrotesk-BoldCond",size:29)!, range: NSRange.init(location: 0, length: attributedString.string.count))
                
                textView.attributedText = attributedString
                textView.textContainerInset =  UIEdgeInsets(top:40, left: 30, bottom:5, right: 30)
            }
            else
            {
                attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name:"DINNeuzeitGrotesk-BoldCond",size:30)!, range: NSRange.init(location: 0, length: attributedString.string.count))
                //DINNeuzeitGrotesk-Light 20.0
                textView.attributedText = attributedString
                textView.textContainerInset =  UIEdgeInsets(top:60, left: 30, bottom:5, right: 30)
            }
           
        }

    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        /*if aryImageDetail == nil
        {
            return 0;
        }
       return aryImageDetail.count */
        
        return 10
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cellIdentifier: String = ""
        
        if  UIDevice.current.userInterfaceIdiom == .pad
        {
            cellIdentifier = "TrailNewCell"
            
        }
        else
        {
            cellIdentifier = "TrailNewCell"
        }
        
        let cell: TrailNewCell = self.tblTrail.dequeueReusableCell(withIdentifier: cellIdentifier) as! TrailNewCell
        
        
         //*************NEW CHANGE************
        
        //Label Title color dynamic
        
      //  cell.lblTitle.text  = appDel.itemDetails["title"] as? String
      //  cell.lblTitle.textColor = AppDelegate.sharedInstance().hexStringToUIColor(hex : appDel.itemDetails["title_colour"] as! String)
        cell.lblTitle.text = "Gopi Trivedi Gopi Trivedi Gopi Trivedi"
       
        //Label long text color dynamic
      
        //cell.lblDynamicTxt.text  = appDel.itemDetails["title"] as? String
        //cell.lblDynamicTxt.textColor = AppDelegate.sharedInstance().hexStringToUIColor(hex : appDel.itemDetails["title_colour"] as! String)
        cell.lblDynamicTxt.text = "bgkbghkbhkhfhkgfmnhkfgnhjkfghnfjkhnfgjkhnfjhntiutgjthnfjhbnfjhnfjhnfjbghnjfhnjhyntjbngfjbgkbghkbhkhfhkgfmnhkfgnhjkfghnfjkhnfgjkhnfjhntiutgjthnfjhbnfjhnfjhnfjbghnjfhnjhyntjbngfjbgkbghkbhkhfhkgfmnhkfgnhjkfghnfjkhnfgjkhnfjhntiutgjthnfjhbnfjhnfjhnfjbghnjfhnjhyntjbngfjbgkbghkbhkhfhkgfmnhkfgnhjkfghnfjkhnfgjkhnfjhntiutgjthnfjhbnfjhnfjhnfjbghnjfhnjhyntjbngfjbgk"
        
        
        // google button
        
        //cell.btnMap.backgroundColor = .clear
        cell.btnMap.layer.cornerRadius = 5
        cell.btnMap.layer.borderWidth = 1
        cell.btnMap.layer.borderColor = UIColor.clear.cgColor
        
       // cell.btnMap.tag = indexPath.row
        //cell.btnMap.addTarget(self, action: #selector(self.MapView(_:)), for: .touchUpInside)
        
        //image set
        
        
     /*  let Gallery1 = dict["file_name"] as! String
        cell.imgTrail.sd_setShowActivityIndicatorView(true)
        cell.imgTrail.sd_setIndicatorStyle(UIActivityIndicatorView.Style.white)
       
        if Gallery1 != nil
       {
            let urlString = Gallery1.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        cell.images.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                
            })
        }*/
        
        // View Website button
        
         cell.btnVisitWebsite.tag = indexPath.row
         cell.btnVisitWebsite.addTarget(self, action: #selector(self.visitWeb(_:)), for: .touchUpInside)
       
       
        //*************BEFORE*************
      /*  let DisplayData = aryImageDetail[indexPath.row]
        cell.lblName.text = DisplayData["name"] as? String
        cell.lblAdress.text = DisplayData["Aderss"] as? String
        cell.btnViewOnMap.tag = indexPath.row
        cell.btnViewOnMap.addTarget(self, action: #selector(self.MapView(_:)), for: .touchUpInside)*/
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            // return 98.0;
            return UITableView.automaticDimension
        }
            
        else
        {
            //return 74.0;
            return UITableView.automaticDimension
        }
        
    }
   
//    @IBAction func MapView(_ sender:UIButton!)
//    {
//        if  UIDevice.current.userInterfaceIdiom == .pad
//        {
//            let passData = aryImageDetail[sender.tag]
//            let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
//            let mapvc = storyboard.instantiateViewController(withIdentifier: "mapPaid") as! MapVC
//            mapvc.DestinationLat = passData["Lat"] as? String
//            mapvc.DestinationLong = passData["Long"] as? String
//            self.navigationController?.pushViewController(mapvc, animated: false)
//
//        }
//        else
//        {
//        let passData = aryImageDetail[sender.tag]
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let mapvc = storyboard.instantiateViewController(withIdentifier: "mapVc") as! MapVC
//        mapvc.DestinationLat = passData["Lat"] as? String
//        mapvc.DestinationLong = passData["Long"] as? String
//        self.navigationController?.pushViewController(mapvc, animated: false)
//        }
//    }
    
     @IBAction func visitWeb(_ sender:UIButton!)
     {
        
        if  UIDevice.current.userInterfaceIdiom == .pad
        {
            print("gopi...")
        }
        else
        {
            print("gopi..")
        }
     }
   
    
    func BottomBarButtonHideShowMethod()
    {
        btnNextPage.isHidden = true
        btnNextPage.isEnabled = true
        btnPrevPage.isHidden = false
        btnPrevPage.isEnabled = true
        btnHomePage.isHidden = false
        btnHomePage.isEnabled = true
        DevPrevImg.isHidden = false
        DevNextImg.isHidden = false
    }
    
    func ConfigInformation()
    {
        
        //Title Heading Text set
        //self.SetTHtmlText()
       // self.SetWebviewTxt()
        
        lblTitleName.text  = appDel.itemDetails["title"] as? String
        lblTitleName.textColor = AppDelegate.sharedInstance().hexStringToUIColor(hex : appDel.itemDetails["title_colour"] as! String)
        
        //Title Video Text set
        lblTitleTrail.text = trailUsDict["title"] as? String
        lblTitleTrail.textColor = AppDelegate.sharedInstance().hexStringToUIColor(hex : trailUsDict["color"] as! String)
     
        let colorValue_BottomView : UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["theme_color"] as! String)
        self.bottomView.backgroundColor = colorValue_BottomView
        
        //Trail Logo
       
        var trailLogo : String!
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            trailLogo = trailUsDict["ipad_icon_image"] as? String
        }
        else
        {
            trailLogo = trailUsDict["iphone_icon_image"] as? String
        }
        
        if trailLogo != nil
        {
            let urlString = trailLogo.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            TrailImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                
            })
        }
        
        
        //NochView Theme Color set
        let colorValue_NochView : UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["theme_color"] as! String)
        self.NochView.backgroundColor = colorValue_NochView
        
        
        
    
        //Map icon set
        
        
        var cameraImg1 : String!
        var mapImg2 : String!
        
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            cameraImg1 = trailUsDict["camera_icon"] as? String
            mapImg2 = trailUsDict["map_icon"] as? String
            
            
        }
        else
        {
            cameraImg1 = trailUsDict["camera_icon"] as? String
            mapImg2 = trailUsDict["map_icon"] as? String
        }
        
       //camera icon set
        if cameraImg1 != nil
        {
            let urlString = cameraImg1.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            self.btnCameraIcon.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
                , completed: { (image, error, cacheType, imageURL) in
                    self.btnCameraIcon.backgroundImage(for: UIControl.State.normal)
            })
        }
        
         //map icon set
        
        if mapImg2 != nil
        {
            let urlString = mapImg2.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            self.btnMapIcon.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
                , completed: { (image, error, cacheType, imageURL) in
                    self.btnMapIcon.backgroundImage(for: UIControl.State.normal)
            })
        }
        
       
        //Devider Next/Prev image set
        
        let colorValue_devColor: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["tab_color"] as! String)
        DevNextImg.backgroundColor = colorValue_devColor
        DevPrevImg.backgroundColor = colorValue_devColor
        
        var tabBarImg1 : String!
        var tabBarImg2 : String!
        var tabBarImg3 : String!
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            tabBarImg1 = self.appDel.configDict["ipad_bottom_icon_1"] as? String
            tabBarImg2 = self.appDel.configDict["ipad_bottom_icon_2"] as? String
            tabBarImg3 = self.appDel.configDict["ipad_bottom_icon_3"] as? String
            
        }
        else
        {
            tabBarImg1 = self.appDel.configDict["ipad_bottom_icon_1"] as? String
            tabBarImg2 = self.appDel.configDict["ipad_bottom_icon_2"] as? String
            tabBarImg3 = self.appDel.configDict["ipad_bottom_icon_3"] as? String
            
        }
        
        //btn Prev page
        if tabBarImg1 != nil
        {
            let urlString = tabBarImg1.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            self.btnPrevPage.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
                , completed: { (image, error, cacheType, imageURL) in
                    self.btnPrevPage.backgroundImage(for: UIControl.State.normal)
            })
        }
        
        //btn Next page
        if tabBarImg3 != nil
        {
            let urlString = tabBarImg3.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            self.btnNextPage.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
                , completed: { (image, error, cacheType, imageURL) in
                    self.btnNextPage.backgroundImage(for: UIControl.State.normal)
            })
        }
        
        //btn Home page
        if tabBarImg2 != nil
        {
            let urlString = tabBarImg2.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            self.btnHomePage.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
                , completed: { (image, error, cacheType, imageURL) in
                    self.btnHomePage.backgroundImage(for: UIControl.State.normal)
            })
            
        }
        
        //BackGround ImgSet
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            let bannerImgStr = appDel.itemDetails["ipad_background_image"] as! String
            
            if bannerImgStr != nil
            {
                let urlString = bannerImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                self.backImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    
                })
            }
        }
        else
        {
            let bannerImgStr = appDel.itemDetails["iphone_background_image"] as! String
            if bannerImgStr != nil
            {
                let urlString = bannerImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                self.backImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    
                })
            }
        }
    }
    
}
