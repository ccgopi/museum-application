//
//  AppDelegate.swift
//  MuseumApp
//
//  Created by CCT Macmini on 31/12/18.
//  Copyright © 2018 CCT Macmini. All rights reserved.
//

import UIKit
import GoogleMaps
@UIApplicationMain
 class AppDelegate: UIResponder, UIApplicationDelegate
{
  
    var window: UIWindow?
    var configDict: Dictionary<String,Any>!
    var itemDetails: Dictionary<String,Any>!
    var strToken: String!
    var strTrailSave: String!
    var orientationLock = UIInterfaceOrientationMask.portrait
   
//hg
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
    {
        
        GMSServices.provideAPIKey("AIzaSyCDHTMM9YVHtm5yYR0-aAuzH9QxG66w-1w")
        // Override point for customization after application launch.
         var storyboard1 : UIStoryboard

        if UIDevice.current.userInterfaceIdiom == .pad
        {
            storyboard1 = UIStoryboard(name: "MainiPad", bundle: nil)
           
        }
        else
        {
           storyboard1 = UIStoryboard(name: "Main", bundle: nil)
        }
        
       // let CustomerLanding = storyboard1.instantiateViewController(withIdentifier: "CustomerLandingVC") as! CustomerLandingVC
       // self.window?.rootViewController = CustomerLanding

        if UserDefaults.standard.value(forKey: "isLogin") == nil
        {
           
            let ViewControllerVC = storyboard1.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            //self.window?.rootViewController = ViewControllerVC
            let nvc: UINavigationController = UINavigationController(rootViewController: ViewControllerVC)
            self.window?.rootViewController = nvc
            self.window?.makeKeyAndVisible()
        }
        else
        {
            strToken = UserDefaults.standard.value(forKey: "Token") as? String
            configDict =  UserDefaults.standard.value(forKey: "configDictValue") as! Dictionary<String, Any>
            let CustomerLanding = storyboard1.instantiateViewController(withIdentifier: "CustomerLandingVC") as! CustomerLandingVC
           // self.window?.rootViewController = CustomerLanding
            let nvc: UINavigationController = UINavigationController(rootViewController: CustomerLanding)
            self.window?.rootViewController = nvc
             self.window?.makeKeyAndVisible()
         }
        return true
    }
    
    
    func applicationWillResignActive(_ application: UIApplication)
    {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication)
    {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication)
    {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication)
    {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication)
    {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
   
    
    func CheckForInternetConnection() -> Bool
    {
        let reachability: Reachability = Reachability()!
        let netStatus = reachability.currentReachabilityStatus
        if netStatus == Reachability.NetworkStatus.notReachable
        {
            return false
        }

        return true
    }
    
    class func sharedInstance() -> AppDelegate
    {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func hexStringToUIColor (hex:String) -> UIColor
    {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6)
        {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    func ShowHUD()  {
        
        let topViewController = UIApplication.shared.keyWindow!.rootViewController!.topMostViewController()
        
        let loadingNotification = MBProgressHUD.showAdded(to: (topViewController.view)!, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Loading"
        
    }
    func HideHUD()
    {
        let topViewController = UIApplication.shared.keyWindow!.rootViewController!.topMostViewController()
       // MBProgressHUD.hideAllHUDs(for: (topViewController.view), animated: true)
        MBProgressHUD.hide(for: (topViewController.view), animated: true)
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask
    {
        return self.orientationLock
    }
    
    
}


struct AppUtility
{
    
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask)
    {
        
        if let delegate = UIApplication.shared.delegate as? AppDelegate
        {
            delegate.orientationLock = orientation
        }
    }
    
    /// OPTIONAL Added method to adjust lock and rotate to the desired orientation
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation)
    {
        
        self.lockOrientation(orientation)
        
        UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
        UINavigationController.attemptRotationToDeviceOrientation()
    }
    
}
