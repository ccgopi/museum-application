//
//  MuseumIntroductionVC.swift
//  MuseumApp
//
//  Created by CCT Macmini on 31/12/18.
//  Copyright © 2018 CCT Macmini. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

var appDel: AppDelegate!




class MuseumIntroductionVC: UIViewController
{

    @IBOutlet weak var backImg: UIImageView!
    @IBOutlet weak var logoImg: UIImageView!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var btnNextPage: UIButton!
    @IBOutlet weak var btnPrevPage: UIButton!
    @IBOutlet weak var btnHomePage: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var NochView: UIView!
    @IBOutlet weak var webview: UIWebView!
    @IBOutlet weak var DevPrevImg: UIImageView!
    @IBOutlet weak var DevNextImg: UIImageView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var ArrowbtnNextPage: UIButton!
    @IBOutlet weak var ArrowbtnPrevPage: UIButton!
    
    
    @IBAction func btnNextPage(_ sender: Any)
    {
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
            let Home = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            self.navigationController?.pushViewController(Home, animated: false)
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let Home = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            self.navigationController?.pushViewController(Home, animated: false)
        }
        
       
    }

    @IBAction func btnPrevPage(_ sender: Any)
    {
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
            let CustomerLanding = storyboard.instantiateViewController(withIdentifier: "CustomerLandingVC") as! CustomerLandingVC
            self.navigationController?.pushViewController(CustomerLanding, animated: false)
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let CustomerLanding = storyboard.instantiateViewController(withIdentifier: "CustomerLandingVC") as! CustomerLandingVC
            self.navigationController?.pushViewController(CustomerLanding, animated: false)
        }
        
        
      //  self.navigationController?.popViewController(animated: true)
    }
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
     }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated) // No need for semicolon
        
        if(appDel == nil)
        {
            appDel = UIApplication.shared.delegate as? AppDelegate
        }
         self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.BottomBarButtonHideShowMethod()
        self.ConfigInformation()
    }
    
    
    func BottomBarButtonHideShowMethod()
    {
        btnNextPage.isHidden = false
        btnNextPage.isEnabled = true
        btnPrevPage.isHidden = false
        btnPrevPage.isEnabled = true
        btnHomePage.isHidden = true
        btnHomePage.isEnabled = false
        DevPrevImg.isHidden = false
        DevNextImg.isHidden = false
      
    }
  
    
    func ConfigInformation()
    {
        
        //webview load
        
        var strText = appDel.configDict["welcome_text"] as! String
       // webview.loadHTMLString(str, baseURL: nil)
        strText = strText.replacingOccurrences(of: "\r\n\r\n", with: "<br/>")
        let htmlData = NSString(string: strText).data(using: String.Encoding.unicode.rawValue)
        let paragraphStyle = NSMutableParagraphStyle.init()
        paragraphStyle.alignment = .center
        let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]
        let attributedString = try! NSMutableAttributedString(data: htmlData!, options: options, documentAttributes: nil)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: NSRange.init(location: 0, length: attributedString.string.count))
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSRange.init(location: 0, length: attributedString.string.count))
        textView.isUserInteractionEnabled = false
        
        
        //NEXT BUTTON
        
        let colorValue_button: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["tab_color"] as! String)
        self.btnNextPage.setTitleColor(colorValue_button, for: .normal)
        self.btnNextPage.setTitle("Next", for: UIControl.State.normal)
        self.btnPrevPage.setTitleColor(colorValue_button, for: .normal)
        self.btnPrevPage.setTitle("Previous", for: UIControl.State.normal)


     if UIDevice.current.userInterfaceIdiom == .pad
     {

         if Device.IS_IPAD_PRO
         {
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name:"DINNeuzeitGrotesk-BoldCond",size:48)!, range: NSRange.init(location: 0, length: attributedString.string.count))
            //DINNeuzeitGrotesk-Light 20.0
            textView.attributedText = attributedString
            textView.textContainerInset =  UIEdgeInsets(top:160, left: 30, bottom:5, right: 30)
         }

         else
        {
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name:"DINNeuzeitGrotesk-BoldCond",size:42)!, range: NSRange.init(location: 0, length: attributedString.string.count))
            //DINNeuzeitGrotesk-Light 20.0
            textView.attributedText = attributedString
            textView.textContainerInset =  UIEdgeInsets(top:90, left: 30, bottom:5, right: 30)
        }
    }

     else
     {
        if Device.IS_IPHONE_6
        {
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name:"DINNeuzeitGrotesk-BoldCond",size:29)!, range: NSRange.init(location: 0, length: attributedString.string.count))
            //DINNeuzeitGrotesk-Light 20.0
            textView.attributedText = attributedString
            textView.textContainerInset =  UIEdgeInsets(top:30, left: 30, bottom:5, right: 30)

        }
         
        else if Device.IS_IPHONE_5
        {
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name:"DINNeuzeitGrotesk-BoldCond",size:22)!, range: NSRange.init(location: 0, length: attributedString.string.count))
            //DINNeuzeitGrotesk-Light 20.0
            textView.attributedText = attributedString
            textView.textContainerInset =  UIEdgeInsets(top:30, left: 30, bottom:5, right: 30)
            
        }
        else if Device.IS_IPHONE_6P
        {
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name:"DINNeuzeitGrotesk-BoldCond",size:29)!, range: NSRange.init(location: 0, length: attributedString.string.count))

            textView.attributedText = attributedString
            textView.textContainerInset =  UIEdgeInsets(top:40, left: 30, bottom:5, right: 30)
        }
        else
        {
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name:"DINNeuzeitGrotesk-BoldCond",size:30)!, range: NSRange.init(location: 0, length: attributedString.string.count))
            //DINNeuzeitGrotesk-Light 20.0
            textView.attributedText = attributedString
            textView.textContainerInset =  UIEdgeInsets(top:60, left: 30, bottom:5, right: 30)
        }



    }


        //Devider Next/Prev image set
        
        let colorValue_devColor: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["tab_color"] as! String)
        DevNextImg.backgroundColor = colorValue_devColor
        DevPrevImg.backgroundColor = colorValue_devColor
        
        
        
        //bottomView Theme Color set
        
        let colorValue_BottomView : UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["theme_color"] as! String)
        self.bottomView.backgroundColor = colorValue_BottomView
        
        
        //NochView Theme Color set
        
        let colorValue_NochView : UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["theme_color"] as! String)
        self.NochView.backgroundColor = colorValue_NochView
        
        //BackGround ImgSet
        
         if UIDevice.current.userInterfaceIdiom == .pad
         {
            let bannerImgStr = appDel.configDict["ipad_screen2_background_image"] as! String
            
            if bannerImgStr != nil
            {
                let urlString = bannerImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                self.backImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    
                })
                
                
                
            }
        
         }
        
        else
         {
            let bannerImgStr = appDel.configDict["iphone_screen2_background_image"] as! String
            
            if bannerImgStr != nil
            {
                let urlString = bannerImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                self.backImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    
                })
                
                
                
            }
        }
        
       
        
        //bottomView Theme Color set
        
//        let colorValue_BottomView : UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["theme_color"] as! String)
//        self.bottomView.backgroundColor = colorValue_BottomView
        
        //Logo ImgSet
        
        let logoImgStr = appDel.configDict["ipad_bottom_icon_3"] as! String
        
        if logoImgStr != nil
        {
            let urlString = logoImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
         //   self.logoImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
         //   })
        }
        
        //btn Next page
        
         if UIDevice.current.userInterfaceIdiom == .pad
         {
            let tabBarImg3 = appDel.configDict["ipad_bottom_icon_3"] as! String
            if tabBarImg3 != nil
            {
                
//                let urlString = tabBarImg3.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//
//                self.btnNextPage.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
//                    , completed: { (image, error, cacheType, imageURL) in
//                        self.btnNextPage.backgroundImage(for: UIControl.State.normal)
//                })
                
            }
      
         }
        
        else
        
         {
            let tabBarImg3 = appDel.configDict["iphone_bottom_icon_3"] as! String
            if tabBarImg3 != nil
            {
                
//                let urlString = tabBarImg3.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//
//                self.btnNextPage.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
//                    , completed: { (image, error, cacheType, imageURL) in
//                        self.btnNextPage.backgroundImage(for: UIControl.State.normal)
//                })
                
            }
        }
        
        
       
       
         //btn Prev page
        
         if UIDevice.current.userInterfaceIdiom == .pad
         {
            let tabBarImg1 = appDel.configDict["ipad_bottom_icon_1"] as! String
            
            if tabBarImg1 != nil
            {
                
//                let urlString = tabBarImg1.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//
//                self.btnPrevPage.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
//                    , completed: { (image, error, cacheType, imageURL) in
//                        self.btnPrevPage.backgroundImage(for: UIControl.State.normal)
//                })
                
            }
         }
        else
         {
            let tabBarImg1 = appDel.configDict["iphone_bottom_icon_1"] as! String
            
            if tabBarImg1 != nil
            {
                
//                let urlString = tabBarImg1.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//                
//                self.btnPrevPage.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
//                    , completed: { (image, error, cacheType, imageURL) in
//                        self.btnPrevPage.backgroundImage(for: UIControl.State.normal)
//                })
                
            }
        }
    }
}
   

