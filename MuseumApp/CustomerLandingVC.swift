//
//  CustomerLandingVC.swift
//  MuseumApp
//
//  Created by CCT Macmini on 31/12/18.
//  Copyright © 2018 CCT Macmini. All rights reserved.
//

import UIKit
import SDWebImage

class CustomerLandingVC: UIViewController
{

   var ConfDict: NSMutableDictionary!
   @IBOutlet weak var BackImg: UIImageView!
   @IBOutlet weak var LogoImg: UIImageView!
   @IBOutlet weak var lblWelCome: UILabel!
   @IBOutlet weak var lblSubtitle1: UILabel!
   @IBOutlet weak var lblSubtitle2: UILabel!
   @IBOutlet weak var bottomView: UIView!
   @IBOutlet weak var NochView: UIView!
   @IBOutlet weak var btnNextPage: UIButton!
   @IBOutlet weak var btnPrevPage: UIButton!
   @IBOutlet weak var btnHomePage: UIButton!
   @IBOutlet weak var DevPrevImg: UIImageView!
   @IBOutlet weak var DevNextImg: UIImageView!
   @IBOutlet weak var ArrowbtnNextPage: UIButton!
   @IBOutlet weak var ArrowbtnPrevPage: UIButton!
    
    var strIsFrom: String!
   
   var appDel: AppDelegate!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated) // No need for semicolon
        
        if(appDel == nil)
        {
           appDel = UIApplication.shared.delegate as? AppDelegate
        }
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.BottomBarButtonHideShowMethod()
        self.ConfigInformation()
    }
    
    func BottomBarButtonHideShowMethod()
    {
       // TrailCheck
     
        
     if UserDefaults.standard.string(forKey: "TrailCheck") == "true"
     {
        btnNextPage.isHidden = false
        btnNextPage.isEnabled = true
        btnPrevPage.isHidden = false
        btnPrevPage.isEnabled = true
        btnHomePage.isHidden = true
        btnHomePage.isEnabled = false
        DevPrevImg.isHidden = false
        DevNextImg.isHidden = false
     }
    else
     {
        btnNextPage.isHidden = false
        btnNextPage.isEnabled = true
        btnPrevPage.isHidden = true
        btnPrevPage.isEnabled = false
        btnHomePage.isHidden = true
        btnHomePage.isEnabled = false
        DevPrevImg.isHidden = true
        DevNextImg.isHidden = true
     }
        
    
     
    }
    
    @IBAction func btnNextPage(_ sender: Any)
    {
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
            let MuseumIntroduction = storyboard.instantiateViewController(withIdentifier: "MuseumIntroductionVC") as! MuseumIntroductionVC
            self.navigationController?.pushViewController(MuseumIntroduction, animated: false)
        }
        else
        {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let MuseumIntroduction = storyboard.instantiateViewController(withIdentifier: "MuseumIntroductionVC") as! MuseumIntroductionVC
        self.navigationController?.pushViewController(MuseumIntroduction, animated: false)
        }
    }
   
    @IBAction func btnPrevPage(_ sender: Any)
    {
        
          if UIDevice.current.userInterfaceIdiom == .pad
            {
                self.PUSH_STORY_R1(Identifier:"trailPadVc")
            }
            else
            {
                self.PUSH_STORY_R1(Identifier:"TrailviewController")
            }
    }
    
    func ConfigInformation()
    {
        let colorValue_lblWelCome: UIColor =
            AppDelegate.sharedInstance().hexStringToUIColor(hex : appDel.configDict["app_subtitle_colour"] as! String)
        //self.lblWelCome.text = appDel.configDict["welcome_text"] as? String
        self.lblWelCome.text = "WELCOME TO " as? String
        self.lblWelCome.textColor = colorValue_lblWelCome
       // self.lblWelCome.font = UIFont(name:"DINNeuzeitGrotesk-BoldCond.ttf",size:70)
        
        //subTitle1 text
        let colorValue_lblSubtitle1: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["app_name_colour"] as! String)
        self.lblSubtitle1.text = self.appDel.configDict["app_name"] as? String
        self.lblSubtitle1.textColor = colorValue_lblSubtitle1
        
        //subTitle2 text
        let colorValue_lblSubtitle2: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["app_subtitle_colour"] as! String)
        self.lblSubtitle2.text = self.appDel.configDict["app_subtitle"] as? String
        self.lblSubtitle2.textColor = colorValue_lblSubtitle2
        
        //bottomView Theme Color set
        
        let colorValue_BottomView : UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["theme_color"] as! String)
        self.bottomView.backgroundColor = colorValue_BottomView
        
        //NochView Theme Color set
        
       let colorValue_NochView : UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["theme_color"] as! String)
       self.NochView.backgroundColor = colorValue_NochView
        
        
        let colorValue_button: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["tab_color"] as! String)
        self.btnNextPage.setTitleColor(colorValue_button, for: .normal)
        self.btnNextPage.setTitle("Next", for: UIControl.State.normal)
        
        
        let colorValue_button1: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["tab_color"] as! String)
        self.btnPrevPage.setTitleColor(colorValue_button1, for: .normal)
        self.btnPrevPage.setTitle("Show Trail", for: UIControl.State.normal)
        
    
        //NextButton Theme Color set
        
        //button.setBackgroundImage(UIImage(named: “testImage.png”), forState: UIControlState.Normal)
        // [self.btnNextPage sd_setBackgroundImageWithURL:[NSURL URLWithString:@"urlString.png"] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
        
        //BackGround ImgSet
        
         if UIDevice.current.userInterfaceIdiom == .pad
         {
          
            let bannerImgStr = self.appDel.configDict["ipad_screen1_background_image"] as! String
            if bannerImgStr != nil
            {
                let urlString = bannerImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                self.BackImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    
                })
            }
         }
        else
         {
            let bannerImgStr = self.appDel.configDict["iphone_screen1_background_image"] as! String
            if bannerImgStr != nil
            {
                let urlString = bannerImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                self.BackImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    
                })
            }
         }
        
      
        
        //Devider Next/Prev image set
       
        let colorValue_devColor: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["tab_color"] as! String)
       DevNextImg.backgroundColor = colorValue_devColor
       DevPrevImg.backgroundColor = colorValue_devColor
      
        
        
        //Logo ImgSet
        
         if UIDevice.current.userInterfaceIdiom == .pad
         {
            let logoImgStr = self.appDel.configDict["ipad_app_logo"] as! String
            if logoImgStr != nil
            {
                let urlString = logoImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                self.LogoImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                })
            }
        }
        
        else
         {
            let logoImgStr = self.appDel.configDict["iphone_app_logo"] as! String
            if logoImgStr != nil
            {
                let urlString = logoImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                self.LogoImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                })
            }
        }
        
       
        
        //Bottom Next Button
         if UIDevice.current.userInterfaceIdiom == .pad
         {
            let tabBarImg3 = self.appDel.configDict["ipad_bottom_icon_3"] as! String
            
            if tabBarImg3 != nil
            {
                
//                let urlString = tabBarImg3.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//
//                self.btnNextPage.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
//                    , completed: { (image, error, cacheType, imageURL) in
//                        self.btnNextPage.backgroundImage(for: UIControl.State.normal)
//                })
                
            }
         }
         else
         {
            let tabBarImg3 = self.appDel.configDict["iphone_bottom_icon_3"] as! String
            
            if tabBarImg3 != nil
            {
                
//                let urlString = tabBarImg3.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//
//                self.btnNextPage.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
//                    , completed: { (image, error, cacheType, imageURL) in
//                        self.btnNextPage.backgroundImage(for: UIControl.State.normal)
//                })
                
            }
         }
        
        
        let tabBarImg4 = self.appDel.configDict["iphone_bottom_icon_3"] as! String

        if tabBarImg4 != nil
        {

//            let urlString = tabBarImg4.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//
//            self.btnHomePage.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
//                , completed: { (image, error, cacheType, imageURL) in
//                    self.btnHomePage.backgroundImage(for: UIControl.State.normal)
//            })

        }
        
    }
  
    


}
