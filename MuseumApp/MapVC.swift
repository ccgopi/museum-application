//
//  MapVC.swift
//  MuseumApp
//
//  Created by CCT Macmini on 23/01/19.
//  Copyright © 2019 CCT Macmini. All rights reserved.
//

import UIKit
import GoogleMaps
class MapVC: UIViewController,CLLocationManagerDelegate


{
    var DestinationLat  : String!
    var DestinationLong  : String!
    @IBOutlet weak var mapView: GMSMapView!
    var locationManager = CLLocationManager()
    
    
    @IBOutlet weak var topView: UIView!
    
   
    @IBOutlet weak var btnBack: UIButton!
    override func viewDidLoad()
    {
        super.viewDidLoad()
       
       self.SetUpLocationManager()
       
    }
    
   
    func SetUpLocationManager()  {
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
    }
    func getPolylineRoute(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D){
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&sensor=true&mode=driving&key=AIzaSyCDHTMM9YVHtm5yYR0-aAuzH9QxG66w-1w")!
        
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
              
            }
            else {
                do {
                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
                        
                        guard let routes = json["routes"] as? NSArray else {
                            DispatchQueue.main.async {
                            }
                            return
                        }
                        
                        if (routes.count > 0) {
                            let overview_polyline = routes[0] as? NSDictionary
                            let dictPolyline = overview_polyline?["overview_polyline"] as? NSDictionary
                            
                            let points = dictPolyline?.object(forKey: "points") as? String
                            
                            self.showPath(polyStr: points!)
                            
                            DispatchQueue.main.async {
                           
                                
                                let bounds = GMSCoordinateBounds(coordinate: source, coordinate: destination)
                                let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top: 170, left: 30, bottom: 30, right: 30))
                                self.mapView!.moveCamera(update)
                            }
                        }
                        else {
                            DispatchQueue.main.async {
                                
                                Alert.showAlert(title: Constant.alertTitle , message: "Path not found." )
                              
                                let camera = GMSCameraPosition.camera(withLatitude: source.latitude, longitude: source.longitude, zoom: 16)
                                self.mapView?.camera = camera
                                self.mapView.mapType = .normal
                                self.mapView?.animate(to: camera)
                            }
                        }
                    }
                }
                catch {
                    print("error in JSONSerialization")
                    DispatchQueue.main.async {
                       // self.activityIndicator.stopAnimating()
                    }
                }
            }
        })
        task.resume()
        
        
    }
    
    func showPath(polyStr :String){
        DispatchQueue.main.async {
            let path = GMSPath(fromEncodedPath: polyStr)
            let polyline = GMSPolyline(path: path)
            polyline.strokeWidth = 5.0
            polyline.strokeColor = UIColor.blue
            polyline.map = self.mapView
            //
            // self.activityIndicator.stopAnimating()
        }
       
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation = locations.last
        let center = CLLocationCoordinate2D(latitude: userLocation!.coordinate.latitude, longitude: userLocation!.coordinate.longitude)
        
        
        let Deslet = Double(DestinationLat)
        let Deslong = Double(DestinationLong)
         let End = CLLocationCoordinate2D(latitude: Deslet ?? 0.0 , longitude: Deslong ?? 0.0)
        DispatchQueue.main.async {
            self.getPolylineRoute(from: center, to: End)
            self.SetMarker(from: center, to: End)
          
        }
        
        locationManager.stopUpdatingLocation()
    }
    func SetMarker(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D)  {
        
    
         let position1 = source
         let markerSource = GMSMarker(position: position1)
          self.getAddress(pdblLatitude: position1) { (adress) in
          markerSource.title = adress
           markerSource.map = self.mapView
        }

   
        let position2 = destination
        let markerDestination = GMSMarker(position: position2)
        self.getAddress(pdblLatitude: position2) { (adress) in
            markerDestination.title = adress
            markerDestination.map = self.mapView
        }
  
  

    //markerDestination.map = mapView
        
    }
    
    func getAddress(pdblLatitude: CLLocationCoordinate2D,handler: @escaping (String) -> Void)
    {
        var address: String = ""
        var City: String = ""
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: pdblLatitude.latitude, longitude: pdblLatitude.longitude)
        //selectedLat and selectedLon are double values set by the app in a previous process
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            
            // Place details
            var placeMark: CLPlacemark?
            placeMark = placemarks?[0]
            
            // Address dictionary
            //print(placeMark.addressDictionary ?? "")
            
            // Location name
            if let locationName = placeMark?.addressDictionary?["Name"] as? String {
                address += locationName + ", "
            }
            
            // Street address
            if let street = placeMark?.addressDictionary?["Thoroughfare"] as? String {
                address += street + ", "
            }
            
            // City
            if let city = placeMark?.addressDictionary?["City"] as? String {
                address += city + ", "
                City = city
            }
            
            // Zip code
            if let zip = placeMark?.addressDictionary?["ZIP"] as? String {
                address += zip + ", "
            }
            
            // Country
            if let country = placeMark?.addressDictionary?["Country"] as? String {
                address += country
            }
            
            // Passing address back
            handler(City)
        })
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch status {
        case .notDetermined:
            
            manager.requestWhenInUseAuthorization()
            manager.startUpdatingLocation()
            break
            
        case .denied:
           
            Alert.showAlert(title: Constant.alertTitle, message: "Please enable Location Services in Settings")
            manager.stopUpdatingLocation()
         
            break
            
        case .authorizedWhenInUse:
           
            manager.startUpdatingLocation() //Will update location immediately
            break
            
        case .authorizedAlways:
           
            manager.startUpdatingLocation() //Will update location immediately
            break
        default:
            break
        }
    }

    @IBAction func btnBackPress(_ sender: Any)
    {
        //POP_VC()
          self.navigationController?.popViewController(animated: true)
        
    }
    
}
