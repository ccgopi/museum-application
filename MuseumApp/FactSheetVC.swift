//
//  FactSheetVC.swift
//  MuseumApp
//
//  Created by CCT Macmini on 03/01/19.
//  Copyright © 2019 CCT Macmini. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire

class FactSheetVC: UIViewController,UITableViewDelegate, UITableViewDataSource
{
    
   
    var videoDict: Dictionary<String,Any>!
    var kidsDict: Dictionary<String,Any>!
    @IBOutlet weak var btnNextPage: UIButton!
    @IBOutlet weak var blurViewTopCons: NSLayoutConstraint!
    @IBOutlet weak var btnPrevPage: UIButton!
    @IBOutlet weak var btnHomePage: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var DevPrevImg: UIImageView!
    @IBOutlet weak var DevNextImg: UIImageView!
    @IBOutlet weak var backImg: UIImageView!
    @IBOutlet weak var NochView: UIView!
    @IBOutlet weak var lblTitleName: UILabel!
    @IBOutlet weak var lblTitleFactSheet: UILabel!
   
    @IBOutlet weak var ArrowbtnNextPage: UIButton!
    @IBOutlet weak var ArrowbtnPrevPage: UIButton!
    @IBOutlet weak var FactImg: UIImageView!
    @IBOutlet var tblFactSheet: UITableView!
    var factSheetDict: Dictionary<String,Any>!
    var aboutUsDict: Dictionary<String,Any>!
    var arrayFactsheet: Array<Any>!
    var Gallery: Dictionary<String,Any>!
    var appDel: AppDelegate!
    var Videos: Int!
    var Kids: Int!
    var check: Bool!
    var FactDict: Dictionary<String,Any>!
    let AryFactSheet: [String] = ["Fact Sheet - Engine", "Fact Sheet - Engine", "Fact Sheet - Engine", "Fact Sheet - Engine", "Goat"]
    

    @IBAction func btnNextPage(_ sender: Any)
    {
        
        
        videoDict = appDel.itemDetails["video"] as? Dictionary<String, Any>
        kidsDict = appDel.itemDetails["kids"] as? Dictionary<String, Any>
       
        if videoDict["status"] as! String != "N"
        {
            if UIDevice.current.userInterfaceIdiom == .pad
            {
                PUSH_STORY_R1(Identifier: "VideosVC")
                return
            }
            else
            {
                PUSH_STORY_R1(Identifier: "VideosVC")
                return
            }}
        
        if kidsDict["status"] as! String != "N"
        {
            if UIDevice.current.userInterfaceIdiom == .pad
            {
                PUSH_STORY_R1(Identifier: "KidsVC")
                return
            }
            else
            {
                PUSH_STORY_R1(Identifier: "KidsVC")
                return
            }
            
        }
       
        
//        if UIDevice.current.userInterfaceIdiom == .pad
//        {
//            let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
//            let Videos = storyboard.instantiateViewController(withIdentifier: "VideosVC") as! VideosVC
//            self.navigationController?.pushViewController(Videos, animated: false)
//        }
//
//        else
//        {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let Videos = storyboard.instantiateViewController(withIdentifier: "VideosVC") as! VideosVC
//            self.navigationController?.pushViewController(Videos, animated: false)
//
//
//        }
    }
    
    func nextButtonText()
    {
        let colorValue_button: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["tab_color"] as! String)
        self.btnNextPage.setTitleColor(colorValue_button, for: .normal)
       
        if videoDict["status"] as! String != "N"
        {
            let str = videoDict["title"] as? String
            self.btnNextPage.setTitle(str!, for: UIControl.State.normal)
          //  self.btnNextPage.setTitle("Videos ", for: UIControl.State.normal)
             ArrowbtnNextPage.isHidden = false;
            return
        }
        else
        {
             ArrowbtnNextPage.isHidden = true;
        }
        
        if kidsDict["status"] as! String != "N"
        {
            let str = kidsDict["title"] as? String
            self.btnNextPage.setTitle(str!, for: UIControl.State.normal)
          //  self.btnNextPage.setTitle("Kids ", for: UIControl.State.normal)
             ArrowbtnNextPage.isHidden = false;
            return
        }
        else
        {
            ArrowbtnNextPage.isHidden = true;
        }
    }
    
   func PrevButtonText()
   {
    
    let colorValue_button: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["tab_color"] as! String)
    self.btnPrevPage.setTitleColor(colorValue_button, for: .normal)
  
    aboutUsDict = appDel.itemDetails["about"] as? Dictionary<String, Any>
    Gallery = appDel.itemDetails["gallery"] as? Dictionary<String, Any>
    
    if UIDevice.current.userInterfaceIdiom == .pad
    {
        if Gallery["status"] as! String != "N"
        {
            let str = Gallery["title"] as? String
            self.btnPrevPage.setTitle(str!, for: UIControl.State.normal)
           // self.btnPrevPage.setTitle("Gallery ", for: UIControl.State.normal)
            return
        }
        
        else if aboutUsDict["status"] as! String != "N"
        {
            let str = aboutUsDict["title"] as? String
            self.btnPrevPage.setTitle(str!, for: UIControl.State.normal)
           // self.btnPrevPage.setTitle("About ", for: UIControl.State.normal)
            return
           
        }
        else
        {
            self.btnPrevPage.setTitle("", for: UIControl.State.normal)
            return
            
        }
      
    }
    else
    {
        
        if Gallery["status"] as! String != "N"
        {
            let str = Gallery["title"] as? String
            self.btnPrevPage.setTitle(str!, for: UIControl.State.normal)
         //   self.btnPrevPage.setTitle("Gallery ", for: UIControl.State.normal)
            return
            
        }
        else if aboutUsDict["status"] as! String != "N"
        {
            let str = aboutUsDict["title"] as? String
            self.btnPrevPage.setTitle(str!, for: UIControl.State.normal)
           // self.btnPrevPage.setTitle("About ", for: UIControl.State.normal)
            return
        }
        else
        {
            
            self.btnPrevPage.setTitle("", for: UIControl.State.normal)
            return
        }
        
    }
    
    
   }
    @IBAction func btnPrevPage(_ sender: Any)
    {
        
        
        aboutUsDict = appDel.itemDetails["about"] as? Dictionary<String, Any>
        Gallery = appDel.itemDetails["gallery"] as? Dictionary<String, Any>
       
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            if Gallery["status"] as! String != "N"
            {
                self.PUSH_STORY_R2(Identifier: "GalleryVC1")
                
            }
            else if aboutUsDict["status"] as! String != "N"
            {
             self.PUSH_STORY_R2(Identifier: "AboutVC")
            
            }
            else
            {
              self.PUSH_STORY_R2(Identifier: "MenuVC")
             }
    }
        else
        {
            
    if Gallery["status"] as! String != "N"{
    self.PUSH_STORY_R2(Identifier: "idGallery")
    
    }else if aboutUsDict["status"] as! String != "N"{
    self.PUSH_STORY_R2(Identifier: "AboutVC")
    
    
    }
    
    else
    {
     self.PUSH_STORY_R2(Identifier: "MenuVC")
    }
            
}
 
    }
    @IBAction func btnHomePage(_ sender: Any)
    {
        if UIDevice.current.userInterfaceIdiom == .pad
        {
//            let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
//            let FactSheet = storyboard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
//            self.navigationController?.pushViewController(FactSheet, animated: false)
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: MenuVC.self) {
                    _ =  self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
        else
        {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: MenuVC.self) {
                    _ =  self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let FactSheet = storyboard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
//            self.navigationController?.pushViewController(FactSheet, animated: false)
        }
    }
   
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
     
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            self.tblFactSheet.tableFooterView = UIView(frame: .zero)
            self.tblFactSheet.separatorStyle = .none
            self.tblFactSheet.estimatedRowHeight = 60.0
            self.tblFactSheet.rowHeight = UITableView.automaticDimension
            self.tblFactSheet.register(UINib.init(nibName: "FactCellIpad", bundle: nil), forCellReuseIdentifier: "FactCellIpad")
        }
        else
        {
            self.tblFactSheet.tableFooterView = UIView(frame: .zero)
            self.tblFactSheet.separatorStyle = .none
            self.tblFactSheet.estimatedRowHeight = 60.0
            self.tblFactSheet.rowHeight = UITableView.automaticDimension
            self.tblFactSheet.register(UINib.init(nibName: "FactCell", bundle: nil), forCellReuseIdentifier: "FactCell")
        }
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated) // No need for semicolon
        
        if(appDel == nil)
        {
            appDel = UIApplication.shared.delegate as? AppDelegate
        }
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        factSheetDict = appDel.itemDetails["fact_sheet"] as? Dictionary<String, Any>
        arrayFactsheet = factSheetDict["fact_sheet_files"] as? Array<Any>
        check = true
        
        
        videoDict = appDel.itemDetails["video"] as? Dictionary<String, Any>
        kidsDict = appDel.itemDetails["kids"] as? Dictionary<String, Any>
        
        if videoDict["status"] as! String != "N"
        {
            check = checkForHiddenButton(_check: check)
        }
        
        if kidsDict["status"] as! String != "N"
        {
            check = checkForHiddenButton(_check: check)
        }
        if check == true
        {
            btnNextPage.isHidden = true
            
        }
      
        self.BottomBarButtonHideShowMethod()
        self.ConfigInformation()
        self.nextButtonText()
        self.PrevButtonText()
        self.SetMenuTopConstraint()
    }
    func BottomBarButtonHideShowMethod()
    {
       // btnNextPage.isHidden = false
        btnNextPage.isEnabled = true
        btnPrevPage.isHidden = false
        btnPrevPage.isEnabled = true
        btnHomePage.isHidden = false
        btnHomePage.isEnabled = true
        DevPrevImg.isHidden = false
        DevNextImg.isHidden = false
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if arrayFactsheet == nil
        {
            return 0;
        }
        return arrayFactsheet.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        var cellIdentifier: String = ""
        if  UIDevice.current.userInterfaceIdiom == .pad
        {
            cellIdentifier = "FactCellIpad"
            
        }
        else
        {
            cellIdentifier = "FactCell"
        }
        
        let cell: FactCell = self.tblFactSheet.dequeueReusableCell(withIdentifier: cellIdentifier) as! FactCell
        
        //let cell: FactCell = self.tblFactSheet.dequeueReusableCell(withIdentifier: cellIdentifier) as? FactCell
        tableView.backgroundColor = .clear
        cell.selectionStyle = .none
        var dict: Dictionary = arrayFactsheet[indexPath.row] as! Dictionary<String,Any>
        cell.lblCellFactSheetTitle.text = dict["name"] as? String
        cell.lblCellFactSheetTitle.textColor = UIColor.white
        return cell
    }
    
    // method to run when table view cell is tapped
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
       
        var dict1: Dictionary = arrayFactsheet[indexPath.row] as! Dictionary<String,Any>
     
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
            let FactSheetDetail = storyboard.instantiateViewController(withIdentifier: "FactSheetDetailVC") as! FactSheetDetailVC
            FactSheetDetail.strTitle = dict1["name"] as? String
            FactSheetDetail.strPdf = dict1["file_name"] as? String
            self.navigationController?.pushViewController(FactSheetDetail, animated: false)
            
        }
        else
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let FactSheetDetail = storyboard.instantiateViewController(withIdentifier: "FactSheetDetailVC") as! FactSheetDetailVC
            FactSheetDetail.strTitle = dict1["name"] as? String
            FactSheetDetail.strPdf = dict1["file_name"] as? String
            self.navigationController?.pushViewController(FactSheetDetail, animated: false)
        }
 
        print("You tapped cell number \(indexPath.row).")
    }

    func SetMenuTopConstraint()
    {
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            if Device.IS_IPAD_PRO
            {
                blurViewTopCons.constant = 30
            }
            else
            {
                blurViewTopCons.constant = 10
            }
        }
            
        else
        {
            if Device.IS_IPHONE_6
            {
                
            }
                
            else if Device.IS_IPHONE_6P
            {
                
            }
                
            else
            {
                
            }
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            return UITableView.automaticDimension
        }
        
        else
        {
            return UITableView.automaticDimension

           // return 66.0;
        }
        
    }
    
    func ConfigInformation()
    {
        
//        //webview Load
//        let strText = aboutUsDict["description"] as! String
//        strText.replacingOccurrences(of: "\n", with: "<br/>")
//        webview.loadHTMLString(strText, baseURL: nil)
        
       
        
        //Title Heading Text set
        
         lblTitleName.text = appDel.itemDetails["title"] as? String
         lblTitleName.textColor = AppDelegate.sharedInstance().hexStringToUIColor(hex : appDel.itemDetails["title_colour"] as! String)
        
        //Title Video Text set
        
        self.lblTitleFactSheet.text = factSheetDict["title"] as? String
        self.lblTitleFactSheet.textColor = AppDelegate.sharedInstance().hexStringToUIColor(hex : factSheetDict["color"] as! String)
        
        //Set Heading Image
        
        var factSheetLogo : String!
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            factSheetLogo = factSheetDict["ipad_icon_image"] as? String
        }
        else
        {
            factSheetLogo = factSheetDict["iphone_icon_image"] as? String
        }
        
        if factSheetLogo != nil
        {
            let urlString = factSheetLogo.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            FactImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                
            })
        }
    
        //Home button
        let colorValue_button: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["tab_color"] as! String)
        self.btnHomePage.setTitleColor(colorValue_button, for: .normal)
        self.btnHomePage.setTitle("Home", for: UIControl.State.normal)
        
        
        //bottomView Theme Color set
        let colorValue_BottomView : UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["theme_color"] as! String)
        self.bottomView.backgroundColor = colorValue_BottomView
        
        let colorValue_NochView : UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["theme_color"] as! String)
        self.NochView.backgroundColor = colorValue_NochView
        
        var tabBarImg1 : String!
        var tabBarImg2 : String!
        var tabBarImg3 : String!
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            tabBarImg1 = self.appDel.configDict["ipad_bottom_icon_1"] as? String
            tabBarImg2 = self.appDel.configDict["ipad_bottom_icon_2"] as? String
            tabBarImg3 = self.appDel.configDict["ipad_bottom_icon_3"] as? String
            
        }
        else
        {
            tabBarImg1 = self.appDel.configDict["ipad_bottom_icon_1"] as? String
            tabBarImg2 = self.appDel.configDict["ipad_bottom_icon_2"] as? String
            tabBarImg3 = self.appDel.configDict["ipad_bottom_icon_3"] as? String
            
        }
        
        //btn Prev page
        if tabBarImg1 != nil
        {
//            let urlString = tabBarImg1.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//            self.btnPrevPage.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
//                , completed: { (image, error, cacheType, imageURL) in
//                    self.btnPrevPage.backgroundImage(for: UIControl.State.normal)
//            })
        }
        
        //btn Next page
        if tabBarImg3 != nil
        {
//            let urlString = tabBarImg3.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//            self.btnNextPage.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
//                , completed: { (image, error, cacheType, imageURL) in
//                    self.btnNextPage.backgroundImage(for: UIControl.State.normal)
//            })
        }
        
        //btn Home page
        if tabBarImg2 != nil
        {
//            let urlString = tabBarImg2.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//            self.btnHomePage.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
//                , completed: { (image, error, cacheType, imageURL) in
//                    self.btnHomePage.backgroundImage(for: UIControl.State.normal)
//            })
            
        }
        
        //BackGround ImgSet
     
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            
            let bannerImgStr = appDel.itemDetails["ipad_background_image"] as! String
            if bannerImgStr != nil
            {
                let urlString = bannerImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                self.backImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    
                })
            }
        }
        else
        {
            let bannerImgStr = appDel.itemDetails["iphone_background_image"] as! String
            if bannerImgStr != nil
            {
                let urlString = bannerImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                self.backImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    
                })
            }
        }
        
        
        //Devider Next/Prev image set
        
        let colorValue_devColor: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["tab_color"] as! String)
        DevNextImg.backgroundColor = colorValue_devColor
        DevPrevImg.backgroundColor = colorValue_devColor
     
    }
    func checkForHiddenButton(_check : Bool) -> Bool {
        check = false
        return check
    }
  
}
