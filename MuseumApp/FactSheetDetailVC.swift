//
//  FactSheetDetailVC.swift
//  MuseumApp
//
//  Created by CCT Macmini on 09/01/19.
//  Copyright © 2019 CCT Macmini. All rights reserved.
//

import UIKit

class FactSheetDetailVC: UIViewController, UIWebViewDelegate
{
   
    @IBOutlet weak var blurViewTopCons: NSLayoutConstraint!
    @IBOutlet weak var btnPrevPage: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var backImg: UIImageView!
    @IBOutlet weak var lblTitleName: UILabel!
    @IBOutlet weak var webview: UIWebView!
    var factSheetDict: Dictionary<String,Any>!
    var arrayFactsheet: Array<Any>!
    var strTitle: String?
    var strPdf : String!
    var appDel: AppDelegate!
   

    override func viewDidLoad()
    {
        super.viewDidLoad()

        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated) // No need for semicolon
        
        if(appDel == nil)
        {
            appDel = UIApplication.shared.delegate as? AppDelegate
        }
        print(strTitle)
        webview.delegate = self
         self.navigationController?.setNavigationBarHidden(true, animated: animated)
        factSheetDict = appDel.itemDetails["fact_sheet"] as? Dictionary<String, Any>
        arrayFactsheet = factSheetDict["fact_sheet_files"] as? Array<Any>
        lblTitleName.text = strTitle
        lblTitleName.textColor = AppDelegate.sharedInstance().hexStringToUIColor(hex : appDel.configDict["tab_color"] as! String)
        lblTitleName.text = strTitle
        self.BottomBarButtonHideShowMethod()
        self.ConfigInformation()
       
    }
    
    func BottomBarButtonHideShowMethod()
    {
       
        btnPrevPage.isHidden = false
        btnPrevPage.isEnabled = true
     }
    
    @IBAction func btnPrevPage(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func ConfigInformation()
    {
        let url: URL! = URL(string: strPdf)
       
        webview.loadRequest(URLRequest(url: url))
                
        //let url = URL(string: "http://www.pdf995.com/samples/pdf.pdf")
       // webview.loadRequest(URLRequest(url: url!))
        //webview.backgroundColor = UIColor.red
        
        
        //back button color change
        
//        let colorValue_backbutton : UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["theme_color"] as! String)
//        self.NochView.backgroundColor = colorValue_backbutton
        
       
        //bottomView Theme Color set
        let colorValue_BottomView : UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["theme_color"] as! String)
        self.bottomView.backgroundColor = colorValue_BottomView
        
    
        //btn Prev page
     
        
        
        //BackGround ImgSet
        
//        if UIDevice.current.userInterfaceIdiom == .pad
//        {
//
//            let bannerImgStr = appDel.itemDetails["ipad_background_image"] as! String
//            if bannerImgStr != nil
//            {
//                let urlString = bannerImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//                self.backImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
//
//                })
//            }
//        }
//        else
//        {
//            let bannerImgStr = appDel.itemDetails["iphone_background_image"] as! String
//            if bannerImgStr != nil
//            {
//                let urlString = bannerImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//                self.backImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
//
//                })
//            }
//        }a
//
        
        //Devider Next/Prev image set
       
        
    }
    
    func webViewDidStartLoad(_ webView: UIWebView)
    {
         self.appDel.ShowHUD()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        self.appDel.HideHUD()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        self.appDel.HideHUD()
    }
    

   

}
