//
//  CollectionViewCellImages.swift
//  MuseumApp
//
//  Created by CCT Mini 2 on 12/04/19.
//  Copyright © 2019 CCT Macmini. All rights reserved.
//

import UIKit



class CollectionViewCellImages: UICollectionViewCell,UIScrollViewDelegate
{
    
    
    @IBOutlet weak var scrolview: UIScrollView!
    @IBOutlet weak var images: UIImageView!
  //  var gestureRecognizer: UITapGestureRecognizer!
    
    override func awakeFromNib()
    {
        self.scrolview.minimumZoomScale = 1.0
        self.scrolview.maximumZoomScale = 4.0
        self.scrolview.delegate = self
    }
    
//    func setupGestureRecognizer()
//    {
//        gestureRecognizer = UITapGestureRecognizer(target: nil, action: nil)
//        gestureRecognizer.numberOfTapsRequired = 2
//        addGestureRecognizer(gestureRecognizer)
//        gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTap))
//    }
    
//    @objc func handleDoubleTap()
//    {
//        if zoomScale == 1
//        {
//            zoom(to: zoomRectForScale(maximumZoomScale, center: gestureRecognizer.location(in: gestureRecognizer.view)), animated: true)
//        }
//        else
//        {
//            setZoomScale(1, animated: true)
//        }
//    }
//    func zoomRectForScale(_ scale: CGFloat, center: CGPoint) -> CGRect
//    {
//        var zoomRect = CGRect.zero
//        zoomRect.size.height = images.frame.size.height / scale
//        zoomRect.size.width = images.frame.size.width / scale
//        let newCenter = convert(center, from: images)
//        zoomRect.origin.x = newCenter.x - (zoomRect.size.width / 2.0)
//        zoomRect.origin.y = newCenter.y - (zoomRect.size.height / 2.0)
//        return zoomRect
//    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView?
    {
        return self.images
    }

}
