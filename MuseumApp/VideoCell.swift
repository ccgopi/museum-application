//
//  VideoCell.swift
//  MuseumApp
//
//  Created by Admin on 07/01/19.
//  Copyright © 2019 CCT Macmini. All rights reserved.
//

import UIKit

class VideoCell: UITableViewCell
{

    @IBOutlet weak var btnVideoPlay: UIButton!
    @IBOutlet weak var imgVideo: UIImageView!
    @IBOutlet weak var lblVideoTitle: UILabel!
  
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
