//
//  TrailTableViewCell.swift
//  MuseumApp
//
//  Created by Admin on 19/03/19.
//  Copyright © 2019 CCT Macmini. All rights reserved.
//

import UIKit

class TrailTableViewCell: UITableViewCell {

    
    @IBOutlet weak var btnSelected: UIButton!
    
    @IBOutlet weak var TrailImgView: UIImageView!
    
    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblAdress: UILabel!
    
    @IBOutlet weak var btnViewOnMap: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
