//
//  VideosVC.swift
//  MuseumApp
//
//  Created by CCT Macmini on 03/01/19.
//  Copyright © 2019 CCT Macmini. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
import AVFoundation
import AVKit
class VideosVC: UIViewController,UITableViewDelegate, UITableViewDataSource
{
    
    
   
    @IBOutlet weak var blurViewTopCons: NSLayoutConstraint!
    @IBOutlet weak var btnNextPage: UIButton!
    @IBOutlet weak var btnPrevPage: UIButton!
    @IBOutlet weak var btnHomePage: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var DevPrevImg: UIImageView!
    @IBOutlet weak var DevNextImg: UIImageView!
    @IBOutlet weak var backImg: UIImageView!
    @IBOutlet weak var NochView: UIView!
    @IBOutlet var tblVideo: UITableView!
    @IBOutlet weak var lblTitleName: UILabel!
    @IBOutlet weak var lblTitleVideo: UILabel!
    @IBOutlet weak var VideoImg: UIImageView!
    @IBOutlet weak var ArrowbtnNextPage: UIButton!
    @IBOutlet weak var ArrowbtnPrevPage: UIButton!
    var videoDict: Dictionary<String,Any>!
    var interaction: UIDocumentInteractionController?
    var arrayVideo: Array<Any>!
    var appDel: AppDelegate!
    var Kids: Int!
    var check: Bool!
    var kidsDict: Dictionary<String,Any>!
    var factSheetDict: Dictionary<String,Any>!
    var aboutUsDict: Dictionary<String,Any>!
    var Gallery: Dictionary<String,Any>!
    
    @IBAction func btnNextPage(_ sender: Any)
    {
         kidsDict = appDel.itemDetails["kids"] as? Dictionary<String, Any>
        if kidsDict["status"] as! String != "N"{
            if UIDevice.current.userInterfaceIdiom == .pad
            {
                PUSH_STORY_R1(Identifier: "KidsVC")
                return
            } else
            {
                PUSH_STORY_R1(Identifier: "KidsVC")
                return
            }}
     
//        if UIDevice.current.userInterfaceIdiom == .pad
//        {
//            let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
//            let Kids = storyboard.instantiateViewController(withIdentifier: "KidsVC") as! KidsVC
//            self.navigationController?.pushViewController(Kids, animated: false)
//        }
//        else
//        {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let Kids = storyboard.instantiateViewController(withIdentifier: "KidsVC") as! KidsVC
//            self.navigationController?.pushViewController(Kids, animated: false)
//        }
    }
    
    @IBAction func btnPrevPage(_ sender: Any)
    {
        factSheetDict = appDel.itemDetails["fact_sheet"] as? Dictionary<String, Any>
        aboutUsDict = appDel.itemDetails["about"] as? Dictionary<String, Any>
        Gallery = appDel.itemDetails["gallery"] as? Dictionary<String, Any>
       
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            if factSheetDict["status"] as! String != "N"{
               self.PUSH_STORY_R2(Identifier: "FactSheetVC")
                
                
            }else if Gallery["status"] as! String != "N"{
                self.PUSH_STORY_R2(Identifier: "GalleryVC1")
            } else if aboutUsDict["status"] as! String != "N" {
               self.PUSH_STORY_R2(Identifier: "AboutVC")
            }
            else{
                self.PUSH_STORY_R2(Identifier: "MenuVC")
            }
            
            
            
        }
        else
        {
            if factSheetDict["status"] as! String != "N"{
                self.PUSH_STORY_R2(Identifier: "FactSheetVC")
                
                
            }else if Gallery["status"] as! String != "N"{
                self.PUSH_STORY_R2(Identifier: "idGallery")
            } else if aboutUsDict["status"] as! String != "N" {
                self.PUSH_STORY_R2(Identifier: "AboutVC")
            }
            else{
                self.PUSH_STORY_R2(Identifier: "MenuVC")
            }
            
        }
        
    }
    
    @IBAction func btnHomePage(_ sender: Any)
    {
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: MenuVC.self) {
                    _ =  self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
//            let storyboard = UIStoryboard(name: "MainiPad", bundle: nil)
//            let FactSheet = storyboard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
//            self.navigationController?.pushViewController(FactSheet, animated: false)
        }
        else
        {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: MenuVC.self) {
                    _ =  self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let FactSheet = storyboard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
//            self.navigationController?.pushViewController(FactSheet, animated: false)
        }
    }
   
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
         if UIDevice.current.userInterfaceIdiom == .pad
         {            
            self.tblVideo.tableFooterView = UIView(frame: .zero)
            self.tblVideo.separatorStyle = .none
            self.tblVideo.estimatedRowHeight = 60.0
            self.tblVideo.rowHeight = UITableView.automaticDimension
            self.tblVideo.register(UINib.init(nibName: "videoiPad", bundle: nil), forCellReuseIdentifier: "videoiPad")
         }
        
        else
         {
            self.tblVideo.tableFooterView = UIView(frame: .zero)
            self.tblVideo.separatorStyle = .none
            self.tblVideo.estimatedRowHeight = 60.0
            self.tblVideo.rowHeight = UITableView.automaticDimension
            self.tblVideo.register(UINib.init(nibName: "VideoCell", bundle: nil), forCellReuseIdentifier: "VideoCell")
        }
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated) // No need for semicolon
        
        if(appDel == nil)
        {
            appDel = UIApplication.shared.delegate as? AppDelegate
        }
         self.navigationController?.setNavigationBarHidden(true, animated: animated)
        videoDict = appDel.itemDetails["video"] as? Dictionary<String, Any>
        arrayVideo = videoDict["video_files"] as? Array<Any>
        check = true
        
        
        kidsDict = appDel.itemDetails["kids"] as? Dictionary<String, Any>
        
        
        
        
        
        if kidsDict["status"] as! String != "N"
        {
            check = checkForHiddenButton(_check: check)
        }
        if check == true {
            btnNextPage.isHidden = true
           
        }
        self.BottomBarButtonHideShowMethod()
        self.ConfigInformation()
        self.nextButtonText()
        self.PrevButtonText()
        self.SetMenuTopConstraint()
    }
    
    func nextButtonText()
    {
        let colorValue_button: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["tab_color"] as! String)
        self.btnNextPage.setTitleColor(colorValue_button, for: .normal)
        
        if kidsDict["status"] as! String != "N"
        {
            let str = kidsDict["title"] as? String
            self.btnNextPage.setTitle(str!, for: UIControl.State.normal)
           // self.btnNextPage.setTitle("Kids ", for: UIControl.State.normal)
             ArrowbtnNextPage.isHidden = false;
            return
        }
        else
        {
             ArrowbtnNextPage.isHidden = true;
        }
    }
    
    func PrevButtonText()
    {
        let colorValue_button: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["tab_color"] as! String)
        self.btnPrevPage.setTitleColor(colorValue_button, for: .normal)
       
        factSheetDict = appDel.itemDetails["fact_sheet"] as? Dictionary<String, Any>
        aboutUsDict = appDel.itemDetails["about"] as? Dictionary<String, Any>
        Gallery = appDel.itemDetails["gallery"] as? Dictionary<String, Any>
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            if factSheetDict["status"] as! String != "N"
            {
                let str = factSheetDict["title"] as? String
                self.btnPrevPage.setTitle(str!, for: UIControl.State.normal)
               // self.btnPrevPage.setTitle("Fact sheet ", for: UIControl.State.normal)
                return
               
            }
            else if Gallery["status"] as! String != "N"
            {
                let str = Gallery["title"] as? String
                self.btnPrevPage.setTitle(str!, for: UIControl.State.normal)
              //  self.btnPrevPage.setTitle("Gallery ", for: UIControl.State.normal)
                return
                
            }
            else if aboutUsDict["status"] as! String != "N"
            {
                let str = aboutUsDict["title"] as? String
                self.btnPrevPage.setTitle(str!, for: UIControl.State.normal)
               // self.btnPrevPage.setTitle("About ", for: UIControl.State.normal)
                return
            }
            else
            {
                
                self.btnPrevPage.setTitle("Menu ", for: UIControl.State.normal)
                return
                
            }
            
            
            
        }
        else
        {
            if factSheetDict["status"] as! String != "N"
            {
                let str = factSheetDict["title"] as? String
                self.btnPrevPage.setTitle(str!, for: UIControl.State.normal)
                //self.btnPrevPage.setTitle("Fact sheet ", for: UIControl.State.normal)
                return
            }
            else if Gallery["status"] as! String != "N"
            {
                let str = Gallery["title"] as? String
                self.btnPrevPage.setTitle(str!, for: UIControl.State.normal)
               // self.btnPrevPage.setTitle("Gallery ", for: UIControl.State.normal)
                return
                
            }
            else if aboutUsDict["status"] as! String != "N"
            {
                let str = aboutUsDict["title"] as? String
                self.btnPrevPage.setTitle(str!, for: UIControl.State.normal)
               // self.btnPrevPage.setTitle("About ", for: UIControl.State.normal)
                return
            }
            else
            {
                self.btnPrevPage.setTitle("", for: UIControl.State.normal)
                return
             }
            
        }
        
    }
    
    func SetMenuTopConstraint()
    {
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            if Device.IS_IPAD_PRO
            {
                blurViewTopCons.constant = 30
            }
            else
            {
                
                blurViewTopCons.constant = 10
            }
        }
            
        else
        {
            if Device.IS_IPHONE_6
            {
                
            }
                
            else if Device.IS_IPHONE_6P
            {
                
            }
                
            else
            {
                
            }
        }
        
    }
    
    func BottomBarButtonHideShowMethod()
    {
       // btnNextPage.isHidden = false
        btnNextPage.isEnabled = true
        btnPrevPage.isHidden = false
        btnPrevPage.isEnabled = true
        btnHomePage.isHidden = false
        btnHomePage.isEnabled = true
        DevPrevImg.isHidden = false
        DevNextImg.isHidden = false
    }
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if arrayVideo == nil
        {
            return 0;
        }
        return arrayVideo.count
    }
    
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
    
        var cellIdentifier: String = ""
        if  UIDevice.current.userInterfaceIdiom == .pad
        {
            cellIdentifier = "videoiPad"
            
        }
        else
        {
            cellIdentifier = "VideoCell"
        }
        
        let cell: VideoCell = self.tblVideo.dequeueReusableCell(withIdentifier: cellIdentifier) as! VideoCell
        tableView.backgroundColor = .clear
        var dict: Dictionary = arrayVideo[indexPath.row] as! Dictionary<String,Any>
        cell.lblVideoTitle.text = dict["name"] as? String
        cell.lblVideoTitle.textColor = UIColor.white
        let videoUrl = dict["thumbnail"] as! String
     
        if videoUrl != nil
        {
            let urlString = videoUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            cell.imgVideo.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                
            })
        }
        cell.selectionStyle = .none
        cell.btnVideoPlay.addTarget(self, action: #selector(VideosVC.coolFunc(_:)), for: .touchUpInside)
        cell.btnVideoPlay.tag = indexPath.row
        return cell
    }
    
    
    @IBAction func coolFunc(_ sender:UIButton!)
    {
        var dict: Dictionary = arrayVideo[sender.tag] as! Dictionary<String,Any>
        let videoURL = URL(string: dict["file_name"] as! String)
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true)
        {
            playerViewController.player!.play()
        }
    }
    
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
   
        var dict: Dictionary = arrayVideo[indexPath.row] as! Dictionary<String,Any>
        let videoURL = URL(string: dict["file_name"] as! String)
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true)
        {
            playerViewController.player!.play()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
            if UIDevice.current.userInterfaceIdiom == .pad
            {
                return 300
                
            }
                
            else
            {
                return 250
                
                
            }
            
        }
        
    
    
    func ConfigInformation()
    {
     
        //Title Heading Text set

        lblTitleName.text = appDel.itemDetails["title"] as? String
        lblTitleName.textColor = AppDelegate.sharedInstance().hexStringToUIColor(hex : appDel.itemDetails["title_colour"] as! String)
        
        //Title Video Text set
        self.lblTitleVideo.text = videoDict["title"] as? String
        self.lblTitleVideo.textColor = AppDelegate.sharedInstance().hexStringToUIColor(hex : videoDict["color"] as! String)
       
        
        
        var videoLogo : String!
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            videoLogo = videoDict["ipad_icon_image"] as? String
        }
        else
        {
            videoLogo = videoDict["iphone_icon_image"] as? String
        }
        
        if videoLogo != nil
        {
            let urlString = videoLogo.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            VideoImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                
            })
        }
        
        //home button
        
        
        let colorValue_button: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["tab_color"] as! String)
        self.btnHomePage.setTitleColor(colorValue_button, for: .normal)
        self.btnHomePage.setTitle("Home", for: UIControl.State.normal)
        
    
        //bottomView Theme Color set
        
        let colorValue_BottomView : UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["theme_color"] as! String)
       self.bottomView.backgroundColor = colorValue_BottomView
        
        //NochView Theme Color set
        let colorValue_NochView : UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["theme_color"] as! String)
        self.NochView.backgroundColor = colorValue_NochView
        
        
        var tabBarImg1 : String!
        var tabBarImg2 : String!
        var tabBarImg3 : String!
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            tabBarImg1 = self.appDel.configDict["ipad_bottom_icon_1"] as? String
            tabBarImg2 = self.appDel.configDict["ipad_bottom_icon_2"] as? String
            tabBarImg3 = self.appDel.configDict["ipad_bottom_icon_3"] as? String
            
        }
        else
        {
            tabBarImg1 = self.appDel.configDict["ipad_bottom_icon_1"] as? String
            tabBarImg2 = self.appDel.configDict["ipad_bottom_icon_2"] as? String
            tabBarImg3 = self.appDel.configDict["ipad_bottom_icon_3"] as? String
            
        }
        
        //btn Prev page
        if tabBarImg1 != nil
        {
//            let urlString = tabBarImg1.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//            self.btnPrevPage.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
//                , completed: { (image, error, cacheType, imageURL) in
//                    self.btnPrevPage.backgroundImage(for: UIControl.State.normal)
//            })
        }
        
        //btn Next page
        if tabBarImg3 != nil
        {
//            let urlString = tabBarImg3.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//            self.btnNextPage.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
//                , completed: { (image, error, cacheType, imageURL) in
//                    self.btnNextPage.backgroundImage(for: UIControl.State.normal)
//            })
        }
        
        //btn Home page
        if tabBarImg2 != nil
        {
//            let urlString = tabBarImg2.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//            self.btnHomePage.sd_setBackgroundImage(with: URL.init(string: urlString!), for: UIControl.State.normal
//                , completed: { (image, error, cacheType, imageURL) in
//                    self.btnHomePage.backgroundImage(for: UIControl.State.normal)
//            })
            
        }
        
        //Devider Next/Prev image set
        
        let colorValue_devColor: UIColor = AppDelegate.sharedInstance().hexStringToUIColor(hex: appDel.configDict["tab_color"] as! String)
        DevNextImg.backgroundColor = colorValue_devColor
        DevPrevImg.backgroundColor = colorValue_devColor
        
        
        //BackGround ImgSet
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            
            let bannerImgStr = appDel.itemDetails["ipad_background_image"] as! String
            
            if bannerImgStr != nil
            {
                let urlString = bannerImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                self.backImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    
                })
            }
        }
        else
        {
            let bannerImgStr = appDel.itemDetails["iphone_background_image"] as! String
            if bannerImgStr != nil
            {
                let urlString = bannerImgStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                self.backImg.sd_setImage(with: URL.init(string: urlString!), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    
                })
            }
        }
        
       
        
    }
    
    func checkForHiddenButton(_check : Bool) -> Bool {
        check = false
        return check
    }
  
    
    
  


}
